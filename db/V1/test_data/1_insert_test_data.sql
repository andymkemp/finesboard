----insert_finesboard_public_users----
INSERT INTO finesboard.public.userLogin 
    (id, username, password) 
VALUES 
    ('501d1a52-dae3-41bc-b312-05175fd68f3a', 'user1', 'AQAAAAEAACcQAAAAEPrwWffAsZuel8+BG9PaTVErpRixWclPzd8fIKB/56yMXdh58m22P1Pfq9BHBYYOwQ=='),
    ('d871f442-e6b8-456c-96b9-77b8f0ef8c5e', 'user2', 'AQAAAAEAACcQAAAAEPrwWffAsZuel8+BG9PaTVErpRixWclPzd8fIKB/56yMXdh58m22P1Pfq9BHBYYOwQ==');

----insert_finesboard_public_finesboards----
INSERT INTO finesboard.public.finesboard 
    (id, finesboardStatusId, name, currency) 
VALUES 
    ('4ecf9c20-27fd-4d4d-84dd-a0df7ac11665', 'e55327ed-2e1b-4135-818a-608e09c7630c', 'Cake Ticks', 'Tick'), 
    ('aa2eca98-42cd-4b3b-b6aa-220fa979d511', 'e55327ed-2e1b-4135-818a-608e09c7630c', 'Hockey Fines', 'Beer'),
    ('a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba', 'e55327ed-2e1b-4135-818a-608e09c7630c', 'Traffic Fines', 'Rand');

----insert_finesboard_public_offences----
INSERT INTO finesboard.public.offence 
    (id, name, finesboardId) 
VALUES 
    ('0da55b8f-d8b4-46cf-b018-9c96f7ee56a1', 'Any', '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665'), 
    ('46b2f62b-4984-4d89-b675-b4924ff6b8cb', 'Green Card', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('cae9540b-28a9-42f4-8d2a-b20dfee6aad0', 'Yellow Card', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('8bfc5283-560b-44a1-a3b3-fdb4c66c3748', 'Red Card', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511');

----insert_finesboard_public_forfeits----
INSERT INTO finesboard.public.forfeit 
    (id, name, amount, finesboardId) 
VALUES 
    ('dd4e93a0-d5c8-416b-afc8-215fd96a8040', 'Cake Tick', 1.00, '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665'), 
    ('f2a45fbf-f1f5-45f2-ba4a-a299cb263888', '2 Sips', 0.2, 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('7f604bd3-4ddf-4083-af1a-480fb07875b6', 'Half Kepler', 0.5, 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('c455a469-41de-4547-81ca-146c72650d65', 'Full Kepler', 1, 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('c37f3934-3f03-429d-82e4-466aee056f5e', 'R 100', 100, 'a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba'),
    ('5e8c48c4-8fa7-4a8b-9d4f-79d89c2ed0e6', 'R 300', 300, 'a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba');

----insert_finesboard_public_offence_forfeits----
INSERT INTO finesboard.public.offenceForfeit 
    (id, offenceId, forfeitId) 
VALUES 
    ('f9037ecc-e418-4171-bbd0-2ff39fe2b901', '0da55b8f-d8b4-46cf-b018-9c96f7ee56a1', 'dd4e93a0-d5c8-416b-afc8-215fd96a8040'), 
    ('6e7b3fdc-0fae-48d1-879c-8d3d2725ebdc', '46b2f62b-4984-4d89-b675-b4924ff6b8cb', 'f2a45fbf-f1f5-45f2-ba4a-a299cb263888'),
    ('d2cb7cb0-cc6a-417d-b70a-1d79d6addf1d', 'cae9540b-28a9-42f4-8d2a-b20dfee6aad0', '7f604bd3-4ddf-4083-af1a-480fb07875b6'),
    ('6e7e8bfd-de71-44f8-a72b-8ac7250e84bf', '8bfc5283-560b-44a1-a3b3-fdb4c66c3748', 'c455a469-41de-4547-81ca-146c72650d65'),
    ('342e346a-6bb1-4df3-850e-9cff9f9555d6', '8bfc5283-560b-44a1-a3b3-fdb4c66c3748', '7f604bd3-4ddf-4083-af1a-480fb07875b6');

INSERT INTO finesboard.public.finesboardUser 
    (id, userLoginId, finesboardId) 
VALUES 
    ('2590324b-0525-4ee4-af76-286608a08f82', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('661b66e6-ee7e-4fc0-b8c4-6c1881897ea2', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511'),
    ('36237bd8-fbf7-4b7e-ac5e-bff020e67e53', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665'),
    ('6e6cc25b-ba4f-4241-ac4e-b8bc1e5ff43d', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba');
    
----insert_finesboard_public_fines.----
INSERT INTO finesboard.public.fine 
    (id, targetId, creatorId, reason, finesboardId, offenceId, forfeitId, amount) 
VALUES 
    ('537427F8-E2A2-42B3-8E6E-AA26011100DD', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', 'Red Build', '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665', '0da55b8f-d8b4-46cf-b018-9c96f7ee56a1', 'dd4e93a0-d5c8-416b-afc8-215fd96a8040', 1),
    ('00AF0F21-7224-49AB-B83B-AA2800A12BDB', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', 'Bad Joke', '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665', '0da55b8f-d8b4-46cf-b018-9c96f7ee56a1', 'dd4e93a0-d5c8-416b-afc8-215fd96a8040', 1),
    ('5B85E84E-6333-45DE-AAB6-AA2800A12BDB', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', 'Swearing', '4ecf9c20-27fd-4d4d-84dd-a0df7ac11665', '0da55b8f-d8b4-46cf-b018-9c96f7ee56a1', 'dd4e93a0-d5c8-416b-afc8-215fd96a8040', 1),
    ('3895F8F3-FA62-4F60-890B-AA2800A12BDB', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', '46b2f62b-4984-4d89-b675-b4924ff6b8cb', 'f2a45fbf-f1f5-45f2-ba4a-a299cb263888', 0.2),
    ('5667072E-DA3A-4436-9C91-AA2800A12BDB', '501d1a52-dae3-41bc-b312-05175fd68f3a', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', '46b2f62b-4984-4d89-b675-b4924ff6b8cb', 'f2a45fbf-f1f5-45f2-ba4a-a299cb263888', 0.2),
    ('6567072E-DA3A-4436-9C91-AA2800A12BDB', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '501d1a52-dae3-41bc-b312-05175fd68f3a', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', 'cae9540b-28a9-42f4-8d2a-b20dfee6aad0', '7f604bd3-4ddf-4083-af1a-480fb07875b6', 0.5),
    ('cf4c0ccf-7bc6-4d8c-b6b8-8cc47d785749', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '501d1a52-dae3-41bc-b312-05175fd68f3a', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', 'cae9540b-28a9-42f4-8d2a-b20dfee6aad0', '7f604bd3-4ddf-4083-af1a-480fb07875b6', 0.5),
    ('431d5743-e54d-44ea-800d-4d6a9181008c', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '501d1a52-dae3-41bc-b312-05175fd68f3a', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', '8bfc5283-560b-44a1-a3b3-fdb4c66c3748', 'c455a469-41de-4547-81ca-146c72650d65', 1),
    ('6bed683b-d574-4fa5-b47e-032097cb0ae5', 'd871f442-e6b8-456c-96b9-77b8f0ef8c5e', '501d1a52-dae3-41bc-b312-05175fd68f3a', '', 'aa2eca98-42cd-4b3b-b6aa-220fa979d511', '8bfc5283-560b-44a1-a3b3-fdb4c66c3748', 'c455a469-41de-4547-81ca-146c72650d65', 1);


