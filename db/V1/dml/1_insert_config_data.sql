----insert_finesboard_public_finesboardStataus.sql----
INSERT INTO finesboard.public.status
    (id, description) 
VALUES 
    ('1', 'Active'), 
    ('0', 'Inactive');
    
----insert_finesboard_public_finesboardStataus.sql----
INSERT INTO finesboard.public.finesboardStatus
    (id, description) 
VALUES 
    ('79b65eb7-aebe-4faf-87ba-f7a61ed073da', 'Pending'), 
    ('e55327ed-2e1b-4135-818a-608e09c7630c', 'Active'),
    ('a996a5e0-1f10-471b-96e4-ae50238b7001', 'Closed'),
    ('df216125-9ab9-4fa0-9267-c9facc0c7c18', 'Archived');

----insert_finesboard_public_roleTypeGroup.sql----
INSERT INTO finesboard.public.userRoleTypeGroup
    (id, description) 
VALUES 
    ('ddf9044f-e9ba-4088-b124-ad5e9fa88736', 'Platform');

----insert_finesboard_public_userRoleType.sql----
INSERT INTO finesboard.public.userRoleType
    (id, code, description, userRoleTypeGroupId) 
VALUES 
    ('c86263b1-7525-487e-a907-b02ff69b7e21', 'ADMIN', 'Admin', 'ddf9044f-e9ba-4088-b124-ad5e9fa88736'); 

----insert_finesboard_public_userLogin.sql----
INSERT INTO finesboard.public.userLogin 
    (id, username, password) 
VALUES 
    ('03db7962-a37a-4b35-93ac-30de0eaeb0ce', 'admin', 'AQAAAAEAACcQAAAAEPrwWffAsZuel8+BG9PaTVErpRixWclPzd8fIKB/56yMXdh58m22P1Pfq9BHBYYOwQ==');

----insert_finesboard_public_userRole.sql----
INSERT INTO finesboard.public.userRole 
    (id, userId, userRoleTypeId, linkId) 
VALUES 
    ('7b5990af-7c53-457f-ad3f-8e1ebce81329', '03db7962-a37a-4b35-93ac-30de0eaeb0ce', 'c86263b1-7525-487e-a907-b02ff69b7e21', null);
