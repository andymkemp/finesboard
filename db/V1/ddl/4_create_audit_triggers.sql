----audit triggers----
CREATE SCHEMA audit;

REVOKE ALL ON SCHEMA audit FROM public;

----fines----
CREATE SEQUENCE audit_fine_id_seq AS INTEGER;

CREATE TABLE finesboard.audit.fine (
    id INTEGER PRIMARY KEY DEFAULT NEXTVAL('audit_fine_id_seq'),
    operation CHAR(1) NOT NULL,
    stamp TIMESTAMP NOT NULL,
    dbuser text NOT NULL,
    fineId UUID NOT NULL,
    targetId UUID NOT NULL,
    creatorId UUID NOT NULL,
    reason VARCHAR(255),
    amount DECIMAL(12,2) NOT NULL,
    finesboardId UUID NOT NULL,
    offenceId UUID NOT NULL,
    forfeitId UUID NOT NULL,
    createdDate TIMESTAMP NOT NULL
);

REVOKE ALL ON finesboard.audit.fine FROM public;

CREATE OR REPLACE FUNCTION finesboard.public.process_fine_inserted_audit() RETURNS TRIGGER AS $fine_inserted_audit$
    BEGIN
        IF TG_WHEN <> 'AFTER' THEN
            RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
        END IF;

        INSERT INTO finesboard.audit.fine (
	        operation,
		    stamp,
		    dbuser,
		    fineId,
		    targetId,
		    creatorId,
		    reason,
		    amount,
		    finesboardId,
		    offenceId,
		    forfeitId,
		    createdDate
        )
        SELECT 
       		'I', now(), user, n.* FROM new_table n;
        RETURN NULL;
    END;
$fine_inserted_audit$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION finesboard.public.process_fine_updated_audit() RETURNS TRIGGER AS $fine_updated_audit$
    BEGIN
        IF TG_WHEN <> 'AFTER' THEN
            RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
        END IF;
        
        INSERT INTO finesboard.audit.fine (
	        operation,
		    stamp,
		    dbuser,
		    fineId,
		    targetId,
		    creatorId,
		    reason,
		    amount,
		    finesboardId,
		    offenceId,
		    forfeitId,
		    createdDate
        )
        SELECT 
       		'U', now(), user, n.* FROM new_table n;
        RETURN NULL;
    END;
$fine_updated_audit$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION finesboard.public.process_fine_deleted_audit() RETURNS TRIGGER AS $fine_deleted_audit$
    BEGIN     
        IF TG_WHEN <> 'AFTER' THEN
            RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
        END IF;
        
        INSERT INTO finesboard.audit.fine (
	        operation,
		    stamp,
		    dbuser,
		    fineId,
		    targetId,
		    creatorId,
		    reason,
		    amount,
		    finesboardId,
		    offenceId,
		    forfeitId,
		    createdDate
        )
        SELECT 
       		'D', now(), user, o.* FROM old_table o;
        RETURN NULL;
    END;
$fine_deleted_audit$ LANGUAGE plpgsql;

CREATE TRIGGER fine_audit_ins
    AFTER INSERT ON finesboard.public.fine
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE PROCEDURE process_fine_inserted_audit();

CREATE TRIGGER fine_audit_upd
    AFTER UPDATE ON finesboard.public.fine
    REFERENCING OLD TABLE AS old_table NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE PROCEDURE process_fine_updated_audit();

CREATE TRIGGER fine_audit_del
    AFTER DELETE ON finesboard.public.fine
    REFERENCING OLD TABLE AS old_table
    FOR EACH STATEMENT EXECUTE PROCEDURE process_fine_deleted_audit();
    
