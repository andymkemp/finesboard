----status----
CREATE TABLE finesboard.public.status (
    id INT PRIMARY KEY,
    description VARCHAR(35) NOT NULL
);

----user----
CREATE TABLE finesboard.public.userLogin (
    id UUID PRIMARY KEY,
    username VARCHAR(35) NOT NULL,
    password  VARCHAR(256) NOT NULL,
    status INT NOT NULL REFERENCES finesboard.public.status(id) DEFAULT 1,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);
CREATE UNIQUE INDEX userlogin_username_unique_idx on userLogin (LOWER(username));

----roleTypeGroup----
CREATE TABLE finesboard.public.userRoleTypeGroup (
    id UUID PRIMARY KEY,
    description VARCHAR(35) NOT NULL
);

----roleType----
CREATE TABLE finesboard.public.userRoleType (
    id UUID PRIMARY KEY,
    code varchar(10) NOT Null,
    description VARCHAR(35) NOT NULL,
    userRoleTypeGroupId UUID NOT NULL REFERENCES finesboard.public.userRoleTypeGroup(id)
);
CREATE UNIQUE INDEX userroletype_username_unique_idx on userRoleTYpe (LOWER(code));

----role----
CREATE TABLE finesboard.public.userRole (
    id UUID PRIMARY KEY,
    userId UUID NOT NULL REFERENCES finesboard.public.userLogin(id),
    userRoleTypeId UUID NOT NULL REFERENCES finesboard.public.userRoleType(id),
    linkId UUID,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----finesboardStatus----
CREATE TABLE finesboard.public.finesboardStatus (
    id UUID PRIMARY KEY,
    description VARCHAR(35) NOT NULL
);

----finesboard----
CREATE TABLE finesboard.public.finesboard (
    id UUID PRIMARY KEY,
    finesboardStatusId UUID NOT NULL REFERENCES finesboard.public.finesboardStatus(id), 
    name VARCHAR(35) NOT NULL,
    currency VARCHAR(35) NOT NULL,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----offence----
CREATE TABLE finesboard.public.offence (
    id UUID PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    finesboardId UUID REFERENCES finesboard.public.finesboard(id) ON DELETE CASCADE,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----forfeit----
CREATE TABLE finesboard.public.forfeit (
    id UUID PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    amount DECIMAL(12,2) NOT NULL,
    finesboardId UUID REFERENCES finesboard.public.finesboard(id) ON DELETE CASCADE,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----offenceForfeit----
CREATE TABLE finesboard.public.offenceForfeit (
    id UUID PRIMARY KEY,
    offenceId UUID REFERENCES finesboard.public.offence(id)  ON DELETE CASCADE,
    forfeitId UUID REFERENCES finesboard.public.forfeit(id)  ON DELETE CASCADE,
    status INT NOT NULL REFERENCES finesboard.public.status(id) DEFAULT 1,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----finesboardUser----
CREATE TABLE finesboard.public.finesboardUser (
    id UUID PRIMARY KEY,
    userLoginId UUID NOT NULL REFERENCES finesboard.public.userLogin(id),
    finesboardId UUID NOT NULL REFERENCES finesboard.public.finesboard(id),
    status INT NOT NULL REFERENCES finesboard.public.status(id) DEFAULT 1,
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);

----fines----
CREATE TABLE finesboard.public.fine (
    id UUID PRIMARY KEY,
    targetId UUID REFERENCES finesboard.public.userLogin(id),
    creatorId UUID REFERENCES finesboard.public.userLogin(id),
    reason VARCHAR(255),
    amount DECIMAL(12,2) NOT NULL,
    finesboardId UUID REFERENCES finesboard.public.finesboard(id),
    offenceId UUID REFERENCES finesboard.public.offence(id),
    forfeitId UUID REFERENCES finesboard.public.forfeit(id),
    createdDate TIMESTAMP NOT NULL DEFAULT NOW()
);
