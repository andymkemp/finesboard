CREATE OR REPLACE FUNCTION finesboard.public.delete_finesboard(finesboardId UUID)
RETURNS int
AS $$
BEGIN		
	IF EXISTS (SELECT 1
		FROM
			finesboard.public.finesboard fb
	    	JOIN finesboard.public.finesboardStatus fs ON fb.finesboardStatusId = fs.id
	    WHERE 
	    	fb.id = finesboardId
	    	AND fs.description = 'Pending'
	)
	THEN		
		DELETE FROM finesboard.public.finesboard WHERE id = finesboardId;
	ELSE
		UPDATE finesboard.public.finesboard 
		SET finesboardStatusId = (SELECT id FROM finesboard.public.finesboardStatus WHERE DESCRIPTION = 'Closed') 
		WHERE id = finesboardId;
	END IF;
	RETURN 1;
End;
$$ LANGUAGE plpgsql;