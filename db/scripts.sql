select uuid_generate_v4()

select ug.name as userGroup, f.name as finesboard from 
public.userGroup ug
join public.finesboard f on ug.id = f.userGroupId
order by ug.name;

select ug.name as userGroup, u.username as user from 
public.userGroup ug
join public.member m on ug.id = m.userGroupId
join public.user u on m.userId = u.id
order by ug.name;

select ug.name as userGroup, f.name as finesboard, u.username as user, urt.description as role from 
public.userGroup ug
join public.finesboard f on ug.id = f.userGroupId
left join public.participant p on f.id = p.finesboardId
left join public.member m on p.memberId = m.id
left join public.user u on m.userId = u.id
left join public.userRole ur on u.id = ur.userId and f.id = ur.linkId
left join public.userRoleType urt on ur.userRoleTypeId = urt.id
order by f.name;

SELECT
    fb.id as finesboardId, o.id as offenceId, ff.id as forfeitId
FROM 
    finesboard.public.fine f
    JOIN finesboard.public.userlogin t ON f.targetId = t.id
    JOIN finesboard.public.userlogin c ON f.creatorId = c.id
    JOIN finesboard.public.finesboard fb ON f.finesboardId = fb.id
    JOIN finesboard.public.finesboardStatus ft ON fb.finesboardStatusId = ft.id
    JOIN finesboard.public.offence o ON f.offenceId = o.id
    JOIN finesboard.public.forfeit ff ON f.forfeitId = ff.id
    JOIN finesboard.public.finesboardUser fbu on fb.id = fbu.finesboardId
    JOIN finesboard.public.userLogin ul on fbu.userLoginId = ul.id
WHERE
    ul.username = 'user1';

SELECT
    ul.*
FROM 
    finesboard.public.finesboard fb
    JOIN finesboard.public.finesboardUser fbu on fb.id = fbu.finesboardId
    JOIN finesboard.public.userLogin ul on fbu.userLoginId = ul.id
WHERE
    fb.id = 'aa2eca98-42cd-4b3b-b6aa-220fa979d511';

---- test audit triggers----
 insert into finesboard.public.fine (
 		id,
	    targetId,
	    creatorId,
	    reason,
	    amount,
	    finesboardId,
	    offenceId,
	    forfeitId
    ) 
    values (
    	uuid_generate_v4(),
	    '501d1a52-dae3-41bc-b312-05175fd68f3a',
	    'd871f442-e6b8-456c-96b9-77b8f0ef8c5e',
	    'just because',
	    1,
	    'aa2eca98-42cd-4b3b-b6aa-220fa979d511',
	    '46b2f62b-4984-4d89-b675-b4924ff6b8cb',
	    'f2a45fbf-f1f5-45f2-ba4a-a299cb263888'
    );
    
    update finesboard.public.fine set amount = 10 where id = 'e57be2a5-f0ec-43a5-b987-ea1f5ae1d94c';

    delete from finesboard.public.fine where id = 'e57be2a5-f0ec-43a5-b987-ea1f5ae1d94c';
    
    select * from finesboard.public.fine;
    select * from finesboard.audit.fine;