interface Element {
        sendInput(value: string): void;
}

Element.prototype.sendInput = function(value: string): void {
    this.value = value;
    this.dispatchEvent(new Event('input'));
};
