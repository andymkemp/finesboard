import { convertToParamMap, ParamMap, Params, ActivatedRouteSnapshot } from '@angular/router';
import { ReplaySubject } from 'rxjs';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 */
export class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private subject = new ReplaySubject<ParamMap>();
  snapshot: { paramMap: ParamMap, parent: { paramMap: ParamMap } };

  constructor(initialParams?: Params) {
    this.setParamMap(initialParams);
  }

  /** The mock paramMap observable */
  readonly paramMap = this.subject.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params) {
    const paramMap =  convertToParamMap(params);
    this.subject.next(paramMap);
    this.snapshot = { paramMap, parent: { paramMap }} ;
  }
}
