import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { RolesEnum } from '../enums/roles';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.less']
})
export class TopNavComponent implements OnInit {

  isAuthenticated: Boolean;
  isAdmin: Boolean;
  username: string;
  links: link[]
  adminLinks = [
    {name: "Add Fine", route: "/fines/add"},
    {name: "Finesboards", route: "/finesboards"},
    {name: "Users", route: "/users"}
  ];
  userLinks = [
    {name: "Add Fine", route: "/fines/add"},
    {name: "Finesboards", route: "/finesboards"}
  ];


  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit() {
    this.authService.currentAuth.subscribe(value => {
      if(value) {
        this.isAuthenticated = true;
        this.username = value.unique_name;
        this.isAdmin = this.authService.isAuthorized([RolesEnum.Admin]);
        if(this.isAdmin) {
          this.links = this.adminLinks;
        } else {
          this.links = this.userLinks;
        }
      } else {
        this.isAuthenticated = false;
      }
    });
  }

  onLogout() {
    this.authService.logout();
    this.username = null;
    this.router.navigate(['/login']);
  }
}

interface link { 
  name: string;
  route: string;
}