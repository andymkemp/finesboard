import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule } from '@angular/material';
import { TopNavComponent } from './top-nav.component';
import { By } from '@angular/platform-browser';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive-stub';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../auth/jwt.interceptor';

fdescribe('TopNavComponent', () => {
  let component: TopNavComponent;
  let fixture: ComponentFixture<TopNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ 
        HttpClientTestingModule, 
        NoopAnimationsModule, MatButtonModule, MatToolbarModule 
      ],
      declarations: [ 
        TopNavComponent, RouterLinkDirectiveStub 
      ],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have 1 links in top nav', () => {
    const nav = fixture.debugElement.queryAll(By.css('a'));
    expect(nav.length).toBe(1);
  });

  it('should have finesboards link', () => {
    const nav = fixture.debugElement.queryAll(By.css('a'));
    const navItem = nav[0];
    expect(navItem.nativeElement.textContent).toBe('Finesboards');
  });

  it('should route to finesboards component when clicking fines link', () => {
    const nav = fixture.debugElement.queryAll(By.css('a'));
    const navItem = nav[0];
    const routerLink = navItem.injector.get(RouterLinkDirectiveStub);
    expect(routerLink.navigatedTo).toBeNull('should not have navigated yet');
    navItem.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(routerLink.navigatedTo).toBe('/finesboards');
  });
});
