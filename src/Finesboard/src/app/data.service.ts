import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) { }

  private host = environment.finesApiUrl;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
  };

  get<T>(endpoint: string): Observable<T> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.get<T>(url.toString())
    .pipe(
      catchError(this.handleError)
    );
  }

  delete<T>(endpoint: string): Observable<T> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.delete<T>(url.toString())
    .pipe(
      catchError(this.handleError)
    );
  }

  insert<T>(endpoint: string, item: T ): Observable<T> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.post<T>(url.toString(), item)
    .pipe(
      catchError(this.handleError)
    );
  }

  post<T,U>(endpoint: string, item: T ): Observable<U> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.post<U>(url.toString(), item)
    .pipe(
      catchError(this.handleError)
    );
  }

  update<T>(endpoint: string, item: T ): Observable<T> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.put<T>(url.toString(), item, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  put<T,U>(endpoint: string, item: T ): Observable<U> {
    const url = new URL(endpoint, this.host);
    return this.httpClient.put<U>(url.toString(), item, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: any): Observable<never> {
    // TODO: send the error to remote logging infrastructure
    console.error(error);
    return throwError(error);
  }
}
