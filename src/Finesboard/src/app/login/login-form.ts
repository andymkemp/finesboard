import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authenticate } from '../models/authenticate';

export class LoginForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private login: Authenticate) {
        this.form = this.formBuilder.group({
            username: [login.username, Validators.required],
            password: [login.password, Validators.required],
          });
    }

    get username() { return this.form.get('username'); }
    get password() { return this.form.get('password'); }
}
