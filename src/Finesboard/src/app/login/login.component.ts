import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { LoginForm } from './login-form';
import { Authenticate } from '../models/authenticate';
import { MessageService } from '../message.service';
import { FinesboardDefaultErrorStateMatcher } from '../finesboard-error-state-matcher';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  message: string;
  loginForm: LoginForm;
  matcher = new FinesboardDefaultErrorStateMatcher();
  returnUrl: string;
  hide: Boolean;

  constructor(public authService: AuthService,
              public router: Router,
              private route: ActivatedRoute,
              private messaging: MessageService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.hide = true;
    this.loginForm = new LoginForm(this.formBuilder, new Authenticate());
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
  }

  onSubmit(form) {
    if (this.loginForm.form.invalid) {
      return;
    }
    const loginDetails: Authenticate = this.loginForm.form.value;
    this.authService.login(loginDetails).subscribe(() => {
      if(this.returnUrl) {
        this.router.navigate([this.returnUrl], {replaceUrl:true});
      } else {
        this.router.navigate(['/']);
      }
    }, () => {
      this.messaging.notify('Failure: Unable to login');
    });
  }

  logout() {
    this.authService.logout();
  }
  
  onToggleHide() {
    this.hide = !this.hide;
  }

}
