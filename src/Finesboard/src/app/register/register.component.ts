import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import { FormBuilder } from '@angular/forms';
import { FinesboardDefaultErrorStateMatcher, FinesboardConfirmPasswordErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';
import { RegistrationForm } from './registration-form';
import { Authenticate } from 'src/app/models/authenticate';
import { Register } from '../models/register';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  message: string;
  registrationForm: RegistrationForm;
  matcher = new FinesboardDefaultErrorStateMatcher();
  confirmPasswordMatcher = new FinesboardConfirmPasswordErrorStateMatcher();
  returnUrl: string;
  hidePassword: Boolean;
  hidePasswordConfirmation: Boolean;

  constructor(public authService: AuthService,
              public router: Router,
              private route: ActivatedRoute,
              private messaging: MessageService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.hidePassword = true;
    this.hidePasswordConfirmation = true;
    this.registrationForm = new RegistrationForm(this.formBuilder, new Authenticate());
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
  }

  onSubmit(form) {
    if (this.registrationForm.form.invalid) {
      return;
    }
    const RegistrationDetails: Register = {
      username: this.registrationForm.username.value,
      password: this.registrationForm.password.value
    };
    this.authService.register(RegistrationDetails).subscribe(() => {
      this.router.navigate(['/login']);
      this.messaging.notify('User registered successful');
    }, () => {
      this.messaging.notify('Failure: Unable to register');
    });
  }
  
  onToggleHidePassword() {
    this.hidePassword = !this.hidePassword;
  }
  
  onToggleHidePasswordPasswordConfirmation() {
    this.hidePasswordConfirmation = !this.hidePasswordConfirmation;
  }
}
