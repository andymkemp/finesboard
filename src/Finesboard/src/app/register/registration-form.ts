import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Authenticate } from '../models/authenticate';
import { passwordMatchValidator } from '../form-validators/password-match.directive'

export class RegistrationForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private login: Authenticate) {
        this.form = this.formBuilder.group({
            username: [login.username, Validators.required],
            password: [login.password, Validators.required],
            passwordConfirmation: [null, Validators.required],
          }, { validators: passwordMatchValidator });
    }

    get username() { return this.form.get('username'); }
    get password() { return this.form.get('password'); }
    get passwordConfirmation() { return this.form.get('passwordConfirmation'); }
}