import { Injectable } from '@angular/core';
import { Fine } from './models/fine';
import { Observable, of } from 'rxjs';
import { DataService } from './data.service';
import { FinesboardDetails } from './models/finesboard-details';
import { FineDetails } from './models/fine-details';
import { OffenceDetails } from './models/offence-details';
import { Forfeit } from './models/forfeit';
import { User } from './models/user';
import { ChangePassword } from './models/change-password';

@Injectable({
  providedIn: 'root'
})
export class FineService {

  constructor(private dataService: DataService) { }

  getFines(): Observable<Fine[]> {
      return this.dataService.get<Fine[]>(`/api/fines`);
  }

  getFine(fineId: string): Observable<Fine> {
    return this.dataService.get<Fine>(`/api/fines/${fineId}`);
  }

  addFine(fine: Fine): Observable<Fine> {
    return this.dataService.insert<Fine>(`/api/finesboards/${fine.finesboardId}/fines`, fine) as Observable<Fine>;
  }

  deleteFine(fineId: string): Observable<void> {
    return this.dataService.delete(`/api/fines/${fineId}`) as Observable<void>;
  }

  updateFine(fine: Fine): Observable<Fine> {
    return this.dataService.update<Fine>(`/api/fines/${fine.id}`, fine) as Observable<Fine>;
  }

  getFinesboards(): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>('/api/finesboards');
  }

  getActiveFinesboards(): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>('/api/finesboards?status=Active');
  }

  getFinesboard(finesboardId: string): Observable<FinesboardDetails> {
    return this.dataService.get<FinesboardDetails>(`/api/finesboards/${finesboardId}`);
  }

  addFinesboard(finesboard: FinesboardDetails): Observable<FinesboardDetails> {
    return this.dataService.insert<FinesboardDetails>(`/api/finesboards`, finesboard);
  }

  getFinesboardFines(finesboardId: string): Observable<FineDetails[]> {
    return this.dataService.get<FineDetails[]>(`/api/finesboards/${finesboardId}/fines`);
  }

  addOffence(offence: OffenceDetails): Observable<OffenceDetails> {
    return this.dataService.insert<OffenceDetails>(`/api/finesboards/${offence.finesboardId}/offences`, offence);
  }

  getForfeits(finesboardId: string): Observable<Forfeit[]> {
    return this.dataService.get<Forfeit[]>(`/api/finesboards/${finesboardId}/forfeits`);
  }

  addForfeit(forfeit: Forfeit): Observable<Forfeit> {
    return this.dataService.insert<Forfeit>(`/api/finesboards/${forfeit.finesboardId}/forfeits`, forfeit);
  }

  getForfeit(finesboardId: string, forfeitId: string): Observable<Forfeit> {
    return this.dataService.get<Forfeit>(`/api/finesboards/${finesboardId}/forfeits/${forfeitId}`);
  }

  updateForfeit(forfeit: Forfeit): Observable<Forfeit> {
    return this.dataService.update<Forfeit>(`/api/finesboards/${forfeit.finesboardId}/forfeits/${forfeit.id}`
    , forfeit) as Observable<Forfeit>;
  }

  activateFinesboard(finesboardId: string): Observable<FinesboardDetails> {
    return this.dataService.post<any,FinesboardDetails>(`/api/finesboards/${finesboardId}/activate`, {});
  }
  
  deactivateFinesboard(finesboardId: string): Observable<FinesboardDetails> {
    return this.dataService.post<any,FinesboardDetails>(`/api/finesboards/${finesboardId}/deactivate`, {});
  }

  getFinesboardUsers(finesboardId: string): Observable<User[]> {
    return this.dataService.get<User[]>(`/api/finesboards/${finesboardId}/users`);
  }

  getUsers(): Observable<User[]> {
    return this.dataService.get<User[]>(`/api/users`);
  }

  getUserFinesboards(userId: string): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>(`/api/users/${userId}/finesboards`);
  }

  getUserActiveFinesboards(userId: string): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>(`/api/users/${userId}/finesboards?status=Active`);
  }

  joinFinesboard(finesboardId: string): Observable<void> {
    return this.dataService.post(`/api/user/finesboards/${finesboardId}/join`, null);
  }

  leaveFinesboard(finesboardId: string): Observable<void> {
    return this.dataService.post(`/api/user/finesboards/${finesboardId}/leave`, null);
  }

  getUserDetails(userId: string): Observable<User> {
    return this.dataService.get(`/api/users/${userId}`);
  }

  resetPassword(userId: string, password: string) {
    const changePassword = new ChangePassword();
    changePassword.newPassword = password;
    return this.dataService.post(`/api/users/${userId}/resetpassword`, changePassword);
  }

  changePassword(password: string) {
    const changePassword = new ChangePassword();
    changePassword.newPassword = password;
    return this.dataService.post(`/api/user/changepassword`, changePassword);
  }

  getMyDetails(): Observable<User> {
    return this.dataService.get(`/api/user/`);
  }

  getMyFinesboards(): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>(`/api/user/finesboards`);
  }

  getMyActiveFinesboards(): Observable<FinesboardDetails[]> {
    return this.dataService.get<FinesboardDetails[]>(`/api/user/finesboards?status=Active`);
  }

  getFinesboardFine(finesboardId: string, fineId: string): Observable<FineDetails> {
    return this.dataService.get<FineDetails>(`api/finesboards/${finesboardId}/fines/${fineId}`);
  }

  deleteFinesboard(finesboardId: string): Observable<void> {
    return this.dataService.delete(`api/finesboards/${finesboardId}`);
  }

  linkForfeitToOffence(finesboardId: string, offenceId: string, forfeitId: string): Observable<OffenceDetails> {
    return this.dataService.post<any, OffenceDetails>(`/api/finesboards/${finesboardId}/offences/${offenceId}/forfeits/${forfeitId}`, {});
  } 

  unlinkForfeitToOffence(finesboardId: string, offenceId: string, forfeitId: string): Observable<OffenceDetails> {
    return this.dataService.delete(`/api/finesboards/${finesboardId}/offences/${offenceId}/forfeits/${forfeitId}`);
  } 

  activateOffenceForfeit(finesboardId: string, offenceId: string, forfeitId: string): Observable<OffenceDetails> {
    return this.dataService.post<any, OffenceDetails>(`/api/finesboards/${finesboardId}/offences/${offenceId}/forfeits/${forfeitId}/activate`, {});
  } 

  deactivateOffenceForfeit(finesboardId: string, offenceId: string, forfeitId: string): Observable<OffenceDetails> {
    return this.dataService.post<any, OffenceDetails>(`/api/finesboards/${finesboardId}/offences/${offenceId}/forfeits/${forfeitId}/deactivate`, {});
  } 

  deleteOffence(finesboardId: string, offenceId: string): Observable<void> {
    return this.dataService.delete(`api/finesboards/${finesboardId}/offences/${offenceId}`);
  }

  deleteForfeit(finesboardId: string, forfeitId: string): Observable<void> {
    return this.dataService.delete(`api/finesboards/${finesboardId}/forfeits/${forfeitId}`);
  }
}
