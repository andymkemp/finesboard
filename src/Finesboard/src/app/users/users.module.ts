import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users/users.component';
import { UsersListComponent } from './users-list/users-list.component';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatFormFieldModule,
  MatInputModule, MatIconModule, MatSnackBarModule, MatSelectModule, MatOptionModule, MatExpansionModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConfigureUserComponent } from './configure-user/configure-user.component';

@NgModule({
  declarations: [UsersComponent, UsersListComponent, ConfigureUserComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatSelectModule,
    MatOptionModule,
    MatExpansionModule
  ]
})
export class UsersModule { }
