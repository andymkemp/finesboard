import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UsersListComponent } from './users-list/users-list.component';
import { AuthGuard } from '../auth/auth.guard';
import { ConfigureUserComponent } from './configure-user/configure-user.component';
import { RoleGuard } from '../auth/role.guard';
import { RolesEnum } from '../enums/roles';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '', 
        canActivate: [RoleGuard],
        data: { acceptRoles: [RolesEnum.Admin] },
        component: UsersListComponent,
      },
      {
        path: ':userId/configuration', 
        canActivate: [RoleGuard],
        data: { acceptRoles: [RolesEnum.Admin] },
        component: ConfigureUserComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
