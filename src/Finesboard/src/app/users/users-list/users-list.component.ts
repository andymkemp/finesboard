import { Component, OnInit } from '@angular/core';
import { FineService } from '../../fine.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from 'src/app/message.service';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.less']
})
export class UsersListComponent implements OnInit {

  constructor(private fineService: FineService,
    private route: ActivatedRoute,
    private location: Location,
    private messaging: MessageService,
    private authService: AuthService) { }

  users: User[];
  displayColumns: string[];

  ngOnInit() {
    this.getUsers();
    this.displayColumns = ['username'];
  }

  getUsers(): void {
    this.fineService.getUsers().subscribe(
      users => {
        this.users = users;
      },
      (err) => {
        this.messaging.notify(`Error: ${err}`);
      }
    );
  }
}
