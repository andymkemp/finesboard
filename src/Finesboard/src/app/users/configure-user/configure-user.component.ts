import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FineService } from 'src/app/fine.service';
import { Location } from '@angular/common';
import { MessageService } from 'src/app/message.service';
import { ActivatedRoute } from '@angular/router';
import { FinesboardDefaultErrorStateMatcher, FinesboardConfirmPasswordErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';
import { FormBuilder } from '@angular/forms';
import { ChangePasswordForm } from './change-password-form';

@Component({
  selector: 'app-configure-user',
  templateUrl: './configure-user.component.html',
  styleUrls: ['./configure-user.component.less']
})
export class ConfigureUserComponent implements OnInit {

  user: User;
  matcher = new FinesboardDefaultErrorStateMatcher();
  confirmPasswordMatcher = new FinesboardConfirmPasswordErrorStateMatcher();
  hidePassword: Boolean;
  hidePasswordConfirmation: Boolean;
  changePasswordForm: ChangePasswordForm;

  constructor(private route: ActivatedRoute,
              private fineService: FineService,
              private messaging: MessageService,
              private location: Location,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.hidePassword = true;
    this.hidePasswordConfirmation = true;
    this.getUser();
    this.changePasswordForm = new ChangePasswordForm(this.formBuilder);
  }

  getUser(): void {
    const userId = this.route.snapshot.paramMap.get('userId');
    this.fineService.getUserDetails(userId).subscribe(user => {
      this.user = user;
    }, () => {
      this.messaging.notify(`Failure: Unable to get user with id: ${userId}`);
    });
  }

  onSubmit(form) {
    if (this.changePasswordForm.form.invalid) {
      return;
    }
    this.fineService.resetPassword(this.user.id, this.changePasswordForm.password.value).subscribe(() => {
      this.messaging.notify('Success: Password reset');
    }, () => {
      this.messaging.notify('Failure: Unable to reset password');
    });
  }
  
  onToggleHidePassword() {
    this.hidePassword = !this.hidePassword;
  }
  
  onToggleHidePasswordPasswordConfirmation() {
    this.hidePasswordConfirmation = !this.hidePasswordConfirmation;
  }

  goBack() {
    this.location.back();
  }
}
