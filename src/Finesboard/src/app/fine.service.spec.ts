import { TestBed } from '@angular/core/testing';
import { FineService } from './fine.service';
import { DataService } from './data.service';
import { Fine } from './models/fine';
import { of } from 'rxjs';
import { FinesboardDetails } from './models/finesboard-details';
import { FineDetails } from './models/fine-details';

describe('FineService', () => {
  let service: FineService;
  let dataService: { get: jasmine.Spy, delete: jasmine.Spy, insert: jasmine.Spy, update: jasmine.Spy };

  beforeEach(() => {
    const dataServiceSpy: {insert: jasmine.Spy} = jasmine.createSpyObj('DataService', ['get', 'delete', 'insert', 'update']);
    TestBed.configureTestingModule({
      providers: [
        FineService,
        { provide: DataService, useValue: dataServiceSpy}
      ]
    });
    service = TestBed.get(FineService);
    dataService = TestBed.get(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should delete fine', () => {
    const outputFine = { id: '1234567', reason: 'desc', targetId: '1', amount: 1, finesboardId: '7654321'} as Fine;
    dataService.delete.and.callFake((url: string) => {
      return of(outputFine);
    });
    service.deleteFine(outputFine.id).subscribe(() => {
      expect(dataService.delete.calls.count()).toBe(1, 'should call delete');
      expect(dataService.delete.calls.argsFor(0).length).toBe(1, 'should call delete with 1 argument');
      expect(dataService.delete.calls.argsFor(0)[0]).toBe(`/api/fines/${outputFine.id}`,
        'should call delete with /api/fines/:id endpoint');
    });
  });

  it('should select fine', () => {
    const outputFine = { id: '1234567', reason: 'desc', targetId: '1',
      amount: 1, finesboardId: '7654321'} as Fine;
    dataService.get.and.callFake((url: string) => {
      return of(outputFine);
    });
    service.getFine(outputFine.id).subscribe((value) => {
      expect(dataService.get.calls.count()).toBe(1, 'should call get');
      expect(dataService.get.calls.argsFor(0).length).toBe(1, 'should call get with 1 argument');
      expect(dataService.get.calls.argsFor(0)[0]).toBe(`/api/fines/${outputFine.id}`,
        'should call get with /api/fines/:id endpoint');
      expect(value).toEqual(outputFine, 'should output selected fine');
    });
  });

  it('should select fines for finesboard', () => {
    const outputFines = [{ id: '1234567', 
      user: { id: '1234567', username: 'user1' }, 
      creator: { id: '1234567', username: 'user2' },
      reason: 'desc',
      amount: 1,
      finesboard: { id: '1234567', name: 'finesboard'},
      offence: { id: '1234567', name: 'offence'},
      forfeit: { id: '1234567', name: 'forfeit'}
    } as FineDetails];
    dataService.get.and.callFake((url: string) => {
      return of(outputFines);
    });
    const finesboardId = '1234567';
    service.getFinesboardFines(finesboardId).subscribe((value) => {
      expect(dataService.get.calls.count()).toBe(1, 'should call get');
      expect(dataService.get.calls.argsFor(0).length).toBe(1, 'should call get with 1 argument');
      expect(dataService.get.calls.argsFor(0)[0]).toBe(`/api/finesboards/${finesboardId}/fines`,
      `should call get with /api/finesboards/${finesboardId}/fines endpoint`);
      expect(value).toEqual(outputFines, 'should output selected fines');
    });
  });

  it('should select all fines', () => {
    const outputFines = [{ id: '1234567', reason: 'desc', targetId: '1',
      amount: 1, finesboardId: '7654321'} as Fine];
    dataService.get.and.callFake((url: string) => {
      return of(outputFines);
    });
    const finesboardId = outputFines[0].finesboardId;
    service.getFines().subscribe((value) => {
      expect(dataService.get.calls.count()).toBe(1, 'should call get');
      expect(dataService.get.calls.argsFor(0).length).toBe(1, 'should call get with 1 argument');
      expect(dataService.get.calls.argsFor(0)[0]).toBe(`/api/fines`,
        `should call get with /api/fines endpoint`);
      expect(value).toEqual(outputFines, 'should output selected fines');
    });
  });

  it('should select finesboards', () => {
    const outputFines = [{ id: '1234567', name: 'name'} as FinesboardDetails];
    dataService.get.and.callFake((url: string) => {
      return of(outputFines);
    });
    service.getFinesboards().subscribe((value) => {
      expect(dataService.get.calls.count()).toBe(1, 'should call get');
      expect(dataService.get.calls.argsFor(0).length).toBe(1, 'should call get with 1 argument');
      expect(dataService.get.calls.argsFor(0)[0]).toBe(`/api/finesboards`, 'should call get with /api/finesboards endpoint');
      expect(value).toEqual(outputFines, 'should output selected fines');
    });
  });

  it('should add finesboard', () => {
    dataService.insert.and.callFake((url: string, finesboard: FinesboardDetails) => {
      finesboard.id = '1234567';
      return of(finesboard);
    });
    const inputFinesboard = { name: 'name' } as FinesboardDetails;
    const outputFinesboard = { id: '1234567', name: 'name' } as FinesboardDetails;
    service.addFinesboard(inputFinesboard).subscribe((value) => {
      expect(dataService.insert.calls.count()).toBe(1, 'should call insert');
      expect(dataService.insert.calls.argsFor(0).length).toBe(2, 'should call insert with 2 arguments');
      expect(dataService.insert.calls.argsFor(0)[0]).toBe('/api/finesboards', 'should call insert with /api/finesboards endpoint');
      expect(dataService.insert.calls.argsFor(0)[1]).toEqual(inputFinesboard, 'should call insert with list of finesboards');
      expect(value).toEqual(outputFinesboard, 'should output list of inserted finesboards');
    });
  });
});
