import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { passwordMatchValidator } from '../form-validators/password-match.directive'

export class ChangePasswordForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            password: [null, Validators.required],
            passwordConfirmation: [null, Validators.required],
          }, { validators: passwordMatchValidator });
    }

    get password() { return this.form.get('password'); }
    get passwordConfirmation() { return this.form.get('passwordConfirmation'); }
}