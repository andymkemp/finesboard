export class Forfeit {
    id: string;
    name: string;
    amount: number;
    finesboardId: string;
}
