export class OffenceForfeit {
    id: string;
    name: string;
    amount: number;
    status: number;
    finesboardId: string;
}
