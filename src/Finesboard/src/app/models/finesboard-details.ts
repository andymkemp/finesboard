import { OffenceDetails } from './offence-details';
import { FinesboardStatus } from './finesboard-status';
import { Forfeit } from './forfeit';

export class FinesboardDetails {
    id: string;
    name: string;
    status: FinesboardStatus;
    currency: string;
    offences: OffenceDetails[];
    forfeits: Forfeit[];
}
