import { OffenceForfeit } from './offence-forfeit';

export class OffenceDetails {
    id: string;
    name: string;
    finesboardId: string;
    forfeits: OffenceForfeit[];
}
