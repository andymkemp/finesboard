export class Fine {
    id: string;
    targetId: string;
    creatorId: string;
    reason: string;
    amount: number;
    finesboardId: string;
    offenceId: string;
    forfeitId: string;
}
