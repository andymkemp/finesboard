import { FinesboardDetails } from './finesboard-details';
import { OffenceDetails } from './offence-details';
import { Forfeit } from './forfeit';
import { User } from './user';

export class FineDetails {
    id: string;
    target: User;
    creator: User;
    reason: string;
    amount: number;
    finesboard: FinesboardDetails;
    offence: OffenceDetails;
    forfeit: Forfeit;
}
