import { TestBed } from '@angular/core/testing';

import { MessageService } from './message.service';
import { MatSnackBarModule, MatSnackBar } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MessageService', () => {
  let matSnackBarSpy: { open: jasmine.Spy };
  beforeEach(() => {
    matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);
    TestBed.configureTestingModule({
      imports: [ MatSnackBarModule, NoopAnimationsModule ],
      providers: [{ provide: MatSnackBar, useValue: matSnackBarSpy }],
    });
  });

  it('should be created', () => {
    const service: MessageService = TestBed.get(MessageService);
    expect(service).toBeTruthy();
  });

  it('should notify', () => {
    const service: MessageService = TestBed.get(MessageService);
    const notification = 'some notification';
    service.notify(notification);
    expect(matSnackBarSpy.open.calls.count()).toBe(1, 'should call open snack bar');
    expect(matSnackBarSpy.open.calls.argsFor(0).length).toBe(3, 'should call open snack bar with 3 arguments');
    expect(matSnackBarSpy.open.calls.argsFor(0)[0]).toBe(notification, '1st argument should be notification message');
    expect(matSnackBarSpy.open.calls.argsFor(0)[1]).toBe('Dismiss', '2nd argument should be dismiss action');
    // tslint:disable-next-line: max-line-length
    expect(matSnackBarSpy.open.calls.argsFor(0)[2]).toEqual({duration: 5000}, '3rd argument should be timeout duration of 5000 milliseconds');
  });
});
