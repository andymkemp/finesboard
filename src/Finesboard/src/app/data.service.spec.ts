import { TestBed } from '@angular/core/testing';
import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Fine } from './models/fine';
import { environment } from 'src/environments/environment';

describe('DataService', () => {
  let service: DataService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.get(DataService);
    // tslint:disable-next-line: deprecation
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should perform get', () => {
    const testData: Fine = new Fine();
    service.get<Fine>('/home').subscribe(data =>
      expect(data).toEqual(testData)
    );
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('GET');
    req.flush(testData);
    httpTestingController.verify();
  });

  it('should handle errors on get', () => {
    const emsg = 'Deliberate 404 error';
    spyOn(console, 'error');
    service.get<Fine>('/home').subscribe((data) => {
      fail('expect error to be handled');
    }, (error) => {
      expect(console.error).toHaveBeenCalled();
      expect(error.error).toBe(emsg);
    });
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('GET');
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
    httpTestingController.verify();
  });

  it('should perform insert', () => {
    const inputFine: Fine = new Fine();
    const outputFine: Fine = new Fine();
    service.insert<Fine>('/home', inputFine).subscribe(data =>
      expect(data).toEqual(outputFine)
    );
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('POST');
    req.flush(outputFine);
    httpTestingController.verify();
  });

  it('should handle errors on insert', () => {
    const emsg = 'Deliberate 404 error';
    const fine = new Fine();
    spyOn(console, 'error');
    service.insert<Fine>('/home', fine).subscribe((data) => {
      fail('expect error to be handled');
    }, (error) => {
      expect(console.error).toHaveBeenCalled();
      expect(error.error).toBe(emsg);
    });
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('POST');
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
    httpTestingController.verify();
  });

  it('should perform update', () => {
    const inputFine: Fine = new Fine();
    const outputFine: Fine = new Fine();
    service.update<Fine>('/home', inputFine).subscribe(data =>
      expect(data).toEqual(outputFine)
    );
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('PUT');
    req.flush(outputFine);
    httpTestingController.verify();
  });

  it('should handle errors on update', () => {
    const emsg = 'Deliberate 404 error';
    const fine = new Fine();
    spyOn(console, 'error');
    service.update<Fine>('/home', fine).subscribe((data) => {
      fail('expect error to be handled');
    }, (error) => {
      expect(console.error).toHaveBeenCalled();
      expect(error.error).toBe(emsg);
    });
    const expectedUrl = environment.finesApiUrl + 'home';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('PUT');
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
    httpTestingController.verify();
    expect(console.error).toHaveBeenCalled();
  });

  it('should perform delete', () => {
    const fine: Fine = new Fine();
    service.delete('/home/1').subscribe(data =>
      expect(data).toEqual(fine)
    );
    const expectedUrl = environment.finesApiUrl + 'home/1';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('DELETE');
    req.flush(fine);
    httpTestingController.verify();
  });

  it('should handle errors on delete', () => {
    const emsg = 'Deliberate 404 error';
    spyOn(console, 'error');
    service.delete('/home/1').subscribe((data) => {
      fail('expect error to be handled');
    }, (error) => {
      expect(console.error).toHaveBeenCalled();
      expect(error.error).toBe(emsg);
    });
    const expectedUrl = environment.finesApiUrl + 'home/1';
    const req = httpTestingController.expectOne(expectedUrl);
    expect(req.request.method).toEqual('DELETE');
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
    httpTestingController.verify();
    expect(console.error).toHaveBeenCalled();
  });
});
