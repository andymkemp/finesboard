import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinesboardsRoutingModule } from './finesboards-routing.module';
import { FinesboardsListComponent } from './finesboards-list/finesboards-list.component';
import { FinesboardsComponent } from './finesboards/finesboards.component';
import { AddFinesboardComponent } from './add-finesboard/add-finesboard.component';
import { MatButtonModule, MatToolbarModule, MatCardModule, MatFormFieldModule,
  MatInputModule, MatIconModule, MatSnackBarModule, MatTableModule, MatStepperModule, MatExpansionModule, 
  MatListModule, MatDividerModule, MatChipsModule, MatTabsModule, MatTooltipModule, MatDialogModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConfigureFinesboardComponent } from './configure-finesboard/configure-finesboard.component';
import { DisableIfUnauthorizedDirective } from '../auth/disable-if-unathorized.directive';

@NgModule({
  declarations: [
    FinesboardsComponent,
    FinesboardsListComponent,
    AddFinesboardComponent,
    ConfigureFinesboardComponent,
    DisableIfUnauthorizedDirective,
  ],
  imports: [
    CommonModule,
    FinesboardsRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatStepperModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatChipsModule,
    MatTabsModule,
    MatDialogModule,
  ],
  exports: [
    DisableIfUnauthorizedDirective,
  ],
})
export class FinesboardsModule { }
