import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinesboardsListComponent } from './finesboards-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatIconModule, MatTable } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterLinkDirectiveStub } from 'src/testing/router-link-directive-stub';
import { FineService } from '../../fine.service';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('FinesboardsListComponent', () => {
  let component: FinesboardsListComponent;
  let fixture: ComponentFixture<FinesboardsListComponent>;
  let finesServiceSpy: { getFinesboards: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatIconModule, RouterTestingModule ],
      declarations: [ FinesboardsListComponent, RouterLinkDirectiveStub ],
      providers: [
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFinesboards']) }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    finesServiceSpy = TestBed.get(FineService);
    finesServiceSpy.getFinesboards.and.returnValue(of([
      { id: '1', name: 'Cake Tick' },
      { id: '2', name: 'Finesboard' }
    ]));
    fixture = TestBed.createComponent(FinesboardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get finesboards', () => {
    expect(finesServiceSpy.getFinesboards).toHaveBeenCalledTimes(1);
  });

  it('should have title of all finesboards', () => {
    const title = fixture.debugElement.query(By.css('.title'));
    expect(title.nativeElement.textContent).toBe('All Finesboards');
  });

  it('should display table headers', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableHeaders = table.nativeElement.querySelectorAll('th');
    expect(tableHeaders.length).toBe(3);
    expect(tableHeaders[0].textContent).toBe('Name');
  });

  it('should display finesboard details', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    expect(tableRows.length).toBe(3); // including header
    let rowCells = tableRows[1].querySelectorAll('td');
    expect(rowCells.length).toBe(3);
    expect(rowCells[0].textContent).toBe('Cake Tick');
    expect(rowCells[1].textContent).toBe('edit');
    expect(rowCells[2].textContent).toBe('delete');
    rowCells = tableRows[2].querySelectorAll('td');
    expect(rowCells.length).toBe(3);
    expect(rowCells[0].textContent).toBe('Finesboard');
    expect(rowCells[1].textContent).toBe('edit');
    expect(rowCells[2].textContent).toBe('delete');
  });

  it('should have a link to the fines of each finesboard', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    let rowCells = tableRows[1].querySelectorAll('td');
    let finesLink = rowCells[0].querySelector('a');
    expect(finesLink.getAttribute('href')).toBe('/1/fines');
    rowCells = tableRows[2].querySelectorAll('td');
    finesLink = rowCells[0].querySelector('a');
    expect(finesLink.getAttribute('href')).toBe('/2/fines');
  });

  it('should have an edit button for each finesboard', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    let rowCells = tableRows[1].querySelectorAll('td');
    let editLink = rowCells[1].querySelector('a');
    expect(editLink.getAttribute('href')).toBe('/update/1');
    rowCells = tableRows[2].querySelectorAll('td');
    editLink = rowCells[1].querySelector('a');
    expect(editLink.getAttribute('href')).toBe('/update/2');
  });

  it('should have a delete button for each finesboard', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    let rowCells = tableRows[1].querySelectorAll('td');
    let editLink = rowCells[2].querySelector('a');
    expect(editLink.getAttribute('href')).toBe('/delete/1');
    rowCells = tableRows[2].querySelectorAll('td');
    editLink = rowCells[2].querySelector('a');
    expect(editLink.getAttribute('href')).toBe('/delete/2');
  });

  it('should have an add button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    const routerLink = button.injector.get(RouterLinkDirectiveStub);
    expect(routerLink.navigatedTo).toBeNull('should not have navigated yet');
    button.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(routerLink.navigatedTo).toBe('./add');
  });
});

describe('FinesboardsListComponent Error Handling', () => {
  let component: FinesboardsListComponent;
  let fixture: ComponentFixture<FinesboardsListComponent>;
  let finesServiceSpy: { getFinesboards: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatIconModule, RouterTestingModule ],
      declarations: [ FinesboardsListComponent, RouterLinkDirectiveStub ],
      providers: [
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFinesboards']) }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    finesServiceSpy = TestBed.get(FineService);
    finesServiceSpy.getFinesboards.and.returnValue(throwError('Get finesboard error'));
    fixture = TestBed.createComponent(FinesboardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should swallow error on get finesboards', () => {
    expect((component.finesboards)).toBeUndefined();
  });

  it('should display empty finesboards table', () => {
    const table = fixture.debugElement.queryAll(By.directive(MatTable));
    expect(table.length).toBe(0);
  });

  it('should display add button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    expect(button.nativeElement.textContent).toBe('Add');
    expect(button.nativeElement.getAttribute('routerlink')).toBe('./add');
  });
});
