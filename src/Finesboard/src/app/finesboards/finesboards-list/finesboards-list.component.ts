import { Component, OnInit, ViewChild} from '@angular/core';
import { FineService } from '../../fine.service';
import { FinesboardDetails } from '../../models/finesboard-details';
import { AuthService } from 'src/app/auth/auth.service';
import { MessageService } from 'src/app/message.service';
import { BehaviorSubject } from 'rxjs';
import { MatTabGroup } from '@angular/material';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { RolesEnum } from 'src/app/enums/roles';

@Component({
  selector: 'app-finesboards-list',
  templateUrl: './finesboards-list.component.html',
  styleUrls: ['./finesboards-list.component.less']
})
export class FinesboardsListComponent implements OnInit {

  constructor(private fineService: FineService, 
              private authService: AuthService,
              private messaging: MessageService,
              public dialog: MatDialog) { }

  @ViewChild(MatTabGroup, {static: false}) tabs: MatTabGroup;

  displayMyFinesboards = true;
  myFinesboards = new BehaviorSubject<FinesboardDetails[]>([]);
  exploreFinesboards: FinesboardDetails[] = [];
  myDisplayColumns: string[];
  exploreDisplayColumns: string[];
  isAdmin: boolean;
  isAuthenticated: boolean;
  
  ngOnInit() {
    this.getMyFinesboards();
    this.getFinesboards();
    this.isAdmin = this.authService.isAuthorized([RolesEnum.Admin]);
    this.myDisplayColumns = ['name','status','member'];
    this.exploreDisplayColumns = ['name','status','member'];
    if(this.isAdmin) {
      this.myDisplayColumns.push('configuration');
      this.exploreDisplayColumns.push('configuration');
    }
  }
  
  getMyFinesboards(): void {
    this.fineService.getMyFinesboards().subscribe(
      finesboards => {
        this.myFinesboards.next(finesboards);
        this.setDisplayMyFinesboards();
      },
      () => {}); // swallow error
  }

  getFinesboards(): void {
    this.fineService.getFinesboards().subscribe(
      finesboards => {
        this.exploreFinesboards = finesboards;
      },
      () => {}); // swallow error
  }
  
  onJoinFinesboard(finesboard: FinesboardDetails) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to join the ${finesboard.name} finesboard?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.joinFinesboard(finesboard);
      }
    });
  }
  
  joinFinesboard(finesboard: FinesboardDetails) {
    this.fineService.joinFinesboard(finesboard.id).subscribe(() => {
      this.getMyFinesboards();
      this.messaging.notify(`Sucess: Joined ${finesboard.name} finesboard`);
    }, () => {
      this.messaging.notify(`Failure: Unable to join ${finesboard.name} finesboard`);
    })
  }

  canJoinFinesboard(finesboard: FinesboardDetails) {
    const isNotAMemberOfTheFinesboard = this.myFinesboards.value.filter(x => x.id == finesboard.id).length == 0;
    const isActive = finesboard.status.description == 'Active';
    return isNotAMemberOfTheFinesboard && isActive;
  }
  
  onLeaveFinesboard(finesboard: FinesboardDetails) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to leave the ${finesboard.name} finesboard?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.leaveFinesboard(finesboard);
      }
    });
  }

  private leaveFinesboard(finesboard: FinesboardDetails) {
    this.fineService.leaveFinesboard(finesboard.id).subscribe(() => {
      const filteredFinesboards = this.myFinesboards.value.filter(x => x.id != finesboard.id);
      this.myFinesboards.next(filteredFinesboards);
      this.setDisplayMyFinesboards();
      this.messaging.notify(`Sucess: Left ${finesboard.name} finesboard`);
    }, () => {
      this.messaging.notify(`Failure: Unable to leave ${finesboard.name} finesboard`);
    })
  }

  canLeaveFinesboard(finesboard: FinesboardDetails) {
    const isAMemberOfTheFinesboard = this.myFinesboards.value.filter(x => x.id == finesboard.id).length > 0;
    const isActive = finesboard.status.description == 'Active';
    return isAMemberOfTheFinesboard && isActive;
  }
  
  private setDisplayMyFinesboards() {
    if (this.myFinesboards.value.length == 0) {
      this.displayMyFinesboards = false;
    }
    else {
      this.displayMyFinesboards = true;
    }
  }
}
    