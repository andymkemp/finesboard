import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinesboardsComponent } from './finesboards.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FinesboardsComponent', () => {
  let component: FinesboardsComponent;
  let fixture: ComponentFixture<FinesboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinesboardsComponent ],
      imports: [ RouterTestingModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinesboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
