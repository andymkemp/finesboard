import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFinesboardComponent } from './add-finesboard.component';
import { FinesboardDetails } from '../../models/finesboard-details';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatFormFieldModule, MatInputModule, MatIconModule,
  MatSnackBarModule,
  MatError,
  MatFormField} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { FineService } from '../../fine.service';
import { MessageService } from '../../message.service';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('AddFinesboardComponent', () => {
  let component: AddFinesboardComponent;
  let fixture: ComponentFixture<AddFinesboardComponent>;
  let fineServiceSpy: { addFinesboard: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };
  let routerSpy: { navigate: jasmine.Spy };
  let finesboard: FinesboardDetails;

  const submitValidForm = () => {
    component.finesboardForm.name.setValue(finesboard.name);
    component.onSubmitFinesboard();
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule, MatSnackBarModule ],
      declarations: [ AddFinesboardComponent ],
      providers: [
        FormBuilder,
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['addFinesboard']) },
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    finesboard = {
      id: '1',
      name: 'Cake Tick',
      status: { id: '1', description: 'active'},
      currency: 'Cake',
      offences: []
    };
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    routerSpy = TestBed.get(Router);
    fineServiceSpy.addFinesboard.and.returnValue(of(finesboard));
    fixture = TestBed.createComponent(AddFinesboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add finesboard if form is valid on submit', () => {
    submitValidForm();
    expect(fineServiceSpy.addFinesboard).toHaveBeenCalledWith({
      id: null,
      name: finesboard.name,
    });
  });

  it('should go to finesboard page when add has completed', () => {
    submitValidForm();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/finesboards']);
  });

  it('should display notification on add success', () => {
    submitValidForm();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Success: Finesboard added');
  });

  it('should not add finesboard if form is invalid on submit', () => {
    component.onSubmitFinesboard();
    expect(fineServiceSpy.addFinesboard).not.toHaveBeenCalled();
  });

  it('should display empty form on init', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const name = matFormFields[0].query(By.css('input')).nativeElement.value;
    expect(name).toBe('');
  });

  it('should call add finesboard when clicking submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    matFormFields[0].query(By.css('input')).nativeElement.sendInput(finesboard.name);
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(fineServiceSpy.addFinesboard).toHaveBeenCalled();
  });

  it('should display required field notification if name is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const name = matFormFields[0];
    let matErrors = name.queryAll(By.directive(MatError));
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = name.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });
});

describe('AddFinesboardComponent Error Handling', () => {
  let fixture: ComponentFixture<AddFinesboardComponent>;
  let fineServiceSpy: { addFinesboard: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule, MatSnackBarModule ],
      declarations: [ AddFinesboardComponent ],
      providers: [
        FormBuilder,
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['addFinesboard']) },
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
      ]
    })
    .compileComponents();
  }));

  it('should display notification on add finesboard failure', () => {
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    fineServiceSpy.addFinesboard.and.returnValue(throwError(new Error('Add finesboard error')));
    fixture = TestBed.createComponent(AddFinesboardComponent);
    const component = fixture.componentInstance;
    fixture.detectChanges();
    component.finesboardForm.name.setValue('Cake Tick');
    component.onSubmitFinesboard();
    expect(fineServiceSpy.addFinesboard).toHaveBeenCalled();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Failure: Unable to add finesboard');
  });
});
