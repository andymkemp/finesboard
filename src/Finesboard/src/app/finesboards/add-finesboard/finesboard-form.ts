import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl,  } from '@angular/forms';
import { FinesboardDetails } from '../../models/finesboard-details';

export class FinesboardForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private finesboard: FinesboardDetails) {
        this.form = this.formBuilder.group({
            id: [finesboard.id],
            name: [finesboard.name, Validators.required],
            currency: [finesboard.currency, Validators.required],
          });
    }

    get id() { return this.form.get('id'); }
    get name() { return this.form.get('name'); }
}
