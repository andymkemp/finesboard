import { Component, OnInit } from '@angular/core';
import { FinesboardDefaultErrorStateMatcher } from '../../finesboard-error-state-matcher';
import { Router } from '@angular/router';
import { FineService } from '../../fine.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from '../../message.service';
import { FinesboardDetails } from '../../models/finesboard-details';
import { FinesboardForm } from './finesboard-form';
import { Location } from '@angular/common';

@Component({
  selector: 'app-finesboard-forms',
  templateUrl: './add-finesboard.component.html',
  styleUrls: ['./add-finesboard.component.less']
})
export class AddFinesboardComponent implements OnInit {

  finesboardForm: FinesboardForm;
  matcher = new FinesboardDefaultErrorStateMatcher();
  isLinear = true;

  constructor(private router: Router,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService) { }

  ngOnInit() {
    this.finesboardForm = new FinesboardForm(this.formBuilder, new FinesboardDetails());
  }

  onSubmitFinesboard() {
    if (this.finesboardForm.form.invalid) {
      return;
    }
    const finesboard = this.finesboardForm.form.value;
    this.fineService.addFinesboard(finesboard).subscribe((result: FinesboardDetails) => {
      this.messaging.notify('Success: Finesboard added');
      this.router.navigate([`/finesboards/${result.id}/configuration`]);
    }, () => {
      this.messaging.notify('Failure: Unable to add finesboard');
    });
  }

  goBack() {
    this.location.back();
  }
}
