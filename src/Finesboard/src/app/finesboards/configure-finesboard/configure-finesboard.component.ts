import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FineService } from 'src/app/fine.service';
import { MessageService } from 'src/app/message.service';
import { FinesboardDetails } from 'src/app/models/finesboard-details';
import { Location } from '@angular/common';
import { FinesboardStatusEnum } from 'src/app/enums/finesboard-status';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { OffenceDetails } from 'src/app/models/offence-details';
import { Forfeit } from 'src/app/models/forfeit';

@Component({
  selector: 'app-configure-finesboard',
  templateUrl: './configure-finesboard.component.html',
  styleUrls: ['./configure-finesboard.component.less']
})
export class ConfigureFinesboardComponent implements OnInit {

  finesboard: FinesboardDetails;

  constructor(private route: ActivatedRoute,
              private fineService: FineService,
              private messaging: MessageService,
              private location: Location,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getFinesboard();
  }

  getFinesboard(): void {
    const finesboardId = this.route.snapshot.paramMap.get('finesboardId');
    this.fineService.getFinesboard(finesboardId).subscribe(finesboard => {
      this.finesboard = finesboard;
    }, () => {
      this.messaging.notify(`Failure: Unable to get finesboard with id: ${finesboardId}`);
    });
  }

  goBack() {
    this.location.back();
  }

  canActivate() {
    // tslint:disable-next-line: triple-equals
    if (!this.finesboard || this.finesboard.status.id != FinesboardStatusEnum.Pending) {
      return false;
    }
    if (!this.finesboard.offences || this.finesboard.offences.length === 0) {
      return false;
    }
    let hasLinkedForfeit = false;
    this.finesboard.offences.forEach(offence => {
      if (offence.forfeits.length > 0) {
        hasLinkedForfeit = true;
      }
    });
    return hasLinkedForfeit;
  }

  canDeactivate() {
    // tslint:disable-next-line: triple-equals
    if (!this.finesboard || this.finesboard.status.id != FinesboardStatusEnum.Active) {
      return false;
    }
    return true;
  }

  onActivate() {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to activate ${this.finesboard.name} finesboard?`,
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.activate();
      }
    });
  }

  activate() {
    this.fineService.activateFinesboard(this.finesboard.id).subscribe(finesboard => {
      this.finesboard = finesboard;
      this.messaging.notify(`Success: ${this.finesboard.name} finesboard activated`);
    }, () => {
      this.messaging.notify(`Failure: Unable to activate ${this.finesboard.name} finesboard`);
    });
  }

  onDeactivate() {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to deactivate ${this.finesboard.name} finesboard?`,
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.deactivate();
      }
    });
  }

  deactivate() {
    this.fineService.deactivateFinesboard(this.finesboard.id).subscribe(finesboard => {
      this.finesboard = finesboard;
      this.messaging.notify(`Success: ${this.finesboard.name} finesboard deactivated`);
    }, () => {
      this.messaging.notify(`Failure: Unable to deactivate ${this.finesboard.name} finesboard`);
    });
  }

  onDelete() {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to delete ${this.finesboard.name} finesboard?`,
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.delete();
      }
    });
  }

  delete() {
    this.fineService.deleteFinesboard(this.finesboard.id).subscribe(() => {
      this.messaging.notify(`Success: Finesboard deleted`);
      this.goBack();
    }, () => {
      this.messaging.notify(`Failure: Unable to delete ${this.finesboard.name} finesboard`);
    });
  }

  onDeleteOffence(offence: OffenceDetails) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to delete ${offence.name} offence?`,
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.deleteOffence(offence);
      }
    });
  }

  deleteOffence(offence: OffenceDetails) {
    this.fineService.deleteOffence(this.finesboard.id, offence.id).subscribe(() => {
      this.getFinesboard();
      this.messaging.notify('Success: Offence deleted');
    }, () => {
      this.messaging.notify('Failure: Unable to delete offence');
    });
  }

  onDeleteForfeit(forfeit: Forfeit) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to delete ${forfeit.name} forfeit?`,
    });

    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.deleteForfeit(forfeit);
      }
    });
  }

  deleteForfeit(forfeit: Forfeit) {
    this.fineService.deleteForfeit(this.finesboard.id, forfeit.id).subscribe(() => {
      this.getFinesboard();
      this.messaging.notify('Success: Forfeit deleted');
    }, () => {
      this.messaging.notify('Failure: Unable to delete forfeit');
    });
  }
}
