import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureFinesboardComponent } from './configure-finesboard.component';

describe('ConfigureFinesboardComponent', () => {
  let component: ConfigureFinesboardComponent;
  let fixture: ComponentFixture<ConfigureFinesboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureFinesboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureFinesboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
