import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinesboardsListComponent } from './finesboards-list/finesboards-list.component';
import { AddFinesboardComponent } from './add-finesboard/add-finesboard.component';
import { FinesboardsComponent } from './finesboards/finesboards.component';
import { FinesListComponent } from '../fines/fines-list/fines-list.component';
import { FinesComponent } from '../fines/fines/fines.component';
import { UpdateFineComponent } from '../fines/fine-forms/update-fine.component';
import { ConfigureFinesboardComponent } from './configure-finesboard/configure-finesboard.component';
import { AddOffenceComponent } from '../offences/add-offence/add-offence.component';
import { OffencesComponent } from '../offences/offences/offences.component';
import { AddForfeitComponent } from '../forfeits/add-forfeit/add-forfeit.component';
import { UpdateForfeitComponent } from '../forfeits/update-forfeit/update-forfeit.component';
import { AddFineComponent } from '../fines/add-fine/add-fine.component';
import { AuthGuard } from '../auth/auth.guard';
import { RoleGuard } from '../auth/role.guard';
import { UpdateOffenceComponent } from '../offences/update-offence/update-offence.component';
import { RolesEnum } from '../enums/roles';

const finesboardsRoutes: Routes = [{
  path: 'finesboards',
  component: FinesboardsComponent,
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    { path: '', component: FinesboardsListComponent },
    { 
      path: 'add', 
      component: AddFinesboardComponent,
      canActivate: [RoleGuard], 
      data: { acceptRoles: [RolesEnum.Admin] },
    },
    { 
      path: ':finesboardId/configuration', 
      component: ConfigureFinesboardComponent,
      canActivate: [RoleGuard],
      data: { acceptRoles: [RolesEnum.Admin] },
    },
    {
      path: ':finesboardId/fines',
      component: FinesComponent,
      children: [
        { 
          path: '', 
          component: FinesListComponent
        },
        { 
          path: 'add', 
          component: AddFineComponent,
        },
        { 
          path: 'update/:fineId', 
          component: UpdateFineComponent,
          canActivate: [RoleGuard],
          data: { acceptRoles: [RolesEnum.Admin] },
        },
      ]
    },
    {
      path: ':finesboardId/configuration/offences',
      component: OffencesComponent,
      canActivate: [RoleGuard],
      data: { acceptRoles: [RolesEnum.Admin] },
      children: [
        { path: 'add', component: AddOffenceComponent },
        { path: ':offenceId/update', component: UpdateOffenceComponent },
      ]
    },
    {
      path: ':finesboardId/configuration/forfeits',
      component: OffencesComponent,
      canActivate: [RoleGuard],
      data: { acceptRoles: [RolesEnum.Admin] },
      children: [
        { path: 'add', component: AddForfeitComponent },
        { path: ':forfeitId/update', component: UpdateForfeitComponent },
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(finesboardsRoutes)],
  exports: [RouterModule]
})
export class FinesboardsRoutingModule { }
