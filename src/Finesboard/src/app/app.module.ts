import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatFormFieldModule, MatCardModule, MatInputModule, MatIconModule, MatExpansionModule } from '@angular/material';
import { FinesboardsModule } from './finesboards/finesboards.module';
import { FinesModule } from './fines/fines.module';
import { OffencesModule } from './offences/offences.module';
import { ForfeitsModule } from './forfeits/forfeits.module';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { UsersModule } from './users/users.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { ConfirmationDialogComponent } from './common/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    LoginComponent, 
    RegisterComponent, 
    UserComponent, 
    ConfirmationDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    FinesModule,
    FinesboardsModule,
    OffencesModule,
    ForfeitsModule,
    UsersModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatExpansionModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  exports: [
    ConfirmationDialogComponent,
  ],
  entryComponents: [
    ConfirmationDialogComponent,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
