import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForfeitsComponent } from './forfeits.component';

describe('ForfeitsComponent', () => {
  let component: ForfeitsComponent;
  let fixture: ComponentFixture<ForfeitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForfeitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForfeitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
