import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForfeitsComponent } from './forfeits/forfeits.component';
import { AddForfeitComponent } from './add-forfeit/add-forfeit.component';
import { UpdateForfeitComponent } from './update-forfeit/update-forfeit.component';
import { AuthGuard } from '../auth/auth.guard';


const routes: Routes = [
  {
    path: 'forfeits',
    component: ForfeitsComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      // { path: '', component: ForfeitListComponent },
      { path: 'add', component: AddForfeitComponent },
      // { path: 'delete/:forfeitId', component: DeleteForfeitComponent },
      { path: ':forfeitId/update', component: UpdateForfeitComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForfeitsRoutingModule { }
