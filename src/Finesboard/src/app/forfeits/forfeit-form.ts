import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl,  } from '@angular/forms';
import { Forfeit } from 'src/app/models/forfeit';

export class ForfeitForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private forfeit: Forfeit) {
        this.form = this.formBuilder.group({
            id: [forfeit.id],
            name: [forfeit.name, Validators.required],
            amount: [forfeit.amount, Validators.required],
            finesboardId: [forfeit.finesboardId],
          });
    }

    get id() { return this.form.get('id'); }
    get name() { return this.form.get('name'); }
    get amount() { return this.form.get('amount'); }
    get fineboardId() { return this.form.get('finesboardId'); }
}
