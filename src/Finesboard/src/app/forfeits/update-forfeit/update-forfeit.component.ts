import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FineService } from 'src/app/fine.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { Location } from '@angular/common';
import { ForfeitForm } from 'src/app/forfeits/forfeit-form';
import { FinesboardDefaultErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';
import { Forfeit } from 'src/app/models/forfeit';

@Component({
  selector: 'app-update-forfeit',
  templateUrl: './update-forfeit.component.html',
  styleUrls: ['./update-forfeit.component.less']
})
export class UpdateForfeitComponent implements OnInit {

  forfeitForm: ForfeitForm;
  matcher = new FinesboardDefaultErrorStateMatcher();

  constructor(private route: ActivatedRoute,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService) { }

  ngOnInit() {
    this.getForfeit();
  }

  getForfeit(): void {
    const finesboardId = this.route.parent.snapshot.paramMap.get('finesboardId');
    const forfeitId = this.route.snapshot.paramMap.get('forfeitId');
    this.fineService.getForfeit(finesboardId, forfeitId).subscribe((result: Forfeit) => {
      this.forfeitForm = new ForfeitForm(this.formBuilder, result);
    }, () => {
      this.messaging.notify(`Failure: Unable to get forfeit with id: ${forfeitId}`);
    });
  }

  onSubmit() {
    if (this.forfeitForm.form.invalid) {
      return;
    }
    const forfeit: Forfeit = this.forfeitForm.form.value;
    this.fineService.updateForfeit(forfeit).subscribe(() => {
      this.messaging.notify('Success: Forfeit updated');
      this.goBack();
    }, () => {
      this.messaging.notify('Failure: Unable to update forfeit');
    });
  }

  goBack() {
    this.location.back();
  }
}
