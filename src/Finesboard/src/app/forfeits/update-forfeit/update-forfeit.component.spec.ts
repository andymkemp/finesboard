import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateForfeitComponent } from './update-forfeit.component';

describe('UpdateForfeitComponent', () => {
  let component: UpdateForfeitComponent;
  let fixture: ComponentFixture<UpdateForfeitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateForfeitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateForfeitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
