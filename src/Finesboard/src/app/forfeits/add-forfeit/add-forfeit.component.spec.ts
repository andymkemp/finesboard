import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddForfeitComponent } from './add-forfeit.component';

describe('AddForfeitComponent', () => {
  let component: AddForfeitComponent;
  let fixture: ComponentFixture<AddForfeitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddForfeitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddForfeitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
