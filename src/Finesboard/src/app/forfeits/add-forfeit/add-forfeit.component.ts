import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FineService } from 'src/app/fine.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { Location } from '@angular/common';
import { ForfeitForm } from 'src/app/forfeits/forfeit-form';
import { Forfeit } from 'src/app/models/forfeit';
import { FinesboardDefaultErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';

@Component({
  selector: 'app-add-forfeit',
  templateUrl: './add-forfeit.component.html',
  styleUrls: ['./add-forfeit.component.less']
})
export class AddForfeitComponent implements OnInit {

  finesboardId: string;
  forfeitForm: ForfeitForm;
  matcher = new FinesboardDefaultErrorStateMatcher();

  constructor(private router: Router,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.finesboardId = this.route.parent.snapshot.paramMap.get('finesboardId');
    this.forfeitForm = new ForfeitForm(this.formBuilder, new Forfeit());
  }

  onSubmit(form) {
    if (this.forfeitForm.form.invalid) {
      return;
    }
    const forfeit: Forfeit = this.forfeitForm.form.value;
    forfeit.finesboardId = this.finesboardId;
    this.fineService.addForfeit(forfeit).subscribe((result: Forfeit) => {
      this.messaging.notify('Success: Offence added');
      this.goBack();
    }, () => {
      this.messaging.notify('Failure: Unable to add offence');
    });
  }

  goBack() {
    this.location.back();
  }
}
