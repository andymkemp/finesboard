import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForfeitsRoutingModule } from './forfeits-routing.module';
import { ForfeitsComponent } from './forfeits/forfeits.component';
import { AddForfeitComponent } from './add-forfeit/add-forfeit.component';
import { MatToolbarModule, MatCardModule, MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateForfeitComponent } from './update-forfeit/update-forfeit.component';

@NgModule({
  declarations: [ForfeitsComponent, AddForfeitComponent, UpdateForfeitComponent],
  imports: [
    CommonModule,
    ForfeitsRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
  ]
})
export class ForfeitsModule { }
