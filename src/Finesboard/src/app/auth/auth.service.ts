import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Authenticate } from '../models/authenticate';
import { DataService } from '../data.service';
import { Authenticated } from '../models/authenticated';
import { AuthenticatedUser } from './authenticated-user.model';
import * as jwt_decode from 'jwt-decode';
import { Register } from '../models/register';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authToken: string;
  private currentAuthSubject: BehaviorSubject<AuthenticatedUser>;
  public currentAuth: Observable<AuthenticatedUser>;

  constructor(private dataService: DataService) { 
    this.authToken = sessionStorage.getItem('currentUserToken');
    this.currentAuthSubject = new BehaviorSubject<AuthenticatedUser>(this.decodeToken(this.authToken));
    this.currentAuth = this.currentAuthSubject.asObservable();
  }

  public get authTokenValue(): string {
    return this.authToken;
  }

  public get currentAuthValue(): AuthenticatedUser {
      return this.currentAuthSubject.value;
  }

  public get isAuthenticated(): Boolean {
    const authenticatedUser = this.currentAuthValue;
    if(authenticatedUser && authenticatedUser.exp*1000 > Date.now()) {
      return true;
    }
    return false;
  }
  
  isAuthorized(acceptRoles: string[]) {
    const authenticatedUser = this.currentAuthValue;
    let authorized = false;
    if(!authenticatedUser.role){
      return false;
    }
    if(authenticatedUser && authenticatedUser.role instanceof Array) {
      authorized = this.checkRoleArray(acceptRoles, authenticatedUser.role);
    } else {
      authorized = this.checkRoleString(acceptRoles, authenticatedUser.role as string);
    }
    return authorized;
  }

  register(register: Register) {
    return this.dataService.post<Authenticate, Authenticated>('/api/users/register', register);
  }
    
  login(authenticate: Authenticate) {
      return this.dataService.post<Authenticate, Authenticated>('/api/users/authenticate', authenticate)
          .pipe(tap(authenticated => {
              this.authToken = authenticated.token
              sessionStorage.setItem('currentUserToken', this.authToken);
              this.currentAuthSubject.next(this.decodeToken(this.authToken));
          }));
  }

  logout() {
      this.authToken = null;
      sessionStorage.removeItem('currentUserToken');
      this.currentAuthSubject.next(null);
  }

  private decodeToken(token: string): AuthenticatedUser {    
    if(!token) {
      return null;
    }
    return jwt_decode(token) as AuthenticatedUser;
  }

  private checkRoleArray(acceptRoles: string[], roles: string[]): boolean {
    let authorized = false;
    for(let acceptRole of acceptRoles) {
      for(let role of roles) {
        if(acceptRole === role) {
          authorized = true;
        }
      }
    }
    return authorized;
  }

  private checkRoleString(acceptRoles: string[], role: string): boolean {
    let authorized = false;
    for(let acceptRole of acceptRoles) {
      if(acceptRole === role) {
        authorized = true;
      }
    }
    return authorized;
  }
}
