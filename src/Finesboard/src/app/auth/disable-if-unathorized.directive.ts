import { Directive, ElementRef, OnChanges, Input } from '@angular/core';
import { AuthService } from './auth.service';
 
@Directive({
    selector: '[appDisableIfUnauthorized]',
})
export class DisableIfUnauthorizedDirective implements OnChanges {
    @Input('appDisableIfUnauthorized') roles: string[]; // Required permission passed in
 
    constructor(private el: ElementRef, private authService: AuthService) { }
 
    ngOnChanges() {
        if (!this.authService.isAuthorized(this.roles)) {
            this.el.nativeElement.disabled = true;
        }
    }
}