import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { MessageService } from '../message.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanActivateChild {
  constructor(private authService: AuthService, private messaging: MessageService) {}

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {            
    const acceptRoles: string[] = route.data.acceptRoles ? route.data.acceptRoles : [] ;
    return this.checkAuthorization(acceptRoles);
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const acceptRoles: string[] = route.data.acceptRoles ? route.data.acceptRoles : [] ;
    return this.checkAuthorization(acceptRoles);
  }

  checkAuthorization(acceptRoles: string[]): boolean {
    const authorized = this.authService.isAuthorized(acceptRoles);
    if (authorized) {
        // logged in so return true
        return true;
    }
    this.messaging.notify("You are not authorized to perform this action");
    return false;
  }
}
