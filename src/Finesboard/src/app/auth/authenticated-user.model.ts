export class AuthenticatedUser {
    nameid: string;
    unique_name: string;
    exp: number;
    role: string | string[];
}