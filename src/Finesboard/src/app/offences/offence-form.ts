import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OffenceDetails } from 'src/app/models/offence-details';

export class OffenceForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private offence: OffenceDetails) {
        this.form = this.formBuilder.group({
            id: [offence.id],
            name: [offence.name, Validators.required],
            forfeits: [offence.forfeits, Validators.required],
          });
    }

    get id() { return this.form.get('id'); }
    get name() { return this.form.get('name'); }
    get forfeits() { return this.form.get('forfeits'); }
}
