import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOffenceComponent } from './add-offence.component';

describe('AddOffenceComponent', () => {
  let component: AddOffenceComponent;
  let fixture: ComponentFixture<AddOffenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOffenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOffenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
