import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { OffenceForm } from '../offence-form';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FineService } from 'src/app/fine.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { OffenceDetails } from 'src/app/models/offence-details';
import { ForfeitForm } from '../forfeit-form';
import { Forfeit } from 'src/app/models/forfeit';
import { FinesboardDefaultErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';
import { MatTable, MatSlideToggle, MatSlideToggleChange } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { FinesboardDetails } from 'src/app/models/finesboard-details';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { OffenceForfeit } from 'src/app/models/offence-forfeit';

@Component({
  selector: 'app-add-offence',
  templateUrl: './add-offence.component.html',
  styleUrls: ['./add-offence.component.less']
})
export class AddOffenceComponent implements OnInit {

  finesboardId: string;
  offenceForm: OffenceForm;
  forfeitForm: ForfeitForm;
  linkedForfeits: Forfeit[];
  unlinkedForfeits: Forfeit[];
  matcher = new FinesboardDefaultErrorStateMatcher();
  @ViewChildren(MatTable) tables: QueryList<MatTable<Forfeit>>;
  @ViewChildren(MatSlideToggle) statusToggles: QueryList<MatSlideToggle>;
  missingForfeits: boolean;

  constructor(private router: Router,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService,
              private route: ActivatedRoute,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.finesboardId = this.route.parent.snapshot.paramMap.get('finesboardId');
    this.offenceForm = new OffenceForm(this.formBuilder, new OffenceDetails());
    this.forfeitForm = new ForfeitForm(this.formBuilder, new Forfeit());
    this.linkedForfeits = [];
    this.getForfeits();
    this.missingForfeits = false;
  }

  getForfeits() {
    this.fineService.getForfeits(this.finesboardId).subscribe((result: Forfeit[]) => {
      this.unlinkedForfeits = result;
    }, () => {
      this.messaging.notify('Failure: Unable to add offence');
    });
  }

  onSubmitOffence() {
    this.offenceForm.forfeits.setValue(this.linkedForfeits);
    this.missingForfeits = this.offenceForm.forfeits.hasError('required');
    if (this.offenceForm.form.invalid) {
      return;
    }
    const offence: OffenceDetails = this.offenceForm.form.value;
    offence.finesboardId = this.finesboardId;
    this.fineService.addOffence(offence).subscribe((result: OffenceDetails) => {
      this.messaging.notify('Success: Offence added');
      this.goBack();
    }, () => {
      this.messaging.notify('Failure: Unable to add offence');
    });
  }

  onSubmitForfeit(form) {
    if (this.forfeitForm.form.invalid) {
      return;
    }
    const forfeit: Forfeit = this.forfeitForm.form.value;
    forfeit.finesboardId = this.finesboardId;
    this.fineService.addForfeit(forfeit).subscribe((result: Forfeit) => {
      this.unlinkedForfeits.push(result);
      this.renderTableRows();
      form.resetForm();
      this.messaging.notify('Success: Forfeit added');
    }, () => {
      this.messaging.notify('Failure: Unable to add forfeit');
    });
  }
  
  onLinkForfeit(forfeit: Forfeit) {    
    this.unlinkedForfeits = this.unlinkedForfeits.filter(x => x.id != forfeit.id);
    this.linkedForfeits.push(forfeit);
    this.renderTableRows();
  }
  
  onUnlinkForfeit(forfeit: Forfeit) {
    this.linkedForfeits = this.linkedForfeits.filter(x => x.id != forfeit.id);
    this.unlinkedForfeits.push(forfeit);
    this.renderTableRows();
  }

  goBack() {
    this.location.back();
  }
  
  renderTableRows() {    
    this.tables.forEach(table => table.renderRows());
  }
}
