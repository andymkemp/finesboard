import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddOffenceComponent } from './add-offence/add-offence.component';
import { OffencesRoutingModule } from './offences-routing.module';
import { OffencesComponent } from './offences/offences.component';
import { MatToolbarModule, MatCardModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatTableModule, 
  MatCheckboxModule, MatIconModule, MatExpansionModule, MatSlideToggleModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateOffenceComponent } from './update-offence/update-offence.component';

@NgModule({
  declarations: [AddOffenceComponent, OffencesComponent, UpdateOffenceComponent],
  imports: [
    CommonModule,
    OffencesRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatTableModule,
    MatCheckboxModule,
    MatIconModule,
    MatExpansionModule,
    MatSlideToggleModule,
  ]
})
export class OffencesModule { }
