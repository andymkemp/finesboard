import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OffencesComponent } from './offences/offences.component';
import { AddOffenceComponent } from './add-offence/add-offence.component';
import { UpdateOffenceComponent } from './update-offence/update-offence.component';
import { AuthGuard } from '../auth/auth.guard';

const offencesRoutes: Routes = [
  {
    path: 'offences',
    component: OffencesComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      // { path: '', component: OffencesListComponent },
      { path: 'add', component: AddOffenceComponent },
      // { path: 'delete/:fineId', component: DeleteOffenceComponent },
      { path: ':offenceId/update', component: UpdateOffenceComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(offencesRoutes)],
  exports: [RouterModule]
})
export class OffencesRoutingModule { }
