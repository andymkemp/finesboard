import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOffenceComponent } from './update-offence.component';

describe('UpdateOffenceComponent', () => {
  let component: UpdateOffenceComponent;
  let fixture: ComponentFixture<UpdateOffenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateOffenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOffenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
