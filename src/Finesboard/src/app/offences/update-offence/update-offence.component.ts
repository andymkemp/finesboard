import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ForfeitForm } from '../forfeit-form';
import { Location } from '@angular/common';
import { FineService } from 'src/app/fine.service';
import { FormBuilder } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { ActivatedRoute } from '@angular/router';
import { FinesboardDefaultErrorStateMatcher } from 'src/app/finesboard-error-state-matcher';
import { Forfeit } from 'src/app/models/forfeit';
import { MatTable, MatSlideToggle, MatSlideToggleChange } from '@angular/material';
import { OffenceDetails } from 'src/app/models/offence-details';
import { FinesboardDetails } from 'src/app/models/finesboard-details';
import { ConfirmationDialogComponent } from '../../common/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { OffenceForfeit } from 'src/app/models/offence-forfeit';

@Component({
  selector: 'app-update-offence',
  templateUrl: './update-offence.component.html',
  styleUrls: ['./update-offence.component.less']
})
export class UpdateOffenceComponent implements OnInit {

  finesboardId: string;
  offenceId: string;
  offence: OffenceDetails;
  forfeitForm: ForfeitForm;
  matcher = new FinesboardDefaultErrorStateMatcher();
  linkedForfeits: OffenceForfeit[];
  unlinkedForfeits: Forfeit[];
  @ViewChildren(MatTable) tables: QueryList<MatTable<Forfeit>>;
  @ViewChildren(MatSlideToggle) statusToggles: QueryList<MatSlideToggle>;
  
  constructor(private fineService: FineService,
    private location: Location,
    private formBuilder: FormBuilder,
    private messaging: MessageService,
    private route: ActivatedRoute,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.finesboardId = this.route.parent.snapshot.paramMap.get('finesboardId');
    this.offenceId = this.route.snapshot.paramMap.get('offenceId');
    this.forfeitForm = new ForfeitForm(this.formBuilder, new Forfeit());
    this.fineService.getFinesboard(this.finesboardId).subscribe((result: FinesboardDetails) => {
      for (const offence of result.offences) {
        if(offence.id == this.offenceId) {
          this.offence = offence;
          this.linkedForfeits = offence.forfeits;
        }
      }
      this.unlinkedForfeits = result.forfeits.filter((value) => {
        let add = true;
        for (const linkedForfeit of this.linkedForfeits) {
          if(value.id == linkedForfeit.id) {
            add = false;
            break;
          }
        }
        return add;
      });
      
    }, () => {
      this.messaging.notify('Failure: Unable to get finesboard details');
    });
  }

  onSubmitForfeit(form) {
    if (this.forfeitForm.form.invalid) {
      return;
    }
    const forfeit: Forfeit = this.forfeitForm.form.value;
    forfeit.finesboardId = this.finesboardId;
    this.fineService.addForfeit(forfeit).subscribe((result: Forfeit) => {
      this.unlinkedForfeits.push(result);
      this.renderTableRows();
      form.resetForm();
      this.messaging.notify('Success: Forfeit added');
    }, (err) => {
      this.messaging.notify(`Failure: Unable to add fofeit [${err.error}]`);
    });
  }
  
  onLinkForfeit(forfeit: Forfeit) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to link ${forfeit.name} forfeit to ${this.offence.name} offence?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.linkForfeit(forfeit);
      }
    });
  }
  
  linkForfeit(forfeit: Forfeit) {
    this.fineService.linkForfeitToOffence(this.finesboardId, this.offenceId, forfeit.id).subscribe(() => {
      this.unlinkedForfeits = this.unlinkedForfeits.filter(x => x.id != forfeit.id);
      const offenceForfeit: OffenceForfeit = {
        id: forfeit.id, 
        name: forfeit.name, 
        amount: forfeit.amount,
        finesboardId: forfeit.finesboardId,
        status: 1
      };
      this.linkedForfeits.push(offenceForfeit);
      this.renderTableRows();
      this.messaging.notify('Success: Forfeit linked to offence');
    }, (err) => {
      this.messaging.notify(`Failure: Unable to link forfeit to offence [${err.error}]`);
    });
  }
  
  onUnlinkForfeit(offenceForfeit: OffenceForfeit) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to unlink ${offenceForfeit.name} forfeit from ${this.offence.name} offence?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.unlinkForfeit(offenceForfeit);
      }
    });
  }
  
  unlinkForfeit(offenceForfeit: OffenceForfeit) {
    this.fineService.unlinkForfeitToOffence(this.finesboardId, this.offenceId, offenceForfeit.id).subscribe(() => {
      this.linkedForfeits = this.linkedForfeits.filter(x => x.id != offenceForfeit.id);
      const forfeit: Forfeit = offenceForfeit;
      this.unlinkedForfeits.push(forfeit);
      this.renderTableRows();
      this.messaging.notify('Success: Forfeit unlinked from offence');
    }, (err) => {
      this.messaging.notify(`Failure: Unable to unlink forfeit from offence [${err.error}]`);
    });
  }

  onToggleStatus(offenceForfeit: OffenceForfeit, $event: MatSlideToggleChange) {
    if((offenceForfeit.status == 1 && $event.checked) || (offenceForfeit.status == 0 &&!$event.checked) )
    {
      return;
    }
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to update offence forfeit status?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.toggleStatus(offenceForfeit, $event);
      } else {
        $event.source.toggle();
      }
    });
  }

  toggleStatus(offenceForfeit: OffenceForfeit, $event: MatSlideToggleChange) {
    if(offenceForfeit.status == 1) {
      this.deactivateOffenceForfeit(offenceForfeit, $event);
    } else {
      this.activateOffenceForfeit(offenceForfeit, $event);
    }
  }

  deactivateOffenceForfeit(offenceForfeit: OffenceForfeit, $event: MatSlideToggleChange) {
    this.fineService.deactivateOffenceForfeit(this.finesboardId, this.offenceId, offenceForfeit.id).subscribe(() => {
      this.linkedForfeits.filter(x => x.id == offenceForfeit.id).forEach(y => y.status = 0);
      this.messaging.notify('Success: Offence forfeit deactivated');
    }, (err) => {
      $event.source.toggle();
      this.messaging.notify(`Failure: Unable to deactivate offence forfeit [${err.error}]`);
    })
  }

  activateOffenceForfeit(offenceForfeit: OffenceForfeit, $event: MatSlideToggleChange) {
    this.fineService.activateOffenceForfeit(this.finesboardId, this.offenceId, offenceForfeit.id).subscribe(() => {
      this.linkedForfeits.filter(x => x.id == offenceForfeit.id).forEach(y => y.status = 1);
      this.messaging.notify('Success: Offence forfeit activated');
    }, (err) => {
      $event.source.toggle();
      this.messaging.notify(`Failure: Unable to activate offence forfeit [${err.error}]`);
    })
  }

  checkStatus(offenceForfeit: OffenceForfeit): boolean {
    return offenceForfeit.status == 1;
  }

  getStatus(offenceForfeit: OffenceForfeit): string {
    return offenceForfeit.status == 1 ? "Active" : "Deactivated";
  }
  
  goBack() {
    this.location.back();
  }
  
  renderTableRows() {    
    this.tables.forEach(table => table.renderRows());
  }
}
