import { Component, OnInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { Fine } from '../../models/fine';
import { FineService } from '../../fine.service';
import { Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { FineForm } from '../fine-form';
import { FinesboardDefaultErrorStateMatcher } from '../../finesboard-error-state-matcher';
import { MessageService } from '../../message.service';
import { FinesboardDetails } from 'src/app/models/finesboard-details';
import { OffenceDetails } from 'src/app/models/offence-details';
import { Forfeit } from 'src/app/models/forfeit';
import { FinesboardStatus } from 'src/app/models/finesboard-status';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/auth/auth.service';
import { OffenceForfeit } from 'src/app/models/offence-forfeit';

@Component({
  selector: 'app-add-fines',
  templateUrl: './add-fine.component.html',
  styleUrls: ['./add-fine.component.less']
})
export class AddFineComponent implements OnInit {

  title = 'Add Fine';
  action = 'Submit';
  fineForm: FineForm;
  matcher = new FinesboardDefaultErrorStateMatcher();
  users: User[];
  finesboards: FinesboardDetails[];
  offences: OffenceDetails[];
  forfeits: Forfeit[];
  status: FinesboardStatus;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService,
              private authService: AuthService) { }
              
  get hasFinesboards() {
    return (this.finesboards && this.finesboards.length > 0)
  }

  ngOnInit() {
    const finesboardId = this.route.snapshot.parent.paramMap.get('finesboardId');
    this.fineForm = new FineForm(this.formBuilder);
    this.fineForm.finesboard.valueChanges.subscribe(this.onChangeFinesboard);
    this.fineForm.offence.valueChanges.subscribe(this.onChangeOffence);
    this.fineForm.forfeit.valueChanges.subscribe(this.onChangeForfeit);
    if (finesboardId) {
      this.fineService.getFinesboard(finesboardId).subscribe((result) => {
        this.finesboards = [result];
        this.fineForm.finesboard.setValue(result);
        this.fineForm.finesboard.disable();
      },
      (err) => {
        this.messaging.notify('Failure: Unable to get finesboards');
      });
      this.fineService.getFinesboardUsers(finesboardId).subscribe((result) => {
        this.users = result;
      },
      (err) => {
        this.messaging.notify('Failure: Unable to get finesboard users');
      });
    } else {
      this.fineService.getMyActiveFinesboards().subscribe((result) => {
        this.finesboards = result;
      },
      (err) => {
        this.messaging.notify('Failure: Unable to get finesboards');
      });
    }
  }

  onSubmit() {
    if (this.fineForm.form.invalid) {
      return;
    }
    const fine: Fine = new Fine();
    fine.targetId = this.fineForm.user.value.id;
    fine.reason = this.fineForm.reason.value;
    fine.amount = this.fineForm.amount.value;
    fine.finesboardId = this.fineForm.finesboard.value.id;
    fine.offenceId = this.fineForm.offence.value.id;
    fine.forfeitId = this.fineForm.forfeit.value.id;
    console.log(fine);
    this.fineService.addFine(fine).subscribe(() => {
      this.messaging.notify('Success: Fine added');
      this.router.navigate([`/finesboards/${fine.finesboardId}/fines`]);
    }, () => {
      this.messaging.notify('Failure: Unable to add fine');
    });
  }

  goBack() {
    this.location.back();
  }

  onChangeFinesboard = (finesboard: FinesboardDetails) => {
    this.fineService.getFinesboardUsers(finesboard.id).subscribe((results) => {
      this.fineForm.user.setValue(null);
      this.users = results;
    });
    this.fineForm.offence.setValue(null);
    this.fineForm.forfeit.setValue(null);
    this.fineForm.amount.setValue(null);
    this.status = finesboard.status;
    this.offences = finesboard.offences.filter(this.hasActiveOffence);
    if (this.offences && this.offences.length === 1) {
      this.fineForm.offence.setValue(this.offences[0]);
    }
  }
  
  onChangeOffence = (offence: OffenceDetails) => {
    if (!offence) {
      return;
    }
    this.fineForm.forfeit.setValue(null);
    this.fineForm.amount.setValue(null);
    this.forfeits = offence.forfeits.filter(this.isActiveOffence);
    if (this.forfeits && this.forfeits.length === 1) {
      this.fineForm.forfeit.setValue(this.forfeits[0]);
    }
  }
  
  onChangeForfeit = (forfeit: Forfeit) => {
    if (!forfeit) {
      return;
    }
    this.fineForm.amount.setValue(forfeit.amount);
  }
  
  private hasActiveOffence = (offence: OffenceDetails): boolean => {
    for(const forfeit of offence.forfeits) {
      if(forfeit.status == 1) {
        return true;
      }
    }
    return false;
  }

  private isActiveOffence = (forfeit: OffenceForfeit): boolean => {
    return forfeit.status == 1;
  }
}
