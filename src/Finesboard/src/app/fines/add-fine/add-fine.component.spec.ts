import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddFineComponent } from './add-fine.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatIconModule,
  MatFormFieldModule, MatInputModule, MatSnackBarModule, MatFormField, MatError } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { FineService } from '../../fine.service';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageService } from '../../message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Fine } from '../../models/fine';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Location } from '@angular/common';
import 'src/testing/element-extensions';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { User } from 'src/app/models/user';

describe('AddFinesComponent', () => {
  let component: AddFineComponent;
  let fixture: ComponentFixture<AddFineComponent>;
  let fineServiceSpy: { addFine: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };
  let routerSpy: { navigate: jasmine.Spy };
  let locationSpy: { back: jasmine.Spy };
  let fine: Fine;
  let user: User;
  let finesboardId: string;

  const submitValidForm = () => {
    component.fineForm.reason.setValue(fine.reason);
    component.fineForm.user.setValue(user);
    component.fineForm.amount.setValue(fine.amount);
    component.onSubmit();
  };

  beforeEach(async(() => {
    finesboardId = '2';
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule, MatSnackBarModule ],
      declarations: [ AddFineComponent ],
      providers: [
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({finesboardId}) },
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['addFine']) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        { provide: Location, useValue: jasmine.createSpyObj('Location', ['back'])},
        FormBuilder,
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fine = {
      id: '1',
      targetId: '1',
      creatorId: '1',
      reason: 'Being Unfriendly',
      amount: 10,
      finesboardId,
      offenceId: null,
      forfeitId: null
    };
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    routerSpy = TestBed.get(Router);
    locationSpy = TestBed.get(Location);
    fineServiceSpy.addFine.and.returnValue(of(fine));
    fixture = TestBed.createComponent(AddFineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add fine if form is valid on submit', () => {
    submitValidForm();
    expect(fineServiceSpy.addFine).toHaveBeenCalledWith({
      id: null,
      description: fine.reason,
      userId: 1,
      amount: fine.amount,
      finesboardId: fine.finesboardId
    });
  });

  it('should go to previous page when add has completed', () => {
    submitValidForm();
    expect(locationSpy.back).toHaveBeenCalledTimes(1);
  });

  it('should display notification on add success', () => {
    submitValidForm();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Success: Fine added');
  });

  it('should not add fine if form is invalid on submit', () => {
    component.onSubmit();
    expect(fineServiceSpy.addFine).not.toHaveBeenCalled();
  });

  it('should display empty form on init', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const description = matFormFields[0].query(By.css('input')).nativeElement.value;
    expect(description).toBe('');
    const user = matFormFields[1].query(By.css('input')).nativeElement.value;
    expect(user).toBe('');
    const amount = matFormFields[2].query(By.css('input')).nativeElement.value;
    expect(amount).toBe('');
  });

  it('should call add fine when clicking submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    matFormFields[0].query(By.css('input')).nativeElement.sendInput(fine.reason);
    matFormFields[1].query(By.css('input')).nativeElement.sendInput(fine.targetId);
    matFormFields[2].query(By.css('input')).nativeElement.sendInput(fine.amount);
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(fineServiceSpy.addFine).toHaveBeenCalled();
  });

  it('should display required field notification if description is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const description = matFormFields[0];
    let matErrors = description.queryAll(By.directive(MatError));
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = description.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display required field notification if user is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const user = matFormFields[1];
    let matErrors = user.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = user.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display required field notification if amount is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const amount = matFormFields[2];
    let matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display must be numeric field notification if amount is not numeric on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const amount = matFormFields[2];
    let matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    amount.query(By.css('input')).nativeElement.sendInput('abc');
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Must be numeric');
  });

  it('should go back when clicking back', () => {
    const submitButton = fixture.debugElement.query(By.css('button.back'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(locationSpy.back).toHaveBeenCalledTimes(1);
  });
});

describe('AddFinesComponent Error Handling', () => {
  let fixture: ComponentFixture<AddFineComponent>;
  let fineServiceSpy: { addFine: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
        MatSnackBarModule ],
      declarations: [ AddFineComponent ],
      providers: [
        FormBuilder,
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({finesboardId: '1'}) },
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['addFine']) },
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
      ]
    })
    .compileComponents();
  }));

  it('should display notification on add fine failure', () => {
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    fineServiceSpy.addFine.and.returnValue(throwError(new Error('Add fine error')));
    fixture = TestBed.createComponent(AddFineComponent);
    const component = fixture.componentInstance;
    fixture.detectChanges();
    component.fineForm.reason.setValue('Being Unfriendly');
    component.fineForm.user.setValue({ 
      id: 1,
      username: 'Nacho'
    });
    component.fineForm.amount.setValue(10);
    component.onSubmit();
    expect(fineServiceSpy.addFine).toHaveBeenCalled();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Failure: Unable to add fine');
  });
});
