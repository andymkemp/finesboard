import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinesRoutingModule } from './fines-routing.module';
import { FinesComponent } from './fines/fines.component';
import { FinesListComponent } from './fines-list/fines-list.component';
import { UpdateFineComponent } from './fine-forms/update-fine.component';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatFormFieldModule,
  MatInputModule, MatIconModule, MatSnackBarModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddFineComponent } from './add-fine/add-fine.component';

@NgModule({
  declarations: [
    FinesComponent,
    FinesListComponent,
    UpdateFineComponent,
    AddFineComponent,
  ],
  imports: [
    CommonModule,
    FinesRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatSelectModule,
    MatOptionModule
  ]
})
export class FinesModule { }
