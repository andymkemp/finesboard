import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinesComponent } from './fines.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FinesComponent', () => {
  let component: FinesComponent;
  let fixture: ComponentFixture<FinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinesComponent ],
      imports: [ RouterTestingModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
