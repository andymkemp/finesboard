import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl,  } from '@angular/forms';
import { Fine } from '../../models/fine';

export class FineForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder, private fine: Fine) {
        console.log(fine);
        this.form = this.formBuilder.group({
            id: [fine.id],
            description: [fine.reason, Validators.required],
            userId: [fine.targetId , Validators.required],
            amount: [fine.amount, [Validators.required, Validators.pattern(new RegExp('^[0-9]*$'))]],
            finesboardId: [fine.finesboardId],
          });
    }

    get id() { return this.form.get('id'); }
    get description() { return this.form.get('description'); }
    get userId() { return this.form.get('userId'); }
    get amount() { return this.form.get('amount'); }
    get finesboardId() { return this.form.get('finesboardId'); }
}
