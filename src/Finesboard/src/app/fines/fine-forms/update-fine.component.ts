import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FineService } from '../../fine.service';
import { FormBuilder} from '@angular/forms';
import { FinesboardDefaultErrorStateMatcher } from '../../finesboard-error-state-matcher';
import { FineForm } from './fine-form';
import { MessageService } from '../../message.service';

@Component({
  selector: 'app-update-fine',
  templateUrl: './fine-form.component.html',
  styleUrls: ['./fine-form.component.less']
})
export class UpdateFineComponent implements OnInit {

  title = 'Update Fine';
  action = 'Submit';
  fineForm: FineForm;
  matcher = new FinesboardDefaultErrorStateMatcher();

  constructor(private route: ActivatedRoute,
              private fineService: FineService,
              private location: Location,
              private formBuilder: FormBuilder,
              private messaging: MessageService) { }

  ngOnInit(): void {
    this.getFine();
  }

  getFine(): void {
    const finesboardId = this.route.snapshot.parent.paramMap.get('finesboardId');
    const fineId = this.route.snapshot.paramMap.get('fineId');
    this.fineService.getFine(fineId).subscribe(fine => {
      console.log(fine);
      this.fineForm = new FineForm(this.formBuilder, fine);
      console.log(this.fineForm);
    }, () => {
      this.messaging.notify(`Failure: Unable to get fine with id: ${fineId}`);
    });
  }

  onSubmit(): Promise<void> {
    if (this.fineForm.form.invalid) {
      return;
    }
    const fine = this.fineForm.form.value;
    console.log(fine);
    this.fineService.updateFine(fine).subscribe(() => {
      this.messaging.notify('Success: Fine updated');
      this.goBack();
    }, () => {
      this.messaging.notify('Failure: Unable to update fine');
    });
  }

  goBack(): void {
    this.location.back();
  }
}
