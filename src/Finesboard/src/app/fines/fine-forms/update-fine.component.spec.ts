import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UpdateFineComponent } from './update-fine.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatIconModule, MatFormFieldModule,
  MatInputModule, MatSnackBarModule, MatFormField, MatError } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { FineService } from '../../fine.service';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Fine } from '../../models/fine';
import { of, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { MessageService } from '../../message.service';
import { Location } from '@angular/common';
import 'src/testing/element-extensions';

describe('UpdateFinesComponent', () => {
  let component: UpdateFineComponent;
  let fixture: ComponentFixture<UpdateFineComponent>;
  let fineServiceSpy: { getFine: jasmine.Spy, updateFine: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };
  let routerSpy: { navigate: jasmine.Spy };
  let locationSpy: { back: jasmine.Spy };
  let fine: Fine;
  let finesboardId: string;
  let fineId: string;

  beforeEach(async(() => {
    finesboardId = '1';
    fineId = '2';
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
        MatSnackBarModule ],
      declarations: [ UpdateFineComponent ],
      providers: [
        FormBuilder,
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFine', 'updateFine']) },
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({finesboardId, fineId}) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
        { provide: Location, useValue: jasmine.createSpyObj('Location', ['back'])},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fine = {
      id: fineId,
      targetId: '1',
      creatorId: '1',
      reason: 'Being Unfriendly',
      amount: 10,
      finesboardId,
      offenceId: null,
      forfeitId: null,
    };
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    routerSpy = TestBed.get(Router);
    locationSpy = TestBed.get(Location);
    fineServiceSpy.getFine.and.returnValue(of(fine));
    fineServiceSpy.updateFine.and.returnValue(of(new Fine()));
    fixture = TestBed.createComponent(UpdateFineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get fine details for routed fine id on init', () => {
    expect(fineServiceSpy.getFine).toHaveBeenCalledWith(finesboardId, fineId);
  });

  it('should update fine if form is valid on submit', () => {
    component.onSubmit();
    expect(fineServiceSpy.updateFine).toHaveBeenCalledWith(fine);
  });

  it('should go to previous page when update has completed', () => {
    component.onSubmit();
    expect(locationSpy.back).toHaveBeenCalledTimes(1);
  });

  it('should display notification on update success', () => {
    component.onSubmit();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Success: Fine updated');
  });

  it('should not update fine if form is invalid on submit', () => {
    component.fineForm.amount.setValue(null);
    component.onSubmit();
    expect(fineServiceSpy.updateFine).not.toHaveBeenCalled();
  });

  it('should populate form with fine details on init', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const description = matFormFields[0].query(By.css('input')).nativeElement.value;
    expect(description).toBe(fine.reason);
    const user = matFormFields[1].query(By.css('input')).nativeElement.value;
    expect(user).toBe(fine.targetId);
    const amount = matFormFields[2].query(By.css('input')).nativeElement.value;
    expect(amount).toBe(fine.amount.toString());
  });

  it('should call update fine when clicking submit', () => {
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(fineServiceSpy.updateFine).toHaveBeenCalledWith(fine);
  });

  it('should display required field notification if description is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const description = matFormFields[0];
    let matErrors = description.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    description.query(By.css('input')).nativeElement.sendInput(null);
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = description.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display required field notification if user is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const user = matFormFields[1];
    let matErrors = user.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    user.query(By.css('input')).nativeElement.sendInput(null);
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = user.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display required field notification if amount is null on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const amount = matFormFields[2];
    let matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    amount.query(By.css('input')).nativeElement.sendInput(null);
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Required');
  });

  it('should display must be numeric field notification if amount is not numeric on submit', () => {
    const matFormFields = fixture.debugElement.queryAll(By.directive(MatFormField));
    const amount = matFormFields[2];
    let matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(0, 'Pre-condition: Expected no errors');
    amount.query(By.css('input')).nativeElement.sendInput('abc');
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    matErrors = amount.queryAll(By.directive(MatError));
    expect(matErrors.length).toBe(1, 'Expected 1 errors');
    expect(matErrors[0].nativeElement.innerText).toBe('Must be numeric');
  });

  it('should go back when clicking back', () => {
    const submitButton = fixture.debugElement.query(By.css('button.back'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(locationSpy.back).toHaveBeenCalledTimes(1);
  });
});

describe('UpdateFinesComponent Error Handling', () => {
  let fixture: ComponentFixture<UpdateFineComponent>;
  let fineServiceSpy: { getFine: jasmine.Spy, updateFine: jasmine.Spy };
  let messageServiceSpy: { notify: jasmine.Spy };
  let finesboardId: string;
  let fineId: string;

  beforeEach(async(() => {
    finesboardId = '1';
    fineId = '2';
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatFormFieldModule, MatInputModule, MatIconModule, RouterTestingModule, FormsModule, ReactiveFormsModule,
        MatSnackBarModule ],
      declarations: [ UpdateFineComponent ],
      providers: [
        FormBuilder,
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFine', 'updateFine']) },
        { provide: MessageService, useValue: jasmine.createSpyObj('Messaging', ['notify']) },
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({finesboardId, fineId}) },
        { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate'])},
      ]
    })
    .compileComponents();
  }));

  it('should display notification on get fine details failure', () => {
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    fineServiceSpy.getFine.and.returnValue(throwError(new Error('Get fine error')));
    fineServiceSpy.updateFine.and.returnValue(throwError(new Error('Update fine error message')));
    fixture = TestBed.createComponent(UpdateFineComponent);
    fixture.detectChanges();
    expect(fineServiceSpy.getFine).toHaveBeenCalled();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Failure: Unable to get fine with id: 2');
  });

  it('should display notification on update fine details failure', () => {
    fineServiceSpy = TestBed.get(FineService);
    messageServiceSpy = TestBed.get(MessageService);
    const fine = {
      id: '1',
      userId: '1',
      description: 'Being Unfriendly',
      amount: 10
    };
    fineServiceSpy.getFine.and.returnValue(of(fine));
    fineServiceSpy.updateFine.and.returnValue(throwError(new Error('Update fine error message')));
    fixture = TestBed.createComponent(UpdateFineComponent);
    fixture.detectChanges();
    const submitButton = fixture.debugElement.query(By.css('button[type=submit]'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(fineServiceSpy.updateFine).toHaveBeenCalled();
    expect(messageServiceSpy.notify).toHaveBeenCalledWith('Failure: Unable to update fine');
  });
});
