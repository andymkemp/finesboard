import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinesListComponent } from './fines-list/fines-list.component';
import { FinesComponent } from './fines/fines.component';
import { UpdateFineComponent } from './fine-forms/update-fine.component';
import { AddFineComponent } from './add-fine/add-fine.component';
import { AuthGuard } from '../auth/auth.guard';

const finesRoutes: Routes = [
  {
    path: 'fines',
    component: FinesComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: '', component: FinesListComponent },
      { path: 'add', component: AddFineComponent },
      { path: 'update/:fineId', component: UpdateFineComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(finesRoutes)],
  exports: [RouterModule]
})
export class FinesRoutingModule { }
