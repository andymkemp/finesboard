import { Component, OnInit } from '@angular/core';
import { FineService } from '../../fine.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from 'src/app/message.service';
import { FineDetails } from 'src/app/models/fine-details';
import { FinesboardDetails } from 'src/app/models/finesboard-details';
import { FinesboardStatusEnum } from 'src/app/enums/finesboard-status';
import { AuthService } from 'src/app/auth/auth.service';
import { ConfirmationDialogComponent } from 'src/app/common/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material';
import { Fine } from 'src/app/models/fine';
import { RolesEnum } from 'src/app/enums/roles';

@Component({
  selector: 'app-fines-list',
  templateUrl: './fines-list.component.html',
  styleUrls: ['./fines-list.component.less']
})
export class FinesListComponent implements OnInit {

  constructor(private fineService: FineService,
              private route: ActivatedRoute,
              private location: Location,
              private messaging: MessageService,
              private authService: AuthService,
              public dialog: MatDialog) { }
              
  finesboard: FinesboardDetails;
  fines: FineDetails[];
  name: string;
  isAuthenticated: boolean;
  isAdmin: boolean;
  displayColumns: string[];

  ngOnInit() {
    const finesboardId = this.route.snapshot.paramMap.get('finesboardId');
    this.getFinesboard(finesboardId);
    this.getFines(finesboardId);
    this.isAdmin = this.authService.isAuthorized([RolesEnum.Admin]);
    this.displayColumns = ['user','offence','forfeit','finedBy','reason','amount']
    if(this.isAdmin) {
      this.displayColumns.push('delete');
    }
  }

  getFines(finesboardId: string): void {
    this.fineService.getFinesboardFines(finesboardId).subscribe(
      fines => {
        this.fines = fines;
      },
      (err) => {
        this.messaging.notify(`Error: ${err}`);
      }
    );
  }

  getFinesboard(finesboardId: string): void {
    this.fineService.getFinesboard(finesboardId).subscribe(
      finesboard => {
        this.finesboard = finesboard;
        this.name = finesboard.name;
      },
      (err) => {
        this.messaging.notify(`Error: ${err}`);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  isFinesboardActive() {
    // tslint:disable-next-line: triple-equals
    return this.finesboard && this.finesboard.status.id == FinesboardStatusEnum.Active;
  }

  onDelete(fine: Fine) {
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: `Are you sure you want to delete fine?`,
    });
    
    dialogRef.afterClosed().subscribe(confirmation => {
      if(confirmation) {
        this.deleteFine(fine);
      }
    });
  }
  
  deleteFine(fine: Fine) {
    this.fineService.deleteFine(fine.id).subscribe(() => {
      this.fines = this.fines.filter(x => x.id != fine.id);
      this.messaging.notify(`Sucess: Fine deleted`);
    }, (err) => {
      this.messaging.notify(`Failure: Unable to delete fine [${err.error}]`);
    })
  }
}
