import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FinesListComponent } from './fines-list.component';
import { FineService } from '../../fine.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule, MatIconModule, MatTable } from '@angular/material';
import { of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkDirectiveStub } from 'src/testing/router-link-directive-stub';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


describe('FinesListComponent', () => {
  let component: FinesListComponent;
  let fixture: ComponentFixture<FinesListComponent>;
  let finesServiceSpy: { getFines: jasmine.Spy, deleteFines: jasmine.Spy };
  let locationSpy: { back: jasmine.Spy };
  let finesId: string;

  beforeEach(async(() => {
    finesId = '1234567';
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatIconModule, RouterTestingModule ],
      declarations: [ FinesListComponent, RouterLinkDirectiveStub ],
      providers: [
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFines']) },
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({finesId}) },
        { provide: Location, useValue: jasmine.createSpyObj('Location', ['back'])},
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    finesServiceSpy = TestBed.get(FineService);
    locationSpy = TestBed.get(Location);
    finesServiceSpy.getFines.and.returnValue(of([
      { id: '1', description: 'Red Build', user: { id: '1', username: 'NachoFriend'}, surname: 'Andrews', amount: '1' },
      { id: '2', description: 'Bad Joke', user: { id: '1', username: 'RichardRichardson' }, amount: '2' }
    ]));
    fixture = TestBed.createComponent(FinesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get fines', () => {
    expect(finesServiceSpy.getFines).toHaveBeenCalledWith(null);
  });

  it('should have title of all fines', () => {
    const title = fixture.debugElement.query(By.css('.title'));
    expect(title.nativeElement.textContent).toBe('All Fines');
  });

  it('should display table headers', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableHeaders = table.nativeElement.querySelectorAll('th');
    expect(tableHeaders.length).toBe(5);
    expect(tableHeaders[0].textContent).toBe('Fine');
    expect(tableHeaders[1].textContent).toBe('Name');
    expect(tableHeaders[2].textContent).toBe('Amount');
  });

  it('should display fine details', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    expect(tableRows.length).toBe(3); // including header
    let rowCells = tableRows[1].querySelectorAll('td');
    expect(rowCells.length).toBe(5);
    expect(rowCells[0].textContent).toBe('Red Build');
    expect(rowCells[1].textContent).toBe('Andrew Andrews');
    expect(rowCells[2].textContent).toBe('R1.00');
    expect(rowCells[3].textContent).toBe('edit');
    expect(rowCells[4].textContent).toBe('delete');
    rowCells = tableRows[2].querySelectorAll('td');
    expect(rowCells.length).toBe(5);
    expect(rowCells[0].textContent).toBe('Bad Joke');
    expect(rowCells[1].textContent).toBe('Richard Richardson');
    expect(rowCells[2].textContent).toBe('R2.00');
    expect(rowCells[3].textContent).toBe('edit');
    expect(rowCells[4].textContent).toBe('delete');
  });

  it('should have an edit button for each fine', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    let rowCells = tableRows[1].querySelectorAll('td');
    let editLink = rowCells[3].querySelector('a');
    expect(editLink.getAttribute('ng-reflect-router-link')).toBe('./update/1');
    rowCells = tableRows[2].querySelectorAll('td');
    editLink = rowCells[3].querySelector('a');
    expect(editLink.getAttribute('ng-reflect-router-link')).toBe('./update/2');
  });

  it('should have a delete button for each fine', () => {
    const table = fixture.debugElement.query(By.css('table'));
    const tableRows = table.nativeElement.querySelectorAll('tr');
    let rowCells = tableRows[1].querySelectorAll('td');
    let editLink = rowCells[4].querySelector('a');
    expect(editLink.getAttribute('ng-reflect-router-link')).toBe('./delete/1');
    rowCells = tableRows[2].querySelectorAll('td');
    editLink = rowCells[4].querySelector('a');
    expect(editLink.getAttribute('ng-reflect-router-link')).toBe('./delete/2');
  });

  it('should have an add button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    const routerLink = button.injector.get(RouterLinkDirectiveStub);
    expect(routerLink.navigatedTo).toBeNull('should not have navigated yet');
    button.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(routerLink.navigatedTo).toBe('./add');
  });

  it('should go back when clicking back', () => {
    const submitButton = fixture.debugElement.query(By.css('button.back'));
    submitButton.nativeElement.click();
    fixture.detectChanges();
    expect(locationSpy.back).toHaveBeenCalledTimes(1);
  });
});

describe('FinesListComponent Error Handling', () => {
  let component: FinesListComponent;
  let fixture: ComponentFixture<FinesListComponent>;
  let finesServiceSpy: { getFines: jasmine.Spy, deleteFines: jasmine.Spy };
  let id: string;

  beforeEach(async(() => {
    id = '1234567';
    TestBed.configureTestingModule({
      imports: [ NoopAnimationsModule, MatButtonModule, MatToolbarModule, MatTableModule, MatCardModule,
        MatIconModule, RouterTestingModule ],
      declarations: [ FinesListComponent, RouterLinkDirectiveStub ],
      providers: [
        { provide: FineService, useValue: jasmine.createSpyObj('FineService', ['getFines']) },
        { provide: ActivatedRoute, useValue: new ActivatedRouteStub({id}) },
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    finesServiceSpy = TestBed.get(FineService);
    finesServiceSpy.getFines.and.returnValue(throwError('Get fines error'));
    fixture = TestBed.createComponent(FinesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should swallow error on get fines', () => {
    expect((component.fines)).toBeUndefined();
  });

  it('should display empty fines table', () => {
    const table = fixture.debugElement.queryAll(By.directive(MatTable));
    expect(table.length).toBe(0);
  });

  it('should display add button', () => {
    const button = fixture.debugElement.query(By.css('button'));
    expect(button.nativeElement.textContent).toBe('Add');
    expect(button.nativeElement.getAttribute('routerlink')).toBe('./add');
  });
});
