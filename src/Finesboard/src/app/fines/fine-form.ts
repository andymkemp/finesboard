import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl,  } from '@angular/forms';
export class FineForm {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            id: [null],
            reason: [null, Validators.required],
            user: [null , Validators.required],
            amount: [{ value: null, disabled: true }, [Validators.required, Validators.pattern(new RegExp('^[0-9]*$'))]],
            finesboard: [null, Validators.required],
            offence: [null, Validators.required],
            forfeit: [null, Validators.required],
        });
    }

    get id() { return this.form.get('id'); }
    get reason() { return this.form.get('reason'); }
    get user() { return this.form.get('user'); }
    get amount() { return this.form.get('amount'); }
    get finesboard() { return this.form.get('finesboard'); }
    get offence() { return this.form.get('offence'); }
    get forfeit() { return this.form.get('forfeit'); }
}
