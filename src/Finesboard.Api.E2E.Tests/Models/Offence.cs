using System;
using System.Collections.Generic;

namespace Finesboard.Api.E2E.Tests.Models
{
    public class OffenceModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public Guid FinesboardId { get; set; }
        public List<ForfeitModel> Forfeits { get; set; }
    }
    
}