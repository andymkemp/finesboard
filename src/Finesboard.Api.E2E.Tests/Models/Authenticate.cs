namespace Finesboard.Api.E2E.Tests.Models
{
    public class Authenticate
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    
}