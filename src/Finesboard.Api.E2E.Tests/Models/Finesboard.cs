using System;

namespace Finesboard.Api.E2E.Tests.Models
{
    public class FinesboardModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
        public Guid finesboardStatusId { get; set; }
    }
    
}