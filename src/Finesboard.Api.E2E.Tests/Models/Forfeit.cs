using System;

namespace Finesboard.Api.E2E.Tests.Models
{
    public class ForfeitModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public Guid FinesboardId { get; set; }
    }
    
}