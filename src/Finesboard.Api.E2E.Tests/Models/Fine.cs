using System;

namespace Finesboard.Api.E2E.Tests.Models
{
    public class Fine
    {
        public Guid? Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Reason { get; set; }
        public decimal Amount { get; set; }
        public Guid FinesboardId { get; set; }
        public Guid OffenceId { get; set; }
        public Guid ForfeitId { get; set; }
    }    
}