using System.Collections.Generic;
using NUnit.Framework;
using RestSharp;
using System.Net;
using Finesboard.Api.E2E.Tests.Models;
using System;

namespace Finesboard.Api.E2E.Tests
{
    [TestFixture]
    public class FinesTests: TestBase
    {
        [Test]
        public void GetFines_ShouldReturnMultipleResults() 
        {
            var request = new RestRequest("api/fines", Method.GET);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.EqualTo(5).Or.Count.GreaterThan(5));
        }

        [Test]
        public void GetFine_WithValidId_ShouldReturnSingleResult() 
        {
            var request = new RestRequest("api/fines/{id}", Method.GET);
            request.AddUrlSegment("id", "6567072E-DA3A-4436-9C91-AA2800A12BDB");
            var response = _client.Execute<Fine>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void GetFines_WithIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var request = new RestRequest("api/fines/{id}", Method.GET);
            request.AddUrlSegment("id", "437427f8-e2a2-42b3-8e6e-aa26011100db");
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void GetFines_WithInvalidId_ShouldReturnBadRequestStatusCode() 
        {
            var request = new RestRequest("api/fines/{id}", Method.GET);
            request.AddUrlSegment("id", "0");
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"fineId\":[\"The value '0' is not valid.\"]}"));
        }

        [Test]
        public void CreateFine_ShouldReturnCreatedStatusCode() 
        {            
            var request = new RestRequest("api/fines", Method.POST);
            var fine = new Fine() { Firstname = "Nacho", Surname = "Friend", Amount = 999, Reason = "Being Unfriendly", FinesboardId = Guid.Parse("aa2eca98-42cd-4b3b-b6aa-220fa979d511"), OffenceId = Guid.Parse("8bfc5283-560b-44a1-a3b3-fdb4c66c3748"), ForfeitId = Guid.Parse("c455a469-41de-4547-81ca-146c72650d65") };
            request.AddJsonBody(fine);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            Assert.That(response.Data, Is.Not.Empty);
        }

        [Test]
        public void CreateFine_WithInvalidFineBody_ShouldReturnBadRequestStatusCode() 
        {            
            var request = new RestRequest("api/fines", Method.POST);
            var fine = new Fine() { };
            request.AddJsonBody(fine);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Contains.Substring("\"Amount\":[\"Amount must be greater than 0\"]"));
        }

        [Test]
        public void DeleteFine_WithValidId_ShouldreturnOkStatusCode()
        {            
            var request = new RestRequest("api/fines/{id}", Method.DELETE);
            request.AddUrlSegment("id", "6bed683b-d574-4fa5-b47e-032097cb0ae5");
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void DeleteFine_WithIdThatDoesNotExist_ShouldReturnOKStatusCode()
        {            
            var request = new RestRequest("api/fines/{id}", Method.DELETE);
            request.AddUrlSegment("id", "437427f8-e2a2-42b3-8e6e-aa26011100db");
            var response = _client.Execute(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Content, Is.Empty);
        }

        [Test]
        public void DeleteFine_WithInvalidId_ShouldReturnBadRequestStatusCode()
        {            
            var request = new RestRequest("api/fines/{id}", Method.DELETE);
            request.AddUrlSegment("id", "0");
            var response = _client.Execute(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"fineId\":[\"The value '0' is not valid.\"]}"));
        }

        [Test]
        public void GetFinesboardFines_ShouldReturnMultipleResults() 
        {
            var request = new RestRequest("api/fines?finesboardid=4ecf9c20-27fd-4d4d-84dd-a0df7ac11665", Method.GET);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.GreaterThanOrEqualTo(2));
        }
    }
}