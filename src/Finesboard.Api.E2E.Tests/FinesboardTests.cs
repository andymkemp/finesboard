using System.Collections.Generic;
using NUnit.Framework;
using RestSharp;
using System.Net;
using System;
using Finesboard.Api.E2E.Tests.Models;

namespace Finesboard.Api.E2E.Tests
{
    [TestFixture]
    public class FinesBoardTests: TestBase
    {
        [Test]
        public void GetFinesboard_ShouldReturnMultipleResults() 
        {
            var request = new RestRequest("api/finesboards", Method.GET);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.EqualTo(2).Or.Count.GreaterThan(2));
        }

        [Test]
        public void GetFinesboard_WithValidId_ShouldReturnSingleResult() 
        {
            var request = new RestRequest("api/finesboards/{id}", Method.GET);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            var response = _client.Execute<Models.FinesboardModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void GetFinesboard_WithIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var request = new RestRequest("api/finesboards/{id}", Method.GET);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void GetFinesboard_WithInvalidId_ShouldReturnBadRequestStatusCode() 
        {
            var request = new RestRequest("api/finesboards/{id}", Method.GET);
            request.AddUrlSegment("id", "string");
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"finesboardId\":[\"The value 'string' is not valid.\"]}"));
        }

        [Test]
        public void CreateFinesboard_ShouldReturnCreatedStatusCode() 
        {            
            var request = new RestRequest("api/finesboards", Method.POST);
            var finesboard = new Models.FinesboardModel() { Name = "New Board", Currency = "Stars", finesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da") };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            Assert.That(response.Data, Is.Not.Empty);
        }

        [Test]
        public void CreateFinesboard_WithInvalidFineBody_ShouldReturnBadRequestStatusCode() 
        {            
            var request = new RestRequest("api/finesboards", Method.POST);
            var finesboard = new Models.FinesboardModel() { };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Contains.Substring("\"Name\":[\"The Name field is required.\"]"));
        }

        [Test]
        public void UpdateFinesboard_WithValidId_ShouldReturnOKStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.PUT);
            request.AddUrlSegment("id", "aa2eca98-42cd-4b3b-b6aa-220fa979d511");
            var finesboard = new Models.FinesboardModel() { Name = "New Board", Currency = "Stars", finesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da") };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Empty);
        }

        [Test]
        public void UpdateFinesboard_WithIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.PUT);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            var finesboard = new Models.FinesboardModel() { Name = "Nine Nine Nine", Currency = "Stars", finesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da") };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Content, Is.Empty);
        }

        [Test]
        public void UpdateFinesboard_WithInvalidId_ShouldReturnBadRequestStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.PUT);
            request.AddUrlSegment("id", "string");
            var finesboard = new Models.FinesboardModel() { Name = "Invalid Id", Currency = "Stars", finesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da") };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"finesboardId\":[\"The value 'string' is not valid.\"]}"));
        }

        [Test]
        public void UpdateFinesboard_WithInvalidFineBody_ShouldReturnBadRequestStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.PUT);
            request.AddUrlSegment("id", "aa2eca98-42cd-4b3b-b6aa-220fa979d511");
            var finesboard = new Models.FinesboardModel() { };
            request.AddJsonBody(finesboard);
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Contains.Substring("\"Name\":[\"The Name field is required.\"]"));
        }

        [Test]
        public void DeleteFinesboard_WithValidId_ShouldreturnOkStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.DELETE);
            request.AddUrlSegment("id", "a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba");
            var response = _client.Execute<List<Models.FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void DeleteFinesboard_WithIdThatDoesNotExist_ShouldReturnOKStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.DELETE);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            var response = _client.Execute(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Content, Is.Empty);
        }

        [Test]
        public void DeleteFinesboard_WithInvalidId_ShouldReturnBadRequestStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{id}", Method.DELETE);
            request.AddUrlSegment("id", "string");
            var response = _client.Execute(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Is.EqualTo("{\"finesboardId\":[\"The value 'string' is not valid.\"]}"));
        }

        [Test]
        public void GetFinesboardOffence_WithValidFinesboardId_ShouldReturnOKStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{id}/offences", Method.GET);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            var response = _client.Execute<List<Models.OffenceModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void GetFinesboardOffence_WithFinesboardIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var request = new RestRequest("api/finesboards/{id}/offences", Method.GET);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            var response = _client.Execute<List<Models.OffenceModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardOffence_WithValidFinesboardId_ShouldReturnOKStatusCode()
        {
            var forfeit = new Models.ForfeitModel() { Id = Guid.Parse("dd4e93a0-d5c8-416b-afc8-215fd96a8040")};
            var offence = new Models.OffenceModel() { Name = "Being Annoying", FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"), Forfeits = new List<ForfeitModel>() { forfeit } };
            var request = new RestRequest("api/finesboards/{id}/offences", Method.POST);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            request.AddJsonBody(offence);
            var response = _client.Execute<Models.OffenceModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void CreateFinesboardOffence_WithFinesboardIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var offence = new Models.OffenceModel() { Name = "Being Annoying", FinesboardId = Guid.Parse("d066f70b-bb44-4e6e-9252-f7381910d0a1") };
            var request = new RestRequest("api/finesboards/{id}/offences", Method.POST);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            request.AddJsonBody(offence);
            var response = _client.Execute<List<Models.OffenceModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardOffence_WithInvalidFinesboardId_ShouldReturnBadRequestStatusCode() 
        {
            var offence = new { Name = "Being Annoying", FinesboardId = "string" };
            var request = new RestRequest("api/finesboards/{id}/offences", Method.POST);
            request.AddUrlSegment("id", "string");
            request.AddJsonBody(offence);
            var response = _client.Execute<List<Models.OffenceModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardOffence_WithInvalidFineBody_ShouldReturnBadRequestStatusCode() 
        {            
            var offence = new Models.OffenceModel() { }; 
            var request = new RestRequest("api/finesboards/{id}/offences", Method.POST);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            request.AddJsonBody(offence);
            var response = _client.Execute<List<Models.OffenceModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Contains.Substring("\"Name\":[\"The Name field is required.\"]"));
        }

        [Test]
        public void GetFinesboardForfeit_WithValidFinesboardId_ShouldReturnOKStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.GET);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            var response = _client.Execute<List<Models.ForfeitModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void GetFinesboardForfeit__WithFinesboardIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.GET);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            var response = _client.Execute<List<Models.ForfeitModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardForfeit_WithValidFinesboardId_ShouldReturnOKStatusCode()
        {
            var forfeit = new Models.ForfeitModel() { Name = "Thumbs down", Amount = 2, FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665") };
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.POST);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            request.AddJsonBody(forfeit);
            var response = _client.Execute<Models.ForfeitModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void CreateFinesboardForfeit_WithFinesboardIdThatDoesNotExist_ShouldReturnNotFoundStatusCode() 
        {
            var forfeit = new Models.ForfeitModel() { Name = "Thumbs down", Amount = 2, FinesboardId = Guid.Parse("d066f70b-bb44-4e6e-9252-f7381910d0a1") };
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.POST);
            request.AddUrlSegment("id", "d066f70b-bb44-4e6e-9252-f7381910d0a1");
            request.AddJsonBody(forfeit);
            var response = _client.Execute<List<Models.ForfeitModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardForfeit_WithInvalidFinesboardId_ShouldReturnBadRequestStatusCode() 
        {
            var forfeit = new { Name = "Thumbs down", Amount = 2, FinesboardId = "string" };
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.POST);
            request.AddUrlSegment("id", "string");
            request.AddJsonBody(forfeit);
            var response = _client.Execute<List<Models.ForfeitModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Data, Is.Null);
        }

        [Test]
        public void CreateFinesboardForfeit_WithInvalidFineBody_ShouldReturnBadRequestStatusCode() 
        {            
            var forfeit = new Models.ForfeitModel() { }; 
            var request = new RestRequest("api/finesboards/{id}/forfeits", Method.POST);
            request.AddUrlSegment("id", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            request.AddJsonBody(forfeit);
            var response = _client.Execute<List<Models.ForfeitModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
            Assert.That(response.Content, Contains.Substring("\"Name\":[\"The Name field is required.\"]"));
        }

        [Test]
        public void LinkForfeitToOffence_WithValidForfeitIdAndOffenceId_ShouldReturnOKStatusCode()
        {            
            var request = new RestRequest("api/finesboards/{finesboardId}/offences/{offenceId}/forfeits/{forfeitId}", Method.PUT);
            request.AddUrlSegment("finesboardId", "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665");
            request.AddUrlSegment("offenceId", "8bfc5283-560b-44a1-a3b3-fdb4c66c3748");
            request.AddUrlSegment("forfeitId", "7f604bd3-4ddf-4083-af1a-480fb07875b6");
            var response = _client.Execute<Models.OffenceModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void GetFinesboardFines_ShouldReturnMultipleResults() 
        {
            var request = new RestRequest("api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/fines", Method.GET);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.GreaterThanOrEqualTo(2));
        }

        [Test]
        public void GetStatuses_ShouldReturnMultipleResults() 
        {
            var request = new RestRequest("api/finesboards/statuses", Method.GET);
            var response = _client.Execute<List<Fine>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.EqualTo(4));
        }

        [Test]
        public void UpdateForfeit_WithValidId_ShouldReturnOKStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{finesboardId}/forfeits/{forfeitId}", Method.PUT);
            request.AddUrlSegment("finesboardId", "a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba");
            request.AddUrlSegment("forfeitId", "c37f3934-3f03-429d-82e4-466aee056f5e");
            var forfeit = new ForfeitModel() { Name = "Updated Forfeit", Amount = 555 };
            request.AddJsonBody(forfeit);
            var response = _client.Execute<ForfeitModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void ActivateFinesboard_WithValidId_ShouldReturnOKStatusCode() 
        {            
            var request = new RestRequest("api/finesboards/{finesboardId}/activate", Method.PUT);
            request.AddUrlSegment("finesboardId", "a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba");
            var response = _client.Execute<ForfeitModel>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Is.Not.Null);
        }

        [Test]
        public void SelectFinesboard_WithStatusQUery_ShouldReturnOKStatusCode() 
        {            
            var request = new RestRequest("api/finesboards", Method.GET);
            request.AddQueryParameter("status","Pending");
            var response = _client.Execute<List<FinesboardModel>>(request);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(response.Data, Has.Count.GreaterThanOrEqualTo(1));
        }
    }
}