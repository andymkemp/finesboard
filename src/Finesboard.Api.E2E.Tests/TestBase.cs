using RestSharp;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using models = Finesboard.Api.E2E.Tests.Models;
using System.Net;

namespace Finesboard.Api.E2E.Tests
{
    [TestFixture]
    public class TestBase
    {
        protected static IRestClient _client;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(TestContext.CurrentContext.TestDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
            var url = config.GetSection("ApiUrl").Value;
            _client = new RestClient(url);

            var authRequest = new RestRequest("api/users/authenticate", Method.POST);
            var authData = new models.Authenticate() { Username = "user1@gmail.com", Password = "Unknown&1" };
            authRequest.AddJsonBody(authData);
            var response = _client.Execute<models.Authenticated>(authRequest);
            if(response.StatusCode == HttpStatusCode.OK) {
                _client.Authenticator = new RestSharp.Authenticators.JwtAuthenticator(response.Data.Token);
            } else {
                throw new System.Exception($"Authenticate request failed with Status {(int)response.StatusCode}:{response.StatusDescription}");
            }
        }
    }
}