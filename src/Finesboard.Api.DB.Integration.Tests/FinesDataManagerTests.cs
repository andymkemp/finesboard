using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Models;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Finesboard.Api.DB.Integration.Tests
{
    [TestFixture]
    public class FinesDataManagerTests
    {
        private static FinesDataManager _dataManager;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(TestContext.CurrentContext.TestDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
            var statementBuilder = new StatementBuilder();
            var dapper = new DapperWrapper();
            var dbConnectionManager = new DapperConnectionManager(dapper, statementBuilder);
            var dbConnector = new NpgsqlConnector(config, dbConnectionManager);
            _dataManager = new FinesDataManager(dbConnector);
        }

        [Test]
        public void GivenFinesDataManager_WhenQueryingForFineById_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFine(Guid.Parse("537427F8-E2A2-42B3-8E6E-AA26011100DD"));
            // assert
            Assert.That(actualResult, Is.Not.Null, "Should have result");
            Assert.That(actualResult.Finesboard, Is.Not.Null, "Should have finesboard");
            Assert.That(actualResult.Offence, Is.Not.Null, "Should have offence");
            Assert.That(actualResult.Forfeit, Is.Not.Null, "Should have forfeit");
        }

        [Test]
        public void GivenFinesDataManager_WhenQueryingForFinesList_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFinesList();
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(8), "Should have result");
            Assert.That(actualResult.First().Finesboard, Is.Not.Null, "Should have finesboard");
            Assert.That(actualResult.First().Offence, Is.Not.Null, "Should have offence");
            Assert.That(actualResult.First().Forfeit, Is.Not.Null, "Should have forfeit");
        }

        [Test]
        public void GivenFinesDataManager_WhenAddingFine_ShouldInsertAndReturnId()
        {
            // arrange
            var fineInput = new FineDataModel() {
                TargetId = Guid.Parse("501d1a52-dae3-41bc-b312-05175fd68f3a"),
                CreatorId = Guid.Parse("501d1a52-dae3-41bc-b312-05175fd68f3a"),
                Reason = "None",
                Amount = 10,
                FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"),
                OffenceId = Guid.Parse("0da55b8f-d8b4-46cf-b018-9c96f7ee56a1"),
                ForfeitId = Guid.Parse("dd4e93a0-d5c8-416b-afc8-215fd96a8040")
            }; 
            // act
            var actualResult = _dataManager.AddFine(fineInput);
            // assert
            Assert.That(actualResult.Id, Is.Not.Null, "Should return Id");
            var fineOutput = _dataManager.GetFine((Guid)actualResult.Id);
            Assert.That(fineOutput.Target.Id, Is.EqualTo(fineInput.TargetId), "Should have inserted record");
        }

        [Test]
        public void GivenFinesDataManager_WhenUpdatingFine_ShouldUpdateAndReturnNumberOfAffectedRecords()
        {
            // arrange
            var fineInput = new FineDataModel() {
                Id = Guid.Parse("537427F8-E2A2-42B3-8E6E-AA26011100DD"),
                TargetId = Guid.Parse("501d1a52-dae3-41bc-b312-05175fd68f3a"),
                CreatorId = Guid.Parse("d871f442-e6b8-456c-96b9-77b8f0ef8c5e"),
                Reason = "None",
                Amount = 999,
                FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"),
                OffenceId = Guid.Parse("0da55b8f-d8b4-46cf-b018-9c96f7ee56a1"),
                ForfeitId = Guid.Parse("dd4e93a0-d5c8-416b-afc8-215fd96a8040")
            };
            // act
            var actualResult = _dataManager.UpdateFine(fineInput);
            // assert
            Assert.That(actualResult, Is.EqualTo(1), "Should return number of affected records");
            var fineOutput = _dataManager.GetFine((Guid)fineInput.Id);
            Assert.That(fineOutput.Target.Id, Is.EqualTo(fineInput.TargetId), "Should have updated record");
        }

        [Test]
        public void GivenFinesDataManager_WhenDeletingFine_ShouldUpdateStatusToArchived()
        {
            // arrange
            var id = Guid.Parse("3895F8F3-FA62-4F60-890B-AA2800A12BDB");
            // act
            _dataManager.DeleteFine(id);
            // assert
            var fineOutput = _dataManager.GetFine(id);
            Assert.That(fineOutput, Is.Null, "Should have updated record");
        }
        
        [Test]
        public void GivenFinesDataManager_WhenQueryingForFinesboardFines_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFinesboardFines(Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"));
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(2), "Should have result");
            Assert.That(actualResult.First().Finesboard, Is.Not.Null, "Should have finesboard");
            Assert.That(actualResult.First().Offence, Is.Not.Null, "Should have offence");
            Assert.That(actualResult.First().Forfeit, Is.Not.Null, "Should have forfeit");
        }
    }
}