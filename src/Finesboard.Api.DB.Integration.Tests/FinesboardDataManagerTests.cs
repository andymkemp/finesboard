using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Models;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Finesboard.Api.DB.Integration.Tests
{
    [TestFixture]
    public class FinesboardDataManagerTests
    {
        private static FinesboardDataManager _dataManager;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(TestContext.CurrentContext.TestDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
            var statementBuilder = new StatementBuilder();
            var dapper = new DapperWrapper();
            var dbConnectionManager = new DapperConnectionManager(dapper, statementBuilder);
            var dbConnector = new NpgsqlConnector(config, dbConnectionManager);
            _dataManager = new FinesboardDataManager(dbConnector);
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForFinesboard_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFinesboard(Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"));
            // assert
            Assert.That(actualResult, Is.Not.Null, "Should have result");
            Assert.That(actualResult.Offences, Has.Count.GreaterThanOrEqualTo(1), "Should have offences");
            Assert.That(actualResult.Offences.First().Forfeits, Has.Count.GreaterThanOrEqualTo(1), "Should have forfeits");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForFinesboardList_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFinesboardList();
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(3), "Should have result");
            Assert.That(actualResult.First().Offences, Has.Count.GreaterThanOrEqualTo(1), "Should have offences");
            Assert.That(actualResult.First().Offences.First().Forfeits, Has.Count.GreaterThanOrEqualTo(1), "Should have forfeits");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenAddingFinesboard_ShouldInsertAndReturnId()
        {
            // arrange
            var finesboardInput = new FinesboardDataModel() {
                Name = "New Finesboard",
                Currency = "Rand",
                FinesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da"),
            };
            // act
            var actualResult = _dataManager.AddFinesboard(finesboardInput);
            // assert
            Assert.That(actualResult.Id, Is.Not.Null, "Should return Id");
            var finesboardOutput = _dataManager.GetFinesboard((Guid)actualResult.Id);
            Assert.That(finesboardOutput.Name, Is.EqualTo(finesboardInput.Name), "Should have inserted record");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenUpdatingFinesboard_ShouldUpdateAndReturnNumberofAffectedRecords()
        {
            // arrange
            var finesboardInput = new FinesboardDataModel() {
                Id = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"),
                Name = "Pizza Face",
                Currency = "Pizza",
                FinesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da"),
            };
            // act
            var actualResult = _dataManager.UpdateFinesboard(finesboardInput);
            // assert
            Assert.That(actualResult, Is.EqualTo(1), "Should return number of affected records");
            var finesboardOutput = _dataManager.GetFinesboard((Guid)finesboardInput.Id);
            Assert.That(finesboardOutput.Name, Is.EqualTo(finesboardInput.Name), "Should have updated record");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenDeletingFinesboard_ShouldUpdateStatusToArchived()
        {
            // arrange
            var finesboardId = Guid.Parse("a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba");
            var expectedStatus = Guid.Parse("df216125-9ab9-4fa0-9267-c9facc0c7c18");
            // act
            _dataManager.DeleteFinesboard(finesboardId);
            // assert
            var finesboard = _dataManager.GetFinesboard(finesboardId);
            Assert.That(finesboard.Status.Id, Is.EqualTo(expectedStatus), "Should set status to Archived");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForOffences_ShouldReturnListOfOffences()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetOffences(Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"));
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(1), "Should have result");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForOffence_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetOffence(Guid.Parse("0da55b8f-d8b4-46cf-b018-9c96f7ee56a1"));
            // assert
            Assert.That(actualResult, Is.Not.Null, "Should have result");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenAddingOffence_ShouldInsertAndReturnId()
        {
            // arrange
            var offenceInput = new OffenceDataModel() {
                Name = "New Offence",
                FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665")
            };
            // act
            var actualResult = _dataManager.AddOffence(offenceInput);
            // assert
            Assert.That(actualResult.Id, Is.Not.Null, "Should return Id");
            var offenceOutput = _dataManager.GetOffence((Guid)actualResult.Id);
            Assert.That(offenceOutput.Name, Is.EqualTo(offenceInput.Name), "Should have inserted record");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForForfeits_ShouldReturnListOfForfeits()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetForfeits(Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"));
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(1), "Should have result");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForForfeits_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetForfeit(Guid.Parse("dd4e93a0-d5c8-416b-afc8-215fd96a8040"));
            // assert
            Assert.That(actualResult, Is.Not.Null, "Should have result");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenAddingForfeit_ShouldInsertAndReturnId()
        {
            // arrange
            var forfeitInput = new ForfeitDataModel() {
                Name = "New forfeit",
                Amount = 1,
                FinesboardId = Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665")
            };
            // act
            var actualResult = _dataManager.AddForfeit(forfeitInput);
            // assert
            Assert.That(actualResult.Id, Is.Not.Null, "Should return Id");
            var forfeitOutput = _dataManager.GetForfeit((Guid)actualResult.Id);
            Assert.That(forfeitOutput.Name, Is.EqualTo(forfeitInput.Name), "Should have inserted record");
        }
        
        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForFinesboardFines_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetFinesboardFines(Guid.Parse("4ecf9c20-27fd-4d4d-84dd-a0df7ac11665"));
            // assert
            Assert.That(actualResult, Has.Count.GreaterThanOrEqualTo(2), "Should have result");
            Assert.That(actualResult.First().Finesboard, Is.Not.Null, "Should have finesboard");
            Assert.That(actualResult.First().Offence, Is.Not.Null, "Should have offence");
            Assert.That(actualResult.First().Forfeit, Is.Not.Null, "Should have forfeit");
        }
        
        [Test]
        public void GivenFinesboardDataManager_WhenQueryingForFinesboardStatusList_ShouldReturnResultSet()
        {
            // arrange
            // act
            var actualResult = _dataManager.GetStatusList();
            // assert
            Assert.That(actualResult, Has.Count.EqualTo(4), "Should have result");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenUpdatingForfeit_ShouldUpdateAndReturnNumberofAffectedRecords()
        {
            // arrange
            var forfeitInput = new ForfeitDataModel() {
                Id = Guid.Parse("c37f3934-3f03-429d-82e4-466aee056f5e"),
                Name = "Updated",
                Amount = 999,
                FinesboardId = Guid.Parse("a59ae5a7-9d17-4ff0-97e7-f2ebab57b1ba")
            };
            // act
            var actualResult = _dataManager.UpdateForfeit(forfeitInput);
            // assert
            Assert.That(actualResult, Is.EqualTo(1), "Should return number of affected records");
            var forfeitOutput = _dataManager.GetForfeit((Guid)forfeitInput.Id);
            Assert.That(forfeitOutput.Name, Is.EqualTo(forfeitInput.Name), "Should have updated record");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenGettingFinesboardByStatus_ShouldReturnOnlyRecordsThatMatchThatStatus()
        {
            // arrange
            var finesboardInput = new FinesboardDataModel() {
                Name = "Pizza Face",
                Currency = "Pizza",
                FinesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da"),
            };
            _dataManager.AddFinesboard(finesboardInput);
            _dataManager.AddFinesboard(finesboardInput);
            var status = "Pending";
            // act
            var actualResult = _dataManager.GetFinesboardsByStatus(status);
            // assert
            Assert.That(actualResult, Has.Count.GreaterThan(1), "Should return multiple results");
            Assert.That(actualResult.Where(finesboard => finesboard.Status.Description == status).ToList(), 
                Has.Count.EqualTo(actualResult.Count()), 
                "All records should have the same status");
        }

        [Test]
        public void GivenFinesboardDataManager_WhenActivatingFinesboard_ShouldSetFinesboardStatusToActive()
        {
            // arrange
            var expectedStatus = "Active";
            var finesboardInput = new FinesboardDataModel() {
                Name = "Pizza Face",
                Currency = "Pizza",
                FinesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da"),
            };
            var finesboard = _dataManager.AddFinesboard(finesboardInput);
            // act
            _dataManager.ActivateFinesboard((Guid)finesboard.Id);
            // assert
            var actualResult = _dataManager.GetFinesboard((Guid)finesboard.Id);
            Assert.That(actualResult.Status.Description, Is.EqualTo(expectedStatus), "Should update status to active");
        }
    }
}