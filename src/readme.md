# DB
* Postgres db (docker image)
* flyway db source countrol (docker image)
* node gulp build
# Web Api
* asp.net core api
* dapper 
* Npgsql
* Nunit unit tests
* SoapUI e2e tests
* dotnet cli build
# Web UI
* Angular
* Angular Material
* Karma, Jasmine unit tests
* Protractor, Jasmine e2e tests
* ng cli build