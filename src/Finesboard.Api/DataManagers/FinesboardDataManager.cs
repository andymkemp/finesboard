using System;
using System.Collections.Generic;
using System.Linq;
using Finesboard.Api.Constants;
using Finesboard.Api.DataManagers.Statements;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public class FinesboardDataManager: IFinesboardDataManager
    {        
        private readonly IDatabaseConnector _db;
        public FinesboardDataManager(IDatabaseConnector databaseConnector)
        {
            _db = databaseConnector;
        }

        public FinesboardDetailDataModel GetFinesboard(Guid finesboardId)
        {            
            using(var conn = _db.Connect())
            {
                
                var statement = new GetFinesboardByIdStatement(finesboardId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public IEnumerable<FinesboardDetailDataModel> GetFinesboardList()
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesboardListStatement();
                var finesboards = conn.Query(statement);
                return finesboards;
            }
        }
        public IEnumerable<FinesboardDetailDataModel> GetFinesboardsByStatus(string status)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesboardByStatusStatement(status);
                var finesboards = conn.Query(statement);
                return finesboards;
            }
        }

        public FinesboardDataModel AddFinesboard(FinesboardDataModel finesboard)
        { 
            using(var conn = _db.Connect())
            {
                finesboard.Id = Guid.NewGuid();
                conn.Insert(finesboard);
                return finesboard;       
            }
        }

        public int UpdateFinesboard(FinesboardDataModel finesboard)
        {
            using(var conn = _db.Connect())
            {
                return conn.Update(finesboard);
            }
        }

        public int DeleteFinesboard(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                return conn.Delete<FinesboardDataModel>(finesboardId);
            }
        }

        public int ActivateFinesboard(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                var finesboard = conn.SelectById<FinesboardDataModel>(finesboardId);
                finesboard.FinesboardStatusId = FinesboardStatus.Active;
                return conn.Update<FinesboardDataModel>(finesboard);
            }
        }

        public int DeactivateFinesboard(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                var finesboard = conn.SelectById<FinesboardDataModel>(finesboardId);
                finesboard.FinesboardStatusId = FinesboardStatus.Closed;
                return conn.Update<FinesboardDataModel>(finesboard);
            }
        }

        public IEnumerable<OffenceDataModel> GetOffences(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<OffenceDataModel>(new {FinesboardId = finesboardId});
            }
        }

        public OffenceDataModel GetOffence(Guid offenceId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectById<OffenceDataModel>(offenceId);
            }
        }

        public OffenceDataModel AddOffence(OffenceDataModel offence)
        {
            using(var conn = _db.Connect())
            {
                offence.Id = Guid.NewGuid();                
                conn.Insert(offence);
                return offence;
            }
        }

        public IEnumerable<ForfeitDataModel> GetForfeits(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<ForfeitDataModel>(new {FinesboardId = finesboardId});
            }
        }

        public ForfeitDataModel GetForfeit(Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectById<ForfeitDataModel>(forfeitId);
            }
        }

        public ForfeitDataModel AddForfeit(ForfeitDataModel forfeit)
        {
            using(var conn = _db.Connect())
            {
                forfeit.Id = Guid.NewGuid();                
                conn.Insert(forfeit);
                return forfeit;
            }
        }
        public OffenceForfeitDataModel AddOffenceForfeit(Guid offenceId, Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                var offenceForfeit = new OffenceForfeitDataModel() {
                    Id = Guid.NewGuid(),
                    OffenceId = offenceId,
                    ForfeitId = forfeitId,
                    Status = 1
                };
                conn.Insert(offenceForfeit);
                return offenceForfeit;
            }
        }
        public IEnumerable<ForfeitDataModel> GetOffenceForfeits(Guid offenceId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetForfeitsLinkedToOffenceStatement(offenceId);
                return conn.Query(statement);
            }
        }

        public IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesForFinesboardStatement(finesboardId);
                return conn.Query(statement);
            }
        }

        public OffenceDetailDataModel GetOffenceDetails(Guid offenceId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetOffenceDetailsStatement(offenceId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public IEnumerable<FinesboardStatusDataModel> GetStatusList()
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<FinesboardStatusDataModel>();
            }
        }

        public int UpdateForfeit(ForfeitDataModel forfeit)
        {
            using(var conn = _db.Connect())
            {
                return conn.Update(forfeit);
            }
        }
        
        public IEnumerable<UserLoginDataModel> GetFinesboardUsers(Guid finesboardId) 
        {            
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesboardUsersStatement(finesboardId);
                return conn.Query(statement);
            }
        }

        public bool IsFinesboardUser(Guid finesboardId, string username)
        {
            using(var conn = _db.Connect())
            {
                var statement = new IsFinesboardUserStatement(finesboardId, username);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public int JoinFinesboard(Guid finesboardId, Guid userLoginId)
        {            
            using(var conn = _db.Connect())
            {
                var finesboardUser = new FinesboardUserDataModel() {
                    Id = Guid.NewGuid(),
                    UserLoginId = userLoginId,
                    FinesboardId = finesboardId
                };
                return conn.Insert(finesboardUser);
            }
        }

        public int LeaveFinesboard(Guid finesboardId, Guid userLoginId) {
            using(var conn = _db.Connect())
            {
                return conn.Delete<FinesboardUserDataModel>(new {FinesboardId = finesboardId, UserLoginId = userLoginId });
            }
        }
        
        public bool IsOffenceLinkedToFine(Guid offenceId)
        {      
            using(var conn = _db.Connect())
            {
                var statement = new IsOffenceLinkedToFineStatement(offenceId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public bool IsForfeitLinkedToFine(Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new IsForfeitLinkedToFineStatement(forfeitId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public bool IsOffenceForfeitLinkedToFine(Guid offenceId, Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new IsOffenceForfeitLinkedToFineStatement(offenceId, forfeitId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public IEnumerable<OffenceForfeitDataModel> GetOffenceForfeitsByForfeitId(Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<OffenceForfeitDataModel>(new {ForfeitId = forfeitId});
            }
        }

        public IEnumerable<OffenceForfeitDataModel> GetOffenceForfeitsByOffenceId(Guid offenceId)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<OffenceForfeitDataModel>(new {OffenceId = offenceId});
            }
        }

        public OffenceForfeitDataModel GetFinesboardOffenceForfeit(Guid finesboardId, Guid offenceId, Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesboardOffenceForfeitStatement(finesboardId, offenceId, forfeitId);
                return conn.Query(statement).FirstOrDefault();
            }
        }

        public int DeleteOffence(Guid offenceId)
        {
            using(var conn = _db.Connect())
            {
                return conn.Delete<OffenceDataModel>(offenceId);
            }
        }

        public int DeleteForfeit(Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                return conn.Delete<ForfeitDataModel>(forfeitId);
            }
        }

        public int DeleteOffenceForfeit(Guid offenceId, Guid forfeitId)
        {
            using(var conn = _db.Connect())
            {
                return conn.Delete<OffenceForfeitDataModel>(new { OffenceId = offenceId, ForfeitId = forfeitId });
            }
        }

        public int UpdateOffenceForfeitStatus(Guid offenceId, Guid forfeitId, int status) {
            using(var conn = _db.Connect())
            {
                var offenceForfeit = conn.SelectList<OffenceForfeitDataModel>(new {OffenceId = offenceId, ForfeitId = forfeitId}).First();
                offenceForfeit.Status = status;
                return conn.Update<OffenceForfeitDataModel>(offenceForfeit);
            }
        }
    }    
}