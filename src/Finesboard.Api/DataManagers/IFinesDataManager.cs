using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public interface IFinesDataManager
    {
        FineDetailDataModel GetFine(Guid fineId);
        FineDetailDataModel GetFine(Guid fineId, string username);
        IEnumerable<FineDetailDataModel> GetFinesList();
        IEnumerable<FineDetailDataModel> GetFinesList(string username);
        FineDataModel AddFine(FineDataModel fine);
        int UpdateFine(FineDataModel fine);
        int DeleteFine(Guid fineId);
        IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId);
        IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId, string username);
    }    
}