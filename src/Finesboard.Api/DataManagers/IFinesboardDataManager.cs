using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public interface IFinesboardDataManager
    {
        FinesboardDetailDataModel GetFinesboard(Guid finesboardId);
        FinesboardDataModel AddFinesboard(FinesboardDataModel finesboard);
        IEnumerable<FinesboardDetailDataModel> GetFinesboardList();
        int UpdateFinesboard(FinesboardDataModel finesboard);
        int DeleteFinesboard(Guid finesboardId);
        IEnumerable<OffenceDataModel> GetOffences(Guid finesboardId);
        OffenceDataModel GetOffence(Guid offenceId);
        OffenceDataModel AddOffence(OffenceDataModel fine);
        IEnumerable<ForfeitDataModel> GetForfeits(Guid finesboardId);
        ForfeitDataModel GetForfeit(Guid forfeitId);
        ForfeitDataModel AddForfeit(ForfeitDataModel forfeit);
        OffenceForfeitDataModel AddOffenceForfeit(Guid offenceId, Guid forfeitId);
        IEnumerable<ForfeitDataModel> GetOffenceForfeits(Guid offenceId);
        IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId);
        OffenceDetailDataModel GetOffenceDetails(Guid offenceId);
        IEnumerable<FinesboardStatusDataModel> GetStatusList();
        int UpdateForfeit(ForfeitDataModel forfeit);
        int ActivateFinesboard(Guid finesboardId);
        int DeactivateFinesboard(Guid finesboardId);
        IEnumerable<FinesboardDetailDataModel> GetFinesboardsByStatus(string status);
        IEnumerable<UserLoginDataModel> GetFinesboardUsers(Guid finesboardId);
        bool IsFinesboardUser(Guid finesboardId, string username);
        int JoinFinesboard(Guid finesboardId, Guid userLoginId);
        int LeaveFinesboard(Guid finesboardId, Guid userLoginId);
        bool IsOffenceLinkedToFine(Guid offenceId);
        bool IsForfeitLinkedToFine(Guid forfeitId);
        bool IsOffenceForfeitLinkedToFine(Guid offenceforfeitId, Guid forfeitId);
        IEnumerable<OffenceForfeitDataModel> GetOffenceForfeitsByForfeitId(Guid forfeitId);
        IEnumerable<OffenceForfeitDataModel> GetOffenceForfeitsByOffenceId(Guid offenceId);
        OffenceForfeitDataModel GetFinesboardOffenceForfeit(Guid finesboardId, Guid offenceId, Guid forfeitId);
        int DeleteOffence(Guid offenceId);
        int DeleteForfeit(Guid forfeitId);
        int DeleteOffenceForfeit(Guid offenceId, Guid forfeitId);
        int UpdateOffenceForfeitStatus(Guid offenceId, Guid forfeitId, int status);
    }    
}