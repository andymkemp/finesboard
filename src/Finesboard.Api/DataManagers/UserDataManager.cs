using System;
using System.Collections.Generic;
using System.Linq;
using Finesboard.Api.DataManagers.Statements;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public class UserDataManager: IUserDataManager
    {        
        private readonly IDatabaseConnector _db;
        public UserDataManager(IDatabaseConnector databaseConnector)
        {
            _db = databaseConnector;
        }

        public UserLoginDataModel GetUserById(Guid id)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<UserLoginDataModel>(new {Id = id}).FirstOrDefault();
            }
        }

        public UserLoginDataModel GetUserByUsername(string username)
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<UserLoginDataModel>(new {Username = username}).FirstOrDefault();
            }
        }

        public IEnumerable<UserLoginDataModel> GetUsersList()
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<UserLoginDataModel>();
            }
        }

        public UserLoginDataModel AddUser(UserLoginDataModel user)
        {
            using(var conn = _db.Connect())
            {
                user.Id = Guid.NewGuid();
                var result = conn.Insert(user);
                if(result == 0)
                {
                    return null;
                }
                else
                {
                    return user;
                }
            }
        }

        public IEnumerable<UserRoleDataModel> GetUserRoles(Guid userLoginId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetUserRolesStatement(userLoginId);
                var result = conn.Query(statement);
                return result;
            }
        }

        public IEnumerable<FinesboardDetailDataModel> GetUserFinesboardsList(Guid userLoginId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetUserFinesboardsListStatement(userLoginId);
                var result = conn.Query(statement);
                return result;
            }
        }

        public UserDetailDataModel GetUserDetails(Guid userLoginId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetUserDetailsByIdStatement(userLoginId);
                var result = conn.Query(statement);
                return result.FirstOrDefault();
            }
        }

        public IEnumerable<FinesboardDetailDataModel>  GetUserFinesboardsListByStatus(Guid userLoginId, string status)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetUserFinesboardsListByStatusStatement(userLoginId, status);
                var result = conn.Query(statement);
                return result;
            }
        }

        public int ChangePassword(Guid userId, string password)
        {
            using(var conn = _db.Connect())
            {
                var user = conn.SelectById<UserLoginDataModel>(userId);
                user.Password = password;
                return conn.Update(user);
            }
        }

        public IEnumerable<FineDataModel> GetFinesboardFinesByTarget(Guid finesboardId, Guid targetId) 
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<FineDataModel>(new { FinesboardId = finesboardId, TargetId = targetId });
            }
        }

        public IEnumerable<FineDataModel> GetFinesboardFinesByCreator(Guid finesboardId, Guid creatorId) 
        {
            using(var conn = _db.Connect())
            {
                return conn.SelectList<FineDataModel>(new { FinesboardId = finesboardId, CreatorId = creatorId });
            }
        }
    }    
}