using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public interface IUserDataManager
    {
        UserLoginDataModel GetUserById(Guid id);
        UserLoginDataModel GetUserByUsername(string username);
        IEnumerable<UserLoginDataModel> GetUsersList();
        UserLoginDataModel AddUser(UserLoginDataModel userLogin);
        IEnumerable<UserRoleDataModel> GetUserRoles(Guid userLoginId);
        IEnumerable<FinesboardDetailDataModel> GetUserFinesboardsList(Guid userLoginId);
        UserDetailDataModel GetUserDetails(Guid userLoginId);
        IEnumerable<FinesboardDetailDataModel>  GetUserFinesboardsListByStatus(Guid userLoginId, string status);
        int ChangePassword(Guid userId, string password);
        IEnumerable<FineDataModel> GetFinesboardFinesByTarget(Guid finesboardId, Guid targetId);
        IEnumerable<FineDataModel> GetFinesboardFinesByCreator(Guid finesboardId, Guid creatorId);
    }    
}