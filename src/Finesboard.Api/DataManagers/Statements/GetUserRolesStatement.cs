using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetUserRolesStatement : IStatement<UserRoleDataModel>
    {
        public Guid UserLoginId { get; set; }

        public GetUserRolesStatement(Guid userLoginId)
        {
            UserLoginId = userLoginId;
        }

        public string SplitOn()
        {
            return "id";
        }

        public string Statement()
        {
            return @"SELECT 
                ur.id,
                urt.code,
                urt.description
            FROM 
                finesboard.public.userRole ur
                JOIN finesboard.public.userRoleType urt on ur.userRoleTypeId = urt.id
            WHERE ur.userId = @UserLoginId";
        }
    }
}