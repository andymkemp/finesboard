using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetForfeitsLinkedToOffenceStatement : IStatement<ForfeitDataModel>
    {
        public Guid Id { get; set; }

        public GetForfeitsLinkedToOffenceStatement(Guid offenceId)
        {
            Id = offenceId;
        }

        public string Statement()
        {
            return @"SELECT
                f.*
            FROM
                finesboard.public.offenceforfeit l
                JOIN finesboard.public.forfeit f on l.forfeitId = f.id
            WHERE
                l.offenceId = @Id";
        }
    }
}