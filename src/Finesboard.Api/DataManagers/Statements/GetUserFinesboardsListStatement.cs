using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetUserFinesboardsListStatement : IStatement<FinesboardDetailDataModel,FinesboardStatusDataModel,IList<OffenceDetailDataModel>,FinesboardDetailDataModel>
    {
        public Guid UserId { get; set; }        
        public GetUserFinesboardsListStatement(Guid userId)
        {
            this.UserId = userId;
        }
        public string Statement()
        {
            return @"SELECT
                    fb.*,
                    fs.*,
                    (SELECT 
                        array_to_json(array_agg(row_to_json(offences)))
                    FROM (
                        SELECT 
                            id,
                            name,
                            finesboardId,
                            (SELECT 
                                array_to_json(array_agg(row_to_json(ff)))
                            FROM (select ff.*, oft.status
	                            FROM 
	                                finesboard.public.offenceForfeit oft
	                                JOIN finesboard.public.forfeit ff ON oft.forfeitId = ff.id
	                            WHERE 
	                                o.id = oft.offenceId
	                            ) as ff
                            ) AS forfeits
                        FROM 
                            finesboard.public.offence o
                        WHERE fb.id = o.finesboardId
                    ) AS offences) AS offences
                FROM 
                    finesboard.public.userLogin ul
                    JOIN finesboard.public.finesboardUser fu on ul.id = fu.userLoginId
                    JOIN finesboard.public.finesboard fb on fu.finesboardId = fb.id
                    JOIN finesboard.public.finesboardStatus fs ON fb.finesboardStatusId = fs.id
                WHERE 
                    ul.id = @UserId";
        }

        public Func<FinesboardDetailDataModel, FinesboardStatusDataModel, IList<OffenceDetailDataModel>, FinesboardDetailDataModel> Map()
        {
            return (FinesboardDetailDataModel finesboard, FinesboardStatusDataModel finesboardStatus, IList<OffenceDetailDataModel> offences) =>
            {
                finesboard.Status = finesboardStatus;
                finesboard.Offences = offences;
                return finesboard;
            };
        }
        
        public string SplitOn()
        {
            return "id,offences";
        }
    }
}