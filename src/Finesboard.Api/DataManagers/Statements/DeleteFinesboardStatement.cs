using System;

namespace Finesboard.Api.DataManagers
{
    public class DeleteFinesboardStatement : IStatement<int>
    {
        public Guid Id { get; set; }
        public DeleteFinesboardStatement(Guid finesboardId)
        {
            Id = finesboardId;
        }
        public string Statement()
        {
            return @"Select delete_finesboard(@Id)";
        }
    }
}
