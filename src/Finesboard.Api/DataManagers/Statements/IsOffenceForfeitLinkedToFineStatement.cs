using System;

namespace Finesboard.Api.DataManagers.Statements
{
    public class IsOffenceForfeitLinkedToFineStatement : IStatement<bool>
    {
        public Guid OffenceId { get; set; }
        public Guid ForfeitId { get; set; }

        public IsOffenceForfeitLinkedToFineStatement(Guid offenceId, Guid forfeitId)
        {
            OffenceId = offenceId;
            ForfeitId = forfeitId;
        }

        public string Statement()
        {
            return @"SELECT
                CASE WHEN COUNT(*) > 0 then CAST(1 as bit) else CAST(0 as bit) end
            FROM 
                finesboard.public.fine
            WHERE
                offenceId = @OffenceId
                AND forfeitId = @ForfeitId
            LIMIT 1";
        }
    }
}