using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetFinesboardUsersStatement : IStatement<UserLoginDataModel>
    {
        public Guid FinesboardId { get; set; }

        public GetFinesboardUsersStatement(Guid finesboardId)
        {
            FinesboardId = finesboardId;
        }

        public string Statement()
        {
            return @"SELECT
                ul.*
            FROM 
                finesboard.public.userLogin ul
                JOIN finesboard.public.finesboardUser fu on ul.id = fu.userLoginId
                JOIN finesboard.public.finesboard f on fu.finesboardId = f.id
            WHERE
                f.id = @FinesboardId";
        }
    }
}