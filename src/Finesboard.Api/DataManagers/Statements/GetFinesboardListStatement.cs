using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetFinesboardListStatement : IStatement<FinesboardDetailDataModel,FinesboardStatusDataModel,IList<OffenceDetailDataModel>, IList<ForfeitDataModel>,FinesboardDetailDataModel>
    {
        public string Statement()
        {
            return @"SELECT
                    fb.*,
                    fs.*,
                    (SELECT 
                        array_to_json(array_agg(row_to_json(offences)))
                    FROM (
                        SELECT 
                            id,
                            name,
                            finesboardId,
                            (SELECT 
                                array_to_json(array_agg(row_to_json(ff)))
                            FROM (select ff.*, oft.status
	                            FROM 
	                                finesboard.public.offenceForfeit oft
	                                JOIN finesboard.public.forfeit ff ON oft.forfeitId = ff.id
	                            WHERE 
	                                o.id = oft.offenceId
	                            ) as ff
                            ) AS forfeits
                        FROM 
                            finesboard.public.offence o
                        WHERE fb.id = o.finesboardId
                    ) AS offences) AS offences,
                    (SELECT 
                            array_to_json(array_agg(row_to_json(ff)))
                        FROM 
                            finesboard.public.forfeit ff
                        WHERE 
                            fb.id = ff.finesboardId
                    ) AS forfeits
                FROM 
                    finesboard.public.finesboard fb
                    JOIN finesboard.public.finesboardStatus fs ON fb.finesboardStatusId = fs.id";
        }

        public Func<FinesboardDetailDataModel, FinesboardStatusDataModel, IList<OffenceDetailDataModel>, IList<ForfeitDataModel>, FinesboardDetailDataModel> Map()
        {
            return (FinesboardDetailDataModel finesboard, FinesboardStatusDataModel finesboardStatus, IList<OffenceDetailDataModel> offences, IList<ForfeitDataModel> forfeits) =>
            {
                finesboard.Status = finesboardStatus;
                finesboard.Offences = offences;
                finesboard.Forfeits = forfeits;
                return finesboard;
            };
        }
        
        public string SplitOn()
        {
            return "id,offences,forfeits";
        }
    }
}