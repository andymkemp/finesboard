using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetFinesForFinesboardStatement : IStatement<FineDetailDataModel, UserLoginDataModel, UserLoginDataModel, FinesboardDetailDataModel, FinesboardStatusDataModel, OffenceDataModel, ForfeitDataModel, FineDetailDataModel>
    {
        public Guid FinesboardId { get; set; }

        public GetFinesForFinesboardStatement(Guid finesboardId)
        {
            FinesboardId = finesboardId;
        }

        public string Statement()
        {
            return @"SELECT
                f.id, f.reason, f.amount, t.*, c.*, fb.*, ft.*, o.*, ff.*
            FROM 
                finesboard.public.fine f
                JOIN finesboard.public.userlogin t ON f.targetId = t.id
                JOIN finesboard.public.userlogin c ON f.creatorId = c.id
                JOIN finesboard.public.finesboard fb ON f.finesboardId = fb.id
                JOIN finesboard.public.finesboardStatus ft ON fb.finesboardStatusId = ft.id
                JOIN finesboard.public.offence o ON f.offenceId = o.id
                JOIN finesboard.public.forfeit ff ON f.forfeitId = ff.id
            WHERE
                f.finesboardId = @FinesboardId";
        }

        public Func<FineDetailDataModel, UserLoginDataModel, UserLoginDataModel, FinesboardDetailDataModel, FinesboardStatusDataModel, OffenceDataModel, ForfeitDataModel, FineDetailDataModel> Map()
        {
            return (FineDetailDataModel fine, UserLoginDataModel target, UserLoginDataModel creator, FinesboardDetailDataModel finesboard, FinesboardStatusDataModel finesboardStatus, OffenceDataModel offence, ForfeitDataModel forfeit) =>
            {
                fine.Target = target;
                fine.Creator = creator;
                finesboard.Status = finesboardStatus;
                fine.Finesboard = finesboard;
                fine.Offence = offence;
                fine.Forfeit = forfeit;
                return fine;
            };
        }
        
        public string SplitOn()
        {
            return "id";
        }
    }
}