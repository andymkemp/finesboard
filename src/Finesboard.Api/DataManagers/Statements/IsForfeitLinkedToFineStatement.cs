using System;

namespace Finesboard.Api.DataManagers.Statements
{
    public class IsForfeitLinkedToFineStatement : IStatement<bool>
    {
        public Guid ForfeitId { get; set; }

        public IsForfeitLinkedToFineStatement(Guid forfeitId)
        {
            ForfeitId = forfeitId;
        }

        public string Statement()
        {
            return @"SELECT
                CASE WHEN COUNT(*) > 0 then CAST(1 as bit) else CAST(0 as bit) end
            FROM 
                finesboard.public.fine
            WHERE
                forfeitId = @ForfeitId
            LIMIT 1";
        }
    }
}