using System;

namespace Finesboard.Api.DataManagers.Statements
{
    public class IsOffenceLinkedToFineStatement : IStatement<bool>
    {
        public Guid OffenceId { get; set; }

        public IsOffenceLinkedToFineStatement(Guid offenceId)
        {
            OffenceId = offenceId;
        }

        public string Statement()
        {
            return @"SELECT
                CASE WHEN COUNT(*) > 0 then CAST(1 as bit) else CAST(0 as bit) end
            FROM 
                finesboard.public.fine
            WHERE
                offenceId = @OffenceId
            LIMIT 1";
        }
    }
}