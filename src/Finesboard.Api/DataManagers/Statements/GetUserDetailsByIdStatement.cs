using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetUserDetailsByIdStatement: IStatement<UserDetailDataModel,IList<FinesboardDetailDataModel>,UserDetailDataModel>
    {
        public Guid Id { get; set; }
        
        public GetUserDetailsByIdStatement(Guid id)
        {
            Id = id;
        }

        public string Statement()
        {
            return @"SELECT
				ul.*,
				(SELECT 
	            	array_to_json(array_agg(row_to_json(finesboards)))
	            FROM (
					SELECT 
	                    fb.id,
	                    fb.name,
	                    row_to_json(fs) AS status,
	                    fb.currency,
	                    (SELECT 
	                        array_to_json(array_agg(row_to_json(offences)))
	                    FROM (
	                        SELECT 
	                            id,
	                            name,
	                            finesboardId,
	                            (SELECT 
									array_to_json(array_agg(row_to_json(ff)))
								FROM (select ff.*, oft.status
									FROM 
										finesboard.public.offenceForfeit oft
										JOIN finesboard.public.forfeit ff ON oft.forfeitId = ff.id
									WHERE 
										o.id = oft.offenceId
									) as ff
								) AS forfeits
	                        FROM 
	                            finesboard.public.offence o
	                        WHERE fb.id = o.finesboardId
	                    ) AS offences) AS offences,
	                    (SELECT 
	                        array_to_json(array_agg(row_to_json(ff)))
	                    FROM 
	                        finesboard.public.forfeit ff
	                    WHERE 
	                        fb.id = ff.finesboardId
	                    ) AS forfeits
	                FROM 
	                	finesboard.public.finesboarduser fu
	                    JOIN finesboard.public.finesboard fb ON fb.id = fu.finesboardid
	                    JOIN finesboard.public.finesboardStatus fs ON fb.finesboardStatusId = fs.id
	                WHERE 
	                	fu.userLoginId = ul.id
					) AS finesboards) AS finesboards
				FROM
					finesboard.public.userLogin ul
				WHERE
					ul.id = @Id;";
        }

        public Func<UserDetailDataModel,IList<FinesboardDetailDataModel>,UserDetailDataModel> Map()
        {
            return (UserDetailDataModel user, IList<FinesboardDetailDataModel> finesboards) =>
            {
                user.Finesboards = finesboards;
                return user;
            };
        }
        
        public string SplitOn()
        {
            return "id,finesboards";
        }
    }
}