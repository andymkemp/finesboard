using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers.Statements
{
    public class GetFinesboardOffenceForfeitStatement : IStatement<OffenceForfeitDataModel>
    {
        public Guid FinesboardId { get; set; }
        public Guid OffenceId { get; set; }
        public Guid ForfeitId { get; set; }
        public GetFinesboardOffenceForfeitStatement(Guid finesboardId, Guid offenceId, Guid forfeitId)
        {
            FinesboardId = finesboardId;
            OffenceId = offenceId;
            ForfeitId = forfeitId;
        }
        public string Statement()
        {
            return @"SELECT
                    ff.*                    
                FROM 
                    finesboard.public.finesboard fb
                    JOIN finesboard.public.offence o ON fb.id = o.finesboardId
                    JOIN finesboard.public.offenceForfeit ff ON o.id = ff.offenceId
                WHERE
                	fb.id = @FinesboardId
                	AND o.id = @OffenceId
                	AND ff.forfeitId = @ForfeitId";
        }
    }
}