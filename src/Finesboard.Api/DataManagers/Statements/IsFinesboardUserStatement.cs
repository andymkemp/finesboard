using System;

namespace Finesboard.Api.DataManagers.Statements
{
    public class IsFinesboardUserStatement : IStatement<bool>
    {
        public Guid FinesboardId { get; set; }
        public string Username { get; set; }

        public IsFinesboardUserStatement(Guid finesboardId, string username)
        {
            FinesboardId = finesboardId;
            Username = username;
        }

        public string Statement()
        {
            return @"SELECT
                CAST(1 as bit)
            FROM 
                finesboard.public.userLogin ul
                JOIN finesboard.public.finesboardUser fu on ul.id = fu.userLoginId
                JOIN finesboard.public.finesboard f on fu.finesboardId = f.id
            WHERE
                f.id = @FinesboardId
                and ul.username = @Username";
        }
    }
}