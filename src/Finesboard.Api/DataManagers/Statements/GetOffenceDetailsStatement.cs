using System;
using System.Collections.Generic;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public class GetOffenceDetailsStatement : IStatement<OffenceDetailDataModel, IList<LinkedForfeitDataModel>, OffenceDetailDataModel>
    {
        public Guid Id { get; set; }

        public GetOffenceDetailsStatement(Guid id)
        {
            Id = id;
        }

        public Func<OffenceDetailDataModel, IList<LinkedForfeitDataModel>, OffenceDetailDataModel> Map()
        {
            return (OffenceDetailDataModel offence, IList<LinkedForfeitDataModel> forfiets) => {
                offence.Forfeits = forfiets;
                return offence;
            };
        }

        public string SplitOn()
        {
            return "id,forfeits";
        }

        public string Statement()
        {
            return @"SELECT 
                id,
                name,
                finesboardId,
                (SELECT 
                    array_to_json(array_agg(row_to_json(ff)))
                FROM (select ff.*, oft.status
                    FROM 
                        finesboard.public.offenceForfeit oft
                        JOIN finesboard.public.forfeit ff ON oft.forfeitId = ff.id
                    WHERE 
                        o.id = oft.offenceId
                    ) as ff
                ) AS forfeits
            FROM 
                finesboard.public.offence o
            WHERE o.id = @Id";
        }
    }
}