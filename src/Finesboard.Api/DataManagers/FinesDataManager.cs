using System;
using System.Collections.Generic;
using System.Linq;
using Finesboard.Api.DataManagers.Statements;
using Finesboard.Api.Models;

namespace Finesboard.Api.DataManagers
{
    public class FinesDataManager: IFinesDataManager
    {        
        private readonly IDatabaseConnector _db;
        public FinesDataManager(IDatabaseConnector databaseConnector)
        {
            _db = databaseConnector;
        }

        public FineDetailDataModel GetFine(Guid fineId)
        {            
            using(var conn = _db.Connect())
            {
                var statement = new GetFineStatement(fineId);
                var fines  = conn.Query(statement);
                return fines.FirstOrDefault();
            }
        }

        public FineDetailDataModel GetFine(Guid fineId, string username)
        {            
            using(var conn = _db.Connect())
            {
                var statement = new GetUserFineStatement(fineId, username);
                var fines  = conn.Query(statement);
                return fines.FirstOrDefault();
            }
        }

        public IEnumerable<FineDetailDataModel> GetFinesList()
        {            
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesListStatement();
                var fines  = conn.Query(statement);
                return fines;
            }
        }

        public IEnumerable<FineDetailDataModel> GetFinesList(string username)
        {            
            using(var conn = _db.Connect())
            {
                var statement = new GetUserFinesListStatement(username);
                var fines  = conn.Query(statement);
                return fines;
            }
        }

        public FineDataModel AddFine(FineDataModel fine)
        { 
            using(var conn = _db.Connect())
            {
                fine.Id = Guid.NewGuid();
                conn.Insert(fine);         
                return fine;
            }
        }

        public int UpdateFine(FineDataModel fine)
        { 
            using(var conn = _db.Connect())
            {
                return conn.Update(fine);
            }
        }

        public int DeleteFine(Guid fineId)
        { 
            using(var conn = _db.Connect())
            {
                return conn.Delete<FineDataModel>(fineId);
            }
        }

        public IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetFinesForFinesboardStatement(finesboardId);
                return conn.Query(statement);
            }
        }

        public IEnumerable<FineDetailDataModel> GetFinesboardFines(Guid finesboardId, string username)
        {
            using(var conn = _db.Connect())
            {
                var statement = new GetUserFinesForFinesboardStatement(finesboardId, username);
                return conn.Query(statement);
            }
        }
    }    
}