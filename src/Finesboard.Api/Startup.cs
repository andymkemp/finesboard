﻿
using System;
using System.IO;
using System.Reflection;
using Finesboard.Api.DataManagers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Finesboard.Api.Middleware;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Finesboard.Api.Services;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.IdentityModel.Logging;
using Microsoft.AspNetCore.Authorization;
using Finesboard.Api.Authorization;
using Microsoft.AspNetCore.Identity;
using Finesboard.Api.Constants;

namespace Finesboard.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            services.AddTransient<IDatabaseConnectionManager, DapperConnectionManager>();
            services.AddTransient<IDatabaseConnector, NpgsqlConnector>();
            services.AddTransient<IDapperWrapper, DapperWrapper>();
            services.AddTransient<IStatementBuilder, StatementBuilder>();
            services.AddTransient<IFinesboardDataManager, FinesboardDataManager>();
            services.AddTransient<IFinesDataManager, FinesDataManager>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserDataManager, UserDataManager>();
            services.AddSingleton<IAuthorizationHandler, FinesboardUserOrRoleHandler>();
            services.AddSingleton<IAuthorizationHandler, FinesboardUserHandler>();
            services.AddSingleton<IPasswordHasher<object>, PasswordHasher<object>>();
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSwaggerGen(x => {
                x.SwaggerDoc("v1", new OpenApiInfo {Title = "Finesboard", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                x.IncludeXmlComments(xmlPath);
                x.AddSecurityDefinition("bearer", new OpenApiSecurityScheme {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "bearer"
                });
                x.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "bearer" }
                        },
                        new string[0]
                    }
                });
            });
            services.AddAutoMapper(typeof(Startup));
            services.AddLogging();
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SuperSecretPassword")),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            
            services.AddAuthorization(options => 
            {
                options.AddPolicy("Admin", 
                    policy => {
                        policy.RequireRole(Roles.Admin);
                    }
                );
                options.AddPolicy("FinesboardUserOrAdmin",
                    policy => {
                        policy.Requirements.Add(new FinesboardUserOrRoleRequirement(Roles.Admin));
                    }
                );
                options.AddPolicy("FinesboardUser",
                    policy => {
                        policy.Requirements.Add(new FinesboardUserOrRoleRequirement());
                    }
                );
                options.AddPolicy("BlockAll",
                    policy => {
                        policy.RequireAssertion(x => false);
                    }
                );
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // app.UseHsts();
            }
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            // app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(x => {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Finesboard v1");
            });
            app.UseRequestLogging();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => 
            {
                endpoints.MapControllers();
            });
        }
    }
}
