using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finesboard.Api
{
    public class StatementBuilder: IStatementBuilder
    {
        public string SelectById<T>() where T: IDbModel
        {
            var tableName = GetTableName<T>();
            return $"SELECT * FROM {tableName} WHERE Id = @Id";
        }

        public string SelectAll<T>() where T: IDbModel
        {
            var tableName = GetTableName<T>();
            return $"SELECT * FROM {tableName}";
        }

        public string SelectWhere<T>(object constraints) where T: IDbModel
        {
            var stringBuilder = new StringBuilder();
            var tableName = GetTableName<T>();
            stringBuilder.Append($"SELECT * FROM {tableName} WHERE ");
            var columns = new List<string>();
            var properties = constraints.GetType().GetProperties();
            foreach(var property in properties)
            {
                var columnName = property.Name;
                columns.Add($"{columnName} = @{columnName}");
            }
            stringBuilder.AppendJoin(" and ", columns);
            return stringBuilder.ToString();
        }

        public string Insert<T>() where T: IDbModel
        { 
            var type = typeof(T);
            var tableName = GetTableName<T>();
            var properties = type.GetProperties().Select(property => property.Name).ToArray();
            return Insert<T>(tableName, properties);
        }

        public string Insert<T>(params string[] ignore) where T: IDbModel
        { 
            var type = typeof(T);
            var tableName = GetTableName<T>();
            var properties = type.GetProperties()
                .Where(property => !ignore.Contains(property.Name))
                .Select(property => property.Name)
                .ToArray();
            return Insert<T>(tableName, properties);
        }

        private string Insert<T>(string tableName, string[] properties) where T: IDbModel
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append($"INSERT INTO {tableName} (");
            var columns = new List<string>();
            foreach(var property in properties)
            {
                var columnName = property;
                columns.Add(columnName);
            }
            stringBuilder.AppendJoin(",", columns);
            stringBuilder.Append(") VALUES (");
            columns = columns.ConvertAll((input) => {
                return $"@{input}";
            });
            stringBuilder.AppendJoin(",", columns);
            stringBuilder.Append(")");
            return stringBuilder.ToString();
        }

        public string UpdateById<T>(params string[] ignore) where T: IDbModel
        {
            return UpdateWhere<T>(new string[] { "Id" }, ignore);
        }
        
        public string UpdateWhere<T>(object constraints, params string[] ignore) where T: IDbModel
        {
            var properties = constraints.GetType().GetProperties().Select(property => property.Name).ToArray();
            return UpdateWhere<T>(properties, ignore);
        }

        private string UpdateWhere<T>(string[] constraints, params string[] ignore) where T: IDbModel
        {
            var stringBuilder = new StringBuilder();
            var type = typeof(T);
            var modelProperties = type.GetProperties().Select(property => property.Name);
            var tableName = GetTableName<T>();
            stringBuilder.Append($"UPDATE {tableName} SET ");
            var setColumns = new List<string>();
            var setProperties = modelProperties.Where(property => {
                return property != "Id" && !ignore.Contains(property);
            });
            foreach(var setProperty in setProperties)
            {
                setColumns.Add($"{setProperty} = @{setProperty}");
            }
            stringBuilder.AppendJoin(",", setColumns);
            if(constraints.Count() == 0)
            {
                throw new ArgumentException("Invalid constraints argument: array cannot be empty");
            }
            var whereContraints = new List<string>();
            stringBuilder.Append(" WHERE ");
            foreach(var constraint in constraints)
            {
                if(!modelProperties.Contains(constraint))
                {
                    throw new ArgumentException($"Invalid constraints argument: {constraint} did not match any properties of the {type.Name} model");
                }
                whereContraints.Add($"{constraint} = @{constraint}");
            }
            stringBuilder.AppendJoin(" and ", whereContraints);
            return stringBuilder.ToString();
        }

        public string DeleteById<T>() where T: IDbModel
        {
            return DeleteWhere<T>("Id");
        } 
        
        public string DeleteWhere<T>(object constraints) where T: IDbModel
        {
            var properties = constraints.GetType().GetProperties().Select(property => property.Name).ToArray();
            return DeleteWhere<T>(properties);
        }

        private string DeleteWhere<T>(params string[] constraints) where T: IDbModel
        {
            if(constraints == null || constraints.Count() == 0)
            {
                throw new ArgumentException("Invalid constraints argument: array cannot be empty");
            }
            var stringBuilder = new StringBuilder();
            var type = typeof(T);
            var modelProperties = type.GetProperties().Select(property => property.Name);
            var tableName = GetTableName<T>();
            stringBuilder.Append($"DELETE FROM {tableName}");
            var whereContraints = new List<string>();
            stringBuilder.Append(" WHERE ");
            foreach(var constraint in constraints)
            {
                if(!modelProperties.Contains(constraint))
                {
                    throw new ArgumentException($"Invalid constraints argument: {constraint} did not match any properties of the {type.Name} model");
                }
                whereContraints.Add($"{constraint} = @{constraint}");
            }
            stringBuilder.AppendJoin(" and ", whereContraints);
            return stringBuilder.ToString();
        }

        private string GetTableName<T>() where T: IDbModel
        {
            var type = typeof(T);
            var modelProperties = type.GetProperties().Select(property => property.Name);
            return type.Name.Replace("DataModel","");
        }
    }
}