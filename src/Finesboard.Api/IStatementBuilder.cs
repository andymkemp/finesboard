namespace Finesboard.Api
{
    public interface IStatementBuilder
    {
        string SelectById<T>() where T: IDbModel;
        string SelectAll<T>() where T: IDbModel;
        string SelectWhere<T>(object constraints) where T: IDbModel;
        string Insert<T>() where T: IDbModel;
        string Insert<T>(params string[] ignore) where T: IDbModel;
        string UpdateById<T>(params string[] ignore) where T: IDbModel;
        string UpdateWhere<T>(object constraints, params string[] ignore) where T: IDbModel;
        string DeleteById<T>() where T: IDbModel;
        string DeleteWhere<T>(object constraints) where T: IDbModel;
    }
}