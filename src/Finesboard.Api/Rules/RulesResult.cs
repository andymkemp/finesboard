using System.Collections.Generic;

namespace Finesboard.Api.Rules
{
    class RulesResult : IRulesResult
    {
        public RulesResult(bool failed, IList<string> messages = null)
        {            
            Failed = failed;
            Messages = messages;
        }
        
        public bool Failed { get; private set; }
        public IList<string> Messages { get; private set; }
    }
}