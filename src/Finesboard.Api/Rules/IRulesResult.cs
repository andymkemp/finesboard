using System.Collections.Generic;

namespace Finesboard.Api.Rules
{
    public interface IRulesResult
    {
        bool Failed { get; }
        IList<string> Messages { get; }
    }
}