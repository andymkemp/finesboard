namespace Finesboard.Api.Rules
{
    public interface IRulesHandler
    {
        void Register(IRuleStatement rule);
        IRulesResult HandleRules();
    }
}