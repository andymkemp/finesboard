namespace Finesboard.Api.Rules
{
    public interface IRuleStatement
    {
        void Execute(IRulesHandlerContext context);
    }
}