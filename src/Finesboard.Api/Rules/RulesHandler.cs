using System.Collections.Generic;
using System.Linq;

namespace Finesboard.Api.Rules
{
    public class RulesHandler : IRulesHandler
    {
        public RulesHandler()
        {
            Rules = new List<IRuleStatement>();
        }

        private IList<IRuleStatement> Rules { get; }

        public void Register(IRuleStatement rule)
        {
            Rules.Add(rule);
        }

        public IRulesResult HandleRules()
        {
            var context = new RulesHandlerContext();
            if(Rules.FirstOrDefault() == null)            
            {
                return context.Result();
            }
            foreach(var rule in Rules)
            {
                rule.Execute(context);
            }
            return context.Result();
        }
    }
}