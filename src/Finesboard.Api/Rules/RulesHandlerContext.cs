using System.Collections.Generic;

namespace Finesboard.Api.Rules
{
    class RulesHandlerContext: IRulesHandlerContext
    {
        private bool Failed { get; set; }
        private IList<string> Messages { get; set; }
        public RulesHandlerContext()
        {
            Failed = false;
            Messages = new List<string>();
        }

        public void Fail(string message)
        {
            Failed = true;
            Messages.Add(message);
        }
        
        public IRulesResult Result()
        {
            if(Failed)
            {
                return new RulesResult(Failed, Messages);
            }
            else
            {
                return new RulesResult(Failed);
            }
        }
    }
}