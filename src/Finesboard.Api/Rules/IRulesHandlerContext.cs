namespace Finesboard.Api.Rules
{
    public interface IRulesHandlerContext
    {
        void Fail(string message);
    }
}