using System;
using AutoMapper;

namespace Finesboard.Api
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            // fine models
            CreateMap<Models.FineDataModel, ApiModels.FineDetailModel>();
            CreateMap<Models.FineDataModel, ApiModels.FineDetailModel>();
            CreateMap<Models.FineDetailDataModel, ApiModels.FineDetailModel>();
            CreateMap<ApiModels.AddFineModel, Models.FineDataModel>();
            CreateMap<ApiModels.AddFinesboardFineModel, Models.FineDataModel>();
            CreateMap<ApiModels.UpdateFineModel, Models.FineDataModel>();

            // finesboard models
            CreateMap<ApiModels.AddFinesboardModel, Models.FinesboardDataModel>();
            CreateMap<Models.FinesboardDetailDataModel, ApiModels.FinesboardDetailModel>();
            CreateMap<Models.FinesboardDataModel, ApiModels.FinesboardExtendedDetailModel>();
            CreateMap<Models.FinesboardDetailDataModel, ApiModels.FinesboardExtendedDetailModel>();

            // finesboard status model
            CreateMap<Models.FinesboardStatusDataModel, ApiModels.FinesboardStatusModel>();

            // offence models
            CreateMap<Models.OffenceDataModel, ApiModels.OffenceModel>();
            CreateMap<Models.OffenceDataModel, ApiModels.OffenceDetailModel>();
            CreateMap<Models.OffenceDataModel, ApiModels.OffenceDetailModel>();
            CreateMap<Models.OffenceDetailDataModel, ApiModels.OffenceDetailModel>();
            CreateMap<ApiModels.AddOffenceModel, Models.OffenceDataModel>();
            CreateMap<ApiModels.OffenceModel, Models.OffenceDataModel>();

            // forfeit models
            CreateMap<Models.ForfeitDataModel, ApiModels.ForfeitModel>();
            CreateMap<Models.ForfeitDataModel, ApiModels.ForfeitDetailModel>();
            CreateMap<Models.LinkedForfeitDataModel, ApiModels.ForfeitModel>();
            CreateMap<Models.LinkedForfeitDataModel, ApiModels.ForfeitDetailModel>();
            CreateMap<Models.LinkedForfeitDataModel, ApiModels.OffenceForfeitModel>();
            CreateMap<ApiModels.AddForfeitModel, Models.ForfeitDataModel>();
            CreateMap<ApiModels.AddForfeitModel, Models.LinkedForfeitDataModel>();
            CreateMap<ApiModels.ForfeitModel, Models.ForfeitDataModel>();
            CreateMap<ApiModels.ForfeitModel, Models.LinkedForfeitDataModel>();
            CreateMap<ApiModels.OffenceForfeitModel, Models.LinkedForfeitDataModel>();

            // user models
            CreateMap<Models.UserLoginDataModel, ApiModels.AuthenticatedModel>();
            CreateMap<Models.UserLoginDataModel, ApiModels.UserRegisteredModel>();
            CreateMap<Models.UserLoginDataModel, ApiModels.UserDetailModel>();
            CreateMap<Models.UserLoginDataModel, ApiModels.UserModel>();
            CreateMap<Models.UserDetailDataModel, ApiModels.UserDetailModel>();
            CreateMap<ApiModels.RegisterUserModel, Models.UserLoginDataModel>();
        }
    }
}