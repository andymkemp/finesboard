using System;
using System.Collections.Generic;
using System.Data;

namespace Finesboard.Api
{
    public interface IDatabaseConnectionManager: IDisposable
    {
        T SelectById<T>(Guid id) where T: IDbModel;
        IEnumerable<T> SelectList<T>() where T: IDbModel;
        IEnumerable<T> SelectList<T>(object where) where T: IDbModel;
        int Insert<T>(T data, IDbTransaction tran = null) where T: IDbModel;
        int Insert<T>(T data, string[] ignore, IDbTransaction tran = null) where T: IDbModel;
        int Insert<T>(IEnumerable<T> data) where T: IDbModel;
        int Update<T>(T data, params string[] ignore) where T: IDbModel;
        int Update<T>(T data, object where, params string[] ignore) where T: IDbModel;
        int Update<T>(IEnumerable<T> data) where T: IDbModel;
        int Delete<T>(Guid id) where T: IDbModel;
        int Delete<T>(object where) where T: IDbModel;
        IEnumerable<T1> Query<T1>(IStatement<T1> statement);
        IEnumerable<U> Query<T1,T2,U>(IStatement<T1,T2,U> statement);
        IEnumerable<U> Query<T1,T2,T3,U>(IStatement<T1,T2,T3,U> statement);
        IEnumerable<U> Query<T1,T2,T3,T4,U>(IStatement<T1,T2,T3,T4,U> statement);
        IEnumerable<U> Query<T1,T2,T3,T4,T5,U>(IStatement<T1,T2,T3,T4,T5,U> statement);
        IEnumerable<U> Query<T1,T2,T3,T4,T5,T6,U>(IStatement<T1,T2,T3,T4,T5,T6,U> statement);
        IEnumerable<U> Query<T1,T2,T3,T4,T5,T6,T7,U>(IStatement<T1,T2,T3,T4,T5,T6,T7,U> statement);
        IDbTransaction BeginTransaction();
        void Open();
        void Connect(IDbConnection dbConnection);
    }
}