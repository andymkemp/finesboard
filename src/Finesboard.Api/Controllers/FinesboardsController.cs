using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Finesboard.Api.Authorization;
using Finesboard.Api.Constants;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;
using Finesboard.Api.RuleStatements;
using Finesboard.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Finesboard.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]"), ]
    [ApiController]
    public class FinesboardsController : ControllerBase
    {
        private readonly IAuthorizationService _authService;
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly IFinesDataManager _finesDataManager;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private Guid UserId 
        { 
            get { return Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value); }
        }


        public FinesboardsController(IAuthorizationService authService, IFinesboardDataManager finesboardDataManager, IFinesDataManager finesDataManager, IUserService userService, IMapper mapper)
        {
            _authService = authService;
            _finesboardDataManager = finesboardDataManager;
            _finesDataManager = finesDataManager;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Finesboards.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult GetFinesboards([FromQuery] string status)
        {
            IEnumerable<Models.FinesboardDetailDataModel> result;
            if(string.IsNullOrEmpty(status))
            {
                result = _finesboardDataManager.GetFinesboardList();
            }
            else
            {
                result = _finesboardDataManager.GetFinesboardsByStatus(status);
            }
            var finesboards = _mapper.Map<IEnumerable<ApiModels.FinesboardExtendedDetailModel>>(result);
            return Ok(finesboards);
        }
        

        /// <summary>
        /// Get a specific Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}"), ActionName("GetFinesboard")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult GetFinesboard([FromRoute] Guid finesboardId)
        {
            var result = _finesboardDataManager.GetFinesboard(finesboardId);
            if(result == null)
            {
                return NotFound();
            }
            var finesboard = _mapper.Map<ApiModels.FinesboardExtendedDetailModel>(result);
            return Ok(finesboard);
        }
        
        /// <summary>
        /// Create a new Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards
        ///     {
        ///        "name": "Traffic Fines",
        ///     }
        ///
        /// </remarks>  
        [Authorize("Admin")]       
        [HttpPost]
        [ProducesResponseType(typeof(ApiModels.FinesboardExtendedDetailModel), 201)]
        [ProducesResponseType(400)]
        public IActionResult AddFinesboard([FromBody] ApiModels.AddFinesboardModel finesboard)
        {
            var input = _mapper.Map<Models.FinesboardDataModel>(finesboard);
            input.FinesboardStatusId = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da");
            input =_finesboardDataManager.AddFinesboard(input);
            var output = _mapper.Map<ApiModels.FinesboardExtendedDetailModel>(input);
            return CreatedAtAction("GetFinesboard", new { finesboardId = output.Id }, output);
        }

        /// <summary>
        /// Update the details of a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB
        ///     {
        ///         "name": "Speeding Fines"
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPut("{finesboardId}")]
        [ProducesResponseType(typeof(ApiModels.FinesboardExtendedDetailModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult UpdateFinesboard([FromRoute] Guid finesboardId, [FromBody] ApiModels.AddFinesboardModel finesboard)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardIsPending(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.FinesboardDataModel>(finesboard);
            input.Id = finesboardId;
            var updated = _finesboardDataManager.UpdateFinesboard(input);
            if(updated == 0)
            {
                return NotFound();
            }
            return Ok(finesboard);
        }

        /// <summary>
        /// Delete a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpDelete("{finesboardId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult DeleteFinesboard([FromRoute] Guid finesboardId)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardIsPending(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.DeleteFinesboard(finesboardId);
            return Ok();
        }

        /// <summary>
        /// Get all Offences on a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/offences")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.OffenceModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult GetOffences([FromRoute] Guid finesboardId)
        {
            var fines = _finesboardDataManager.GetOffences(finesboardId);
            var output = _mapper.Map<IEnumerable<ApiModels.OffenceModel>>(fines);
            return Ok(output);
        }

        /// <summary>
        /// Get a specific Offence on a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/offences/{offenceId}"), ActionName("GetFinesboardOffence")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetOffence([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId)
        {
            var offence = _finesboardDataManager.GetOffenceDetails(offenceId);
            if(offence == null)
            {
                return NotFound();
            }
            var output = _mapper.Map<ApiModels.OffenceDetailModel>(offence);
            return Ok(output);
        }

        /// <summary>
        /// Add an Offence to a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences
        ///     {
        ///         "name": "Any",
        ///         "finesboardId": "00AF0F21-7224-49AB-B83B-AA2800A12BDB"
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/offences")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult AddOffence([FromRoute] Guid finesboardId, [FromBody] ApiModels.AddOffenceModel offence)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.OffenceDataModel>(offence);
            input.FinesboardId = finesboardId;
            input = _finesboardDataManager.AddOffence(input);
            foreach(var forfeit in offence.Forfeits)
            {
                _finesboardDataManager.AddOffenceForfeit((Guid)input.Id, forfeit.Id);
            }
            var offenceDetails = _finesboardDataManager.GetOffenceDetails((Guid)input.Id);
            var output = _mapper.Map<ApiModels.OffenceDetailModel>(offenceDetails);
            return CreatedAtAction("GetFinesboardOffence", new { finesboardId = finesboardId.ToString(), offenceId = output.Id.ToString() }, output);
        }

        /// <summary>
        /// Delete an Offence from a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///         
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpDelete("{finesboardId}/offences/{offenceId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult DeleteOffence([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, finesboardId, offenceId));
            rulesHandler.Register(new OffenceIsNotLinkedToFine(_finesboardDataManager, offenceId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.DeleteOffence(offenceId);
            return Ok();
        }

        /// <summary>
        /// Get all Forfeits on a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/forfeits
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/forfeits")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.ForfeitDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult GetForfeits([FromRoute] Guid finesboardId)
        {
            var forfeits = _finesboardDataManager.GetForfeits(finesboardId);
            var output = _mapper.Map<IEnumerable<ApiModels.ForfeitDetailModel>>(forfeits);
            return Ok(output);
        }

        /// <summary>
        /// Get a specific Forfeit on a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/forfeits/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/forfeits/{forfeitId}"), ActionName("GetFinesboardForfeit")]
        [ProducesResponseType(typeof(ApiModels.ForfeitDetailModel), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult GetForfeit([FromRoute] Guid finesboardId, [FromRoute] Guid forfeitId)
        {
            var forfeit = _finesboardDataManager.GetForfeit(forfeitId);
            if(forfeit == null)
            {
                return NotFound();
            }
            var output = _mapper.Map<ApiModels.ForfeitDetailModel>(forfeit);
            return Ok(output);
        }

        /// <summary>
        /// Add a Forfeit to a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/forfeits
        ///     {
        ///         "name": "1 Cake Tick",
        ///         "amount": 1.00,
        ///         "finesboardId": "00AF0F21-7224-49AB-B83B-AA2800A12BDB",
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/forfeits")]
        [ProducesResponseType(typeof(ApiModels.ForfeitDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult AddForfeit([FromRoute] Guid finesboardId, [FromBody] ApiModels.AddForfeitModel forfeit)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.ForfeitDataModel>(forfeit);
            input.FinesboardId = finesboardId;
            input = _finesboardDataManager.AddForfeit(input);
            var output = _mapper.Map<ApiModels.ForfeitDetailModel>(input);
            return CreatedAtAction("GetFinesboardForfeit", new { finesboardId = finesboardId.ToString(), forfeitId = output.Id.ToString() }, output);
        }

        /// <summary>
        /// Update the details of a Finesboard Forfeit.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/forfeits/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///         "name": "R100",
        ///         "amount: "100",
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPut("{finesboardId}/forfeits/{forfeitId}")]
        [ProducesResponseType(typeof(ApiModels.ForfeitDetailModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult UpdateForfeit([FromRoute] Guid finesboardId, [FromRoute] Guid forfeitId, [FromBody] ApiModels.AddForfeitModel forfeit)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new ForfeitIsNotLinkedToFine(_finesboardDataManager, forfeitId));
            rulesHandler.Register(new ForfeitIsNotLinkedToMoreThanOneOffence(_finesboardDataManager, forfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.ForfeitDataModel>(forfeit);
            input.Id = forfeitId;
            input.FinesboardId = finesboardId;
            var updated = _finesboardDataManager.UpdateForfeit(input);
            if(updated == 0)
            {
                return NotFound();
            }
            var output =  _mapper.Map<ApiModels.ForfeitDetailModel>(input);
            return Ok(output);
        }

        /// <summary>
        /// Delete a Forfeit from a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/forfeits/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///         
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpDelete("{finesboardId}/forfeits/{forfeitId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult DeleteForfeit([FromRoute] Guid finesboardId, [FromRoute] Guid forfeitId)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new ForfeitIsNotLinkedToFine(_finesboardDataManager, forfeitId));
            rulesHandler.Register(new ForfeitIsNotTheOnlyForfietLinkedToOffence(_finesboardDataManager, forfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.DeleteForfeit(forfeitId);
            return Ok();
        }

        /// <summary>
        /// Link a Forfeit to an Offence.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/0da55b8f-d8b4-46cf-b018-9c96f7ee56a1/forfeits/dd4e93a0-d5c8-416b-afc8-215fd96a8040
        ///     {
        ///    
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/offences/{offenceId}/forfeits/{forfeitId}")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult LinkForfeitToOffence([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId, [FromRoute] Guid forfeitId)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, finesboardId, offenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new OffenceIsNotLinkedToTheForfeit(_finesboardDataManager, offenceId, forfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.AddOffenceForfeit(offenceId, forfeitId);
            var offence = _finesboardDataManager.GetOffenceDetails(offenceId);
            var output = _mapper.Map<ApiModels.OffenceDetailModel>(offence);
            return Ok(output);
        }

        /// <summary>
        /// Deactivate Offence Forfeit.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/0da55b8f-d8b4-46cf-b018-9c96f7ee56a1/forfeits/dd4e93a0-d5c8-416b-afc8-215fd96a8040/deactivate
        ///     {
        ///    
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/offences/{offenceId}/forfeits/{forfeitId}/deactivate")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult DeactivateOffenceForfeit([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId, [FromRoute] Guid forfeitId)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, finesboardId, offenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, offenceId, forfeitId));
            rulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, offenceId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.UpdateOffenceForfeitStatus(offenceId, forfeitId, Status.Inactive);
            var offence = _finesboardDataManager.GetOffenceDetails(offenceId);
            var output = _mapper.Map<ApiModels.OffenceDetailModel>(offence);
            return Ok(output);
        }

        /// <summary>
        /// Activate Offence Forfeit.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/0da55b8f-d8b4-46cf-b018-9c96f7ee56a1/forfeits/dd4e93a0-d5c8-416b-afc8-215fd96a8040/activate
        ///     {
        ///    
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/offences/{offenceId}/forfeits/{forfeitId}/activate")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult ActivateOffenceForfeit([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId, [FromRoute] Guid forfeitId)
        {            
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, finesboardId, offenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, offenceId, forfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.UpdateOffenceForfeitStatus(offenceId, forfeitId, Status.Active);
            var offence = _finesboardDataManager.GetOffenceDetails(offenceId);
            var output = _mapper.Map<ApiModels.OffenceDetailModel>(offence);
            return Ok(output);
        }

        /// <summary>
        /// Unlink a Forfeit from an Offence.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/offences/0da55b8f-d8b4-46cf-b018-9c96f7ee56a1/forfeits/dd4e93a0-d5c8-416b-afc8-215fd96a8040
        ///     {
        ///    
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpDelete("{finesboardId}/offences/{offenceId}/forfeits/{forfeitId}")]
        [ProducesResponseType(typeof(ApiModels.OffenceDetailModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult UnlinkForfeitFromOffence([FromRoute] Guid finesboardId, [FromRoute] Guid offenceId, [FromRoute] Guid forfeitId)
        {   
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, finesboardId, offenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, finesboardId, forfeitId));
            rulesHandler.Register(new OffenceForfeitIsNotLinkedToFine(_finesboardDataManager, offenceId, forfeitId));
            rulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, offenceId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.DeleteOffenceForfeit(offenceId, forfeitId);
            return Ok();
        }

        /// <summary>
        /// Get all Fines for a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/fines
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/fines")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FineDetailModel>), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetFines([FromRoute] Guid finesboardId)
        {
            return await AuthorizeFinesboardUser("FinesboardUserOrAdmin", finesboardId, () => {
                var fines = _finesboardDataManager.GetFinesboardFines(finesboardId);
                var output = _mapper.Map<IEnumerable<ApiModels.FineDetailModel>>(fines);
                return Ok(output);
            });
        }

        /// <summary>
        /// Get a specific Fine for a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/fines/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/fines/{fineId}"), ActionName("GetFinesboardFines")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FineDetailModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetFines([FromRoute] Guid finesboardId, [FromRoute] Guid fineId)
        {
            return await AuthorizeFinesboardUser("FinesboardUserOrAdmin", finesboardId, () => {
                var fine = _finesDataManager.GetFine(fineId);
                if(fine == null)
                {
                    return NotFound();
                }
                var output = _mapper.Map<ApiModels.FineDetailModel>(fine);
                return Ok(output);
            });
        }

        /// <summary>
        /// Add a Fine to a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/fines
        ///     {
        ///         "targetId": "501d1a52-dae3-41bc-b312-05175fd68f3a",
        ///         "reason": "100 km/h in a 60 km/h zone",
        ///         "amount": 1000,
        ///         "offenceId": "0da55b8f-d8b4-46cf-b018-9c96f7ee56a1",
        ///         "forfeitId": "dd4e93a0-d5c8-416b-afc8-215fd96a8040"
        ///     }
        ///
        /// </remarks>
        [HttpPost("{finesboardId}/fines")]
        [ProducesResponseType(typeof(ApiModels.FineDetailModel), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddFine([FromRoute] Guid finesboardId, [FromBody] ApiModels.AddFinesboardFineModel fine)
        {
            // todo: add rules to check offenceId and forfeitId
            return await AuthorizeFinesboardUser("FinesboardUserOrAdmin", finesboardId, () => {
                var rulesHandler = new RulesHandler();
                rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
                rulesHandler.Register(new OffenceForfeitIsActive(_finesboardDataManager, finesboardId, fine.OffenceId, fine.ForfeitId));
                var result = rulesHandler.HandleRules();
                if(result.Failed)
                {
                    return BadRequest(result.Messages);
                }
                var input = _mapper.Map<Models.FineDataModel>(fine);
                input.CreatorId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                input.FinesboardId = finesboardId;
                input = _finesDataManager.AddFine(input);
                var output = _finesDataManager.GetFine((Guid)input.Id);
                return CreatedAtAction("GetFinesboardFines", new { finesboardId, fineId = output.Id }, output);
            });
        }

        /// <summary>
        /// Get finesboard statuses.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/statuses
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("statuses")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FinesboardStatusModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult GetStatusList()
        {
            var result = _finesboardDataManager.GetStatusList();
            var finesboards = _mapper.Map<IEnumerable<ApiModels.FinesboardStatusModel>>(result);
            return Ok(finesboards);
        }

        /// <summary>
        /// Set Finesboard Status to Active.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/activate
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/activate")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult ActivateFinesboard([FromRoute] Guid finesboardId)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardIsPending(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new FinesboardHasAnActiveOffenceForfeit(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.ActivateFinesboard(finesboardId);
            var finesboard = _finesboardDataManager.GetFinesboard(finesboardId);
            return Ok(finesboard);
        }

        /// <summary>
        /// Set Finesboard Status to Closed.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/00AF0F21-7224-49AB-B83B-AA2800A12BDB/activate
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{finesboardId}/deactivate")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult DeactivateFinesboard([FromRoute] Guid finesboardId)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardIsActive(_finesboardDataManager, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.DeactivateFinesboard(finesboardId);
            var finesboard = _finesboardDataManager.GetFinesboard(finesboardId);
            return Ok(finesboard);
        }

        /// <summary>
        /// Get all Users for a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/users
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{finesboardId}/users")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.UserModel>), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetUsers([FromRoute] Guid finesboardId)
        {
            return await AuthorizeFinesboardUser("FinesboardUserOrAdmin", finesboardId, () => {
                var result = _finesboardDataManager.GetFinesboardUsers(finesboardId);
                if(result.Count() == 0)
                {
                    return NotFound();
                }
                var fines = _mapper.Map<IEnumerable<ApiModels.UserModel>>(result);
                return Ok(fines);
            });
        }

        /// <summary>
        /// Add user to Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/users/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpPost("{finesboardId}/users/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.UserDetailModel>), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> AddUserToFinesboard([FromRoute] Guid finesboardId, [FromRoute] Guid userId)
        {
            return await AuthorizeIsTargetUser(finesboardId, userId, () => {
                var rulesHandler = new RulesHandler();
                rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
                rulesHandler.Register(new UserIsNotAMemberOfTheFinesboard(_userService, UserId, finesboardId));
                var result = rulesHandler.HandleRules();
                if(result.Failed)
                {
                    return BadRequest(result.Messages);
                }
                _finesboardDataManager.JoinFinesboard(finesboardId, UserId);
                var userDetails = _userService.GetUserDetails(UserId);
                return Ok(userDetails);
            });
        }

        /// <summary>
        /// Remove user from Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/users/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpDelete("{finesboardId}/users/{userId}")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.UserDetailModel>), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> RemoveUserFromFinesboard([FromRoute] Guid finesboardId, [FromRoute] Guid userId )
        {
            return await AuthorizeIsTargetUser(finesboardId, userId, () => {
                var rulesHandler = new RulesHandler();
                rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
                rulesHandler.Register(new UserHasNotBeenFinedOnFinesboard(_userService, UserId, finesboardId));
                rulesHandler.Register(new UserHasNotRaisedFineOnFinesboard(_userService, UserId, finesboardId));
                var result = rulesHandler.HandleRules();
                if(result.Failed)
                {
                    return BadRequest(result.Messages);
                }
                _finesboardDataManager.LeaveFinesboard(finesboardId, UserId);
                var userDetails = _userService.GetUserDetails(UserId);
                return Ok(userDetails);
            });
        }

        private async Task<IActionResult> AuthorizeFinesboardUser(string policyName, Guid finesboardId, Func<IActionResult> result)
        {
            var authResult = await _authService.AuthorizeAsync(User, finesboardId, policyName);
            if(authResult.Succeeded)
            {
                return result();
            }
            else
            {
                return Forbid();
            }
        }

        private async Task<IActionResult> AuthorizeIsTargetUser(Guid finesboardId, Guid targetId, Func<IActionResult> result)
        {
            var isTargetUser = await _authService.AuthorizeAsync(User, targetId, new IsTargetUserRequirement());
            if(isTargetUser.Succeeded)
            {
                return result();
            }
            else
            {
                return Forbid();
            }
        }
    }
}