using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Finesboard.Api.ApiModels;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;
using Finesboard.Api.RuleStatements;
using Finesboard.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Finesboard.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IAuthorizationService _authService;
        private readonly IUserService _userService;
        private readonly IFinesboardDataManager _finesboardDataManager;
        private Guid UserId 
        { 
            get { return Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value); }
        }
        

        public UserController(IAuthorizationService authService, IUserService userService, IFinesboardDataManager finesboardDataManager)
        {
            _authService = authService;
            _userService = userService;
            _finesboardDataManager = finesboardDataManager;
        }

        /// <summary>
        /// User details.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/user
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("")]
        [ProducesResponseType(typeof(UserDetailModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult GetUser()
        {
            var userDetails = _userService.GetUserDetails(UserId);
            return Ok(userDetails);
        }

        /// <summary>
        /// Change user password.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/user
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpPost("changepassword")]
        [ProducesResponseType(typeof(UserDetailModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult ChangePassword([FromBody] ChangePasswordModel changePassword)
        {
            _userService.ChangePassword(UserId, changePassword.NewPassword);
            return Ok();
        }

        /// <summary>
        /// User Finesboards.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/finesboards
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("finesboards")]
        [ProducesResponseType(typeof(IEnumerable<FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult UserFinesboards([FromQuery] string status)
        {
            IEnumerable<ApiModels.FinesboardExtendedDetailModel> finesboards;
            if(string.IsNullOrEmpty(status))
            {
                finesboards = _userService.GetFinesboards(UserId);
            }
            else
            {
                finesboards = _userService.GetFinesboardsByStatus(UserId, status);
            }
            return Ok(finesboards);
        }

        /// <summary>
        /// Join a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/user/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/join
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpPost("finesboards/{finesboardId}/join")]
        [ProducesResponseType(typeof(IEnumerable<UserDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult JoinFinesboard([FromRoute] Guid finesboardId)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new UserIsNotAMemberOfTheFinesboard(_userService, UserId, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.JoinFinesboard(finesboardId, UserId);
            var userDetails = _userService.GetUserDetails(UserId);
            return Ok(userDetails);
        }

        /// <summary>
        /// Leave a Finesboard.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/user/finesboards/4ecf9c20-27fd-4d4d-84dd-a0df7ac11665/leave
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpPost("finesboards/{finesboardId}/leave")]
        [ProducesResponseType(typeof(IEnumerable<UserDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult LeaveFinesboard([FromRoute] Guid finesboardId)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardHasNotBeenDeactivated(_finesboardDataManager, finesboardId));
            rulesHandler.Register(new UserHasNotBeenFinedOnFinesboard(_userService, UserId, finesboardId));
            rulesHandler.Register(new UserHasNotRaisedFineOnFinesboard(_userService, UserId, finesboardId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesboardDataManager.LeaveFinesboard(finesboardId, UserId);
            var userDetails = _userService.GetUserDetails(UserId);
            return Ok(userDetails);
        }
    }
}