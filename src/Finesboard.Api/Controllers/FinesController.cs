using System;
using System.Collections.Generic;
using System.Security.Claims;
using AutoMapper;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;
using Finesboard.Api.RuleStatements;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Finesboard.Api.Controllers
{
    [Authorize("Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class FinesController : ControllerBase
    {
        private readonly IAuthorizationService _authService;
        private readonly IFinesDataManager _finesDataManager;
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly IMapper _mapper;

        public FinesController(IAuthorizationService authService, IFinesDataManager finesDataManager, IMapper mapper, IFinesboardDataManager finesboardDataManager)
        {
            _authService = authService;
            _finesDataManager = finesDataManager;
            _mapper = mapper;
            _finesboardDataManager = finesboardDataManager;
        }

        /// <summary>
        /// Get all Fines.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/fines
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FineDetailModel>), 200)]
        [ProducesResponseType(400)]
        public IActionResult Get([FromQuery] Guid? finesboardId)
        {
            IEnumerable<Models.FineDetailDataModel> fines;
            if(finesboardId == null)
            {
                fines = _finesDataManager.GetFinesList();
            }
            else
            {
                fines = _finesDataManager.GetFinesboardFines((Guid)finesboardId);
            }
            var output = _mapper.Map<IEnumerable<ApiModels.FineDetailModel>>(fines);
            return Ok(output);
        }

        /// <summary>
        /// Get a specific Fine.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/fines/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [HttpGet("{fineId}"), ActionName("GetFines")]
        [ProducesResponseType(typeof(IEnumerable<ApiModels.FineDetailModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult Get([FromRoute] Guid fineId)
        {
            var fine = _finesDataManager.GetFine(fineId);
            if(fine == null)
            {
                return NotFound();
            }
            var output = _mapper.Map<ApiModels.FineDetailModel>(fine);
            return Ok(output);
        }

        /// <summary>
        /// Add a Fine.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/fines
        ///     {
        ///         "targetId": "501d1a52-dae3-41bc-b312-05175fd68f3a",
        ///         "reason": "100 km/h in a 60 km/h zone",
        ///         "amount": 1000,
        ///         "finesboardId": "00AF0F21-7224-49AB-B83B-AA2800A12BDB",
        ///         "offenceId": "0da55b8f-d8b4-46cf-b018-9c96f7ee56a1",
        ///         "forfeitId": "dd4e93a0-d5c8-416b-afc8-215fd96a8040"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(typeof(ApiModels.FineDetailModel), 201)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody] ApiModels.AddFineModel fine)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, fine.FinesboardId, fine.OffenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, fine.FinesboardId, fine.OffenceId));
            rulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, fine.OffenceId, fine.ForfeitId));
            rulesHandler.Register(new OffenceForfeitIsActive(_finesboardDataManager, fine.FinesboardId, fine.OffenceId, fine.ForfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.FineDataModel>(fine);
            input.CreatorId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            input = _finesDataManager.AddFine(input);
            var output = _finesDataManager.GetFine((Guid)input.Id);
            return CreatedAtAction("GetFines", new { fineId = output.Id.ToString() }, output);
        }

        /// <summary>
        /// Update a Fine.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/fines/5b85e84e-6333-45de-aab6-aa2800a12bdb
        ///     {
        ///         "targetId": "501d1a52-dae3-41bc-b312-05175fd68f3a",
        ///         "creatorId": "d871f442-e6b8-456c-96b9-77b8f0ef8c5e",
        ///         "reason": "100 km/h in a 60 km/h zone",
        ///         "amount": 1000,
        ///         "finesboardId": "4ecf9c20-27fd-4d4d-84dd-a0df7ac11665",
        ///         "offenceId": "0da55b8f-d8b4-46cf-b018-9c96f7ee56a1",
        ///         "forfeitId": "dd4e93a0-d5c8-416b-afc8-215fd96a8040"
        ///     }
        ///
        /// </remarks>
        // [Authorize("BlockAll")]
        [HttpPut("{fineId}")]
        [ProducesResponseType(typeof(ApiModels.FineDetailModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public IActionResult Put([FromRoute] Guid fineId, [FromBody] ApiModels.UpdateFineModel fine)
        {
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, fine.FinesboardId, fine.OffenceId));
            rulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, fine.FinesboardId, fine.OffenceId));
            rulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, fine.OffenceId, fine.ForfeitId));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            var input = _mapper.Map<Models.FineDataModel>(fine);
            input.Id = fineId;
            var updated = _finesDataManager.UpdateFine(input);
            if(updated == 0)
            {
                return NotFound();
            }
            var output = _finesDataManager.GetFine((Guid)input.Id);
            return Ok(output);
        }

        /// <summary>
        /// Delete a Fine.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/fines/aa2eca98-42cd-4b3b-b6aa-220fa979d511
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        // [Authorize("BlockAll")]
        [HttpDelete("{fineId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult Delete([FromRoute] Guid fineId)
        {
            var fine = _finesDataManager.GetFine(fineId);
            var rulesHandler = new RulesHandler();
            rulesHandler.Register(new FinesboardIsActive(_finesboardDataManager, fine.Finesboard.Id));
            var result = rulesHandler.HandleRules();
            if(result.Failed)
            {
                return BadRequest(result.Messages);
            }
            _finesDataManager.DeleteFine(fineId);
            return Ok();
        }
    }
}