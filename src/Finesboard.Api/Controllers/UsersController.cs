using System;
using System.Collections.Generic;
using Finesboard.Api.ApiModels;
using Finesboard.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Finesboard.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        
        /// <summary>
        /// Authenticate User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/authenticate
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(typeof(AuthenticatedModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult Authenticate([FromBody] AuthenticateModel authenticate)
        {
            var token = _userService.Authenticate(authenticate.Username, authenticate.Password);
            if(token == null)
            {
                return Unauthorized();
            }
            return Ok(token);
        }
        
        /// <summary>
        /// Register User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/register
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(typeof(UserRegisteredModel), 200)]
        [ProducesResponseType(400)]
        public IActionResult Register([FromBody] RegisterUserModel user)
        {
            var newUser = _userService.Register(user);
            return Ok(newUser);
        }

        /// <summary>
        /// Reset user password.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Post /api/users/501d1a52-dae3-41bc-b312-05175fd68f3a/resetpassword
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpPost("{userId}/resetpassword")]
        [ProducesResponseType(typeof(UserDetailModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult ResetPassword([FromRoute] Guid userId, [FromBody] ChangePasswordModel changePassword)
        {
            _userService.ChangePassword(userId, changePassword.NewPassword);
            return Ok();
        }
        
        /// <summary>
        /// Users.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpGet]
        [ProducesResponseType(typeof(UserDetailModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult Users()
        {
            var users = _userService.GetUsers();
            return Ok(users);
        }

        /// <summary>
        /// User details.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/501d1a52-dae3-41bc-b312-05175fd68f3a
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpGet("{userId}")]
        [ProducesResponseType(typeof(UserDetailModel), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult GetUser([FromRoute] Guid userId)
        {
            var finesboards = _userService.GetUserDetails(userId);
            return Ok(finesboards);
        }

        /// <summary>
        /// User Finesboards.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/501d1a52-dae3-41bc-b312-05175fd68f3a/finesboards
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        [Authorize("Admin")]
        [HttpGet("{userId}/finesboards")]
        [ProducesResponseType(typeof(IEnumerable<FinesboardExtendedDetailModel>), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public IActionResult UserFinesboards([FromRoute] Guid userId,[FromQuery] string status)
        {
            IEnumerable<ApiModels.FinesboardExtendedDetailModel> finesboards;
            if(string.IsNullOrEmpty(status))
            {
                finesboards = _userService.GetFinesboards(userId);
            }
            else
            {
                finesboards = _userService.GetFinesboardsByStatus(userId, status);
            }
            return Ok(finesboards);
        }
    }
}