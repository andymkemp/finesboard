using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceForfeitIsActive : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        private readonly Guid _offenceId;
        private readonly Guid _forfeitId;
        public OffenceForfeitIsActive(IFinesboardDataManager finesboardDataManager, Guid finesboardId, Guid offenceId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
            _offenceId = offenceId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var forfeit = _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfeitId);
            if(forfeit == null || forfeit.Status != 1)
            {
                context.Fail($"Offence forfeit is not active");
            }
        }
    }
}