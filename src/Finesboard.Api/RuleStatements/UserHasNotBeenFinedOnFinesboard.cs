using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;
using Finesboard.Api.Services;

namespace Finesboard.Api.RuleStatements
{
    public class UserHasNotBeenFinedOnFinesboard : IRuleStatement
    {
        private readonly IUserService _userService;
        private readonly Guid _userId;
        private readonly Guid _finesboardId;
        public UserHasNotBeenFinedOnFinesboard(IUserService userService, Guid userId, Guid finesboardId)
        {
            _userService = userService;
            _userId = userId;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var fines = _userService.GetFinesboardFinesByTarget(_finesboardId, _userId);
            if(fines.Count() > 0)
            {
                context.Fail($"User has been fined on finesboard");
            }
        }
    }
}