using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceIsNotLinkedToFine : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        public OffenceIsNotLinkedToFine(IFinesboardDataManager finesboardDataManager, Guid offenceId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            if(_finesboardDataManager.IsOffenceLinkedToFine(_offenceId))
            {
                context.Fail($"Offence is linked to a fine");
            }
        }
    }
}