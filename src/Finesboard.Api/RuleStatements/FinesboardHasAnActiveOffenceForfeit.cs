using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class FinesboardHasAnActiveOffenceForfeit : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        public FinesboardHasAnActiveOffenceForfeit(IFinesboardDataManager finesboardDataManager, Guid finesboardId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var finesboard = _finesboardDataManager.GetFinesboard(_finesboardId);
            if(finesboard.Offences == null)
            {
                context.Fail($"Finesboard does not have an active offence forfeit");
                return;
            }
            bool failed = true;
            foreach(var offence in finesboard.Offences)
            {
                if(offence.Forfeits == null)
                {
                    continue;
                }
                foreach(var forfeit in offence.Forfeits)
                {
                    if(forfeit.Status == 1)
                    {
                        failed = false;
                    }
                }
            }
            if(failed)
            {
                context.Fail($"Finesboard does not have an active offence forfeit");
            }
        }
    }
}