using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;
using Finesboard.Api.Services;

namespace Finesboard.Api.RuleStatements
{
    public class UserHasNotRaisedFineOnFinesboard : IRuleStatement
    {
        private readonly IUserService _userService;
        private readonly Guid _userId;
        private readonly Guid _finesboardId;
        public UserHasNotRaisedFineOnFinesboard(IUserService userService, Guid userId, Guid finesboardId)
        {
            _userService = userService;
            _userId = userId;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var fines = _userService.GetFinesboardFinesByCreator(_finesboardId, _userId);
            if(fines.Count() > 0)
            {
                context.Fail($"User has raised fine on finesboard");
            }
        }
    }
}