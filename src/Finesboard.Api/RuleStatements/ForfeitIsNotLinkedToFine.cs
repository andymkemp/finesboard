using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ForfeitIsNotLinkedToFine : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _forfeitId;
        public ForfeitIsNotLinkedToFine(IFinesboardDataManager finesboardDataManager, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            if(_finesboardDataManager.IsForfeitLinkedToFine(_forfeitId))
            {
                context.Fail($"Forfeit is linked to a fine");
            }            
        }
    }
}