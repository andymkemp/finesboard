using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ForfeitIsLinkedToFinesboard : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        private readonly Guid _forfeitId;
        public ForfeitIsLinkedToFinesboard(IFinesboardDataManager finesboardDataManager, Guid finesboardId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var forfeit = _finesboardDataManager.GetForfeit(_forfeitId);
            if(forfeit == null || forfeit.FinesboardId != _finesboardId)
            {
                context.Fail($"Forfeit is not linked to the finesboard");
            }
        }
    }
}