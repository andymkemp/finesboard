using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ForfeitIsLinkedToOffence : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        private readonly Guid _forfeitId;
        public ForfeitIsLinkedToOffence(IFinesboardDataManager finesboardDataManager, Guid offenceId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfeitId);
            if(offenceForfeits == null || offenceForfeits.Where(x => x.OffenceId == _offenceId).Count() == 0)
            {
                context.Fail($"Forfeit is not linked to the offence");
            }            
        }
    }
}