using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceIsNotLinkedToTheForfeit : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        private readonly Guid _forfeitId;
        public OffenceIsNotLinkedToTheForfeit(IFinesboardDataManager finesboardDataManager, Guid offenceId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId);
            if(offenceForfeits.Where(x => x.ForfeitId == _forfeitId).Count() > 0)
            {
                context.Fail($"Offence is linked to the forfeit");
            }
        }
    }
}