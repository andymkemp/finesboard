using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ForfeitIsNotTheOnlyForfietLinkedToOffence : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _forfeitId;
        public ForfeitIsNotTheOnlyForfietLinkedToOffence(IFinesboardDataManager finesboardDataManager, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var forfeitOffences = _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfeitId);
            foreach(var forfeitOffence in forfeitOffences)
            {
                var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByOffenceId(forfeitOffence.OffenceId);
                if(!(offenceForfeits.Count() > 1))
                {
                    context.Fail($"Only forfeit linked to offence: {forfeitOffence.OffenceId}");
                }  
            }          
        }
    }
}