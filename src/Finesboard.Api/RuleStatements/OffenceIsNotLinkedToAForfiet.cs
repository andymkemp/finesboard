using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceIsNotLinkedToAForfiet : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        public OffenceIsNotLinkedToAForfiet(IFinesboardDataManager finesboardDataManager, Guid offenceId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId);
            if(offenceForfeits != null && offenceForfeits.Count() > 0)
            {
                context.Fail($"Offence is linked to a forfeit");
            }
        }
    }
}