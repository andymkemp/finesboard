using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceIsLinkedToMoreThanOneActiveForfeit : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        public OffenceIsLinkedToMoreThanOneActiveForfeit(IFinesboardDataManager finesboardDataManager, Guid offenceId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId);
            if(!(offenceForfeits.Where(x => x.Status == 1).Count() > 1))
            {
                context.Fail($"Offence is only linked to one active forfeit");
            }        
        }
    }
}