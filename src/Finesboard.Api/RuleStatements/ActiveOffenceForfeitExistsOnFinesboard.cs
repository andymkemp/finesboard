using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ActiveOffenceForfeitExistsOnFinesboard : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        private readonly Guid _offenceId;
        private readonly Guid _forfeitId;
        public ActiveOffenceForfeitExistsOnFinesboard(IFinesboardDataManager finesboardDataManager, Guid finesboardId, Guid offenceId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
            _offenceId = offenceId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var offenceForfeit = _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfeitId);
            if(offenceForfeit == null)
            {
                context.Fail($"Offence forfeit does not exist on finesboard");
                return;
            }
            if(offenceForfeit.Status != 1)
            {
                context.Fail($"Offence forfeit is not active");
            }
        }
    }
}