using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class FinesboardIsActive : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        public FinesboardIsActive(IFinesboardDataManager finesboardDataManager, Guid finesboardId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var finesboard = _finesboardDataManager.GetFinesboard(_finesboardId);
            if(finesboard.Status.Description != "Active")
            {
                context.Fail($"Finesboard status is not active");
            }
        }
    }
}