using System;
using System.Linq;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class ForfeitIsNotLinkedToMoreThanOneOffence : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _forfeitId;
        public ForfeitIsNotLinkedToMoreThanOneOffence(IFinesboardDataManager finesboardDataManager, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var offenceForfeits = _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfeitId);
            if(offenceForfeits.Count() > 1)
            {
                context.Fail($"Forfeit is linked to more than one offence");
            }  
        }
    }
}