using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class FinesboardHasNotBeenDeactivated : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        public FinesboardHasNotBeenDeactivated(IFinesboardDataManager finesboardDataManager, Guid finesboardId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {
            var finesboard = _finesboardDataManager.GetFinesboard(_finesboardId);
            if(finesboard.Status.Description == "Closed" || finesboard.Status.Description == "Archived")
            {
                context.Fail($"Finesboard is {finesboard.Status.Description}");
            }
        }
    }
}