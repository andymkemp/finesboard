using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceForfeitIsNotLinkedToFine : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _offenceId;
        private readonly Guid _forfeitId;
        public OffenceForfeitIsNotLinkedToFine(IFinesboardDataManager finesboardDataManager, Guid offenceId, Guid forfeitId)
        {
            _finesboardDataManager = finesboardDataManager;
            _offenceId = offenceId;
            _forfeitId = forfeitId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            if(_finesboardDataManager.IsOffenceForfeitLinkedToFine(_offenceId, _forfeitId))
            {
                context.Fail($"Offence forfeit is linked to a fine");
            }
        }
    }
}