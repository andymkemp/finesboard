using System;
using System.Linq;
using Finesboard.Api.Rules;
using Finesboard.Api.Services;

namespace Finesboard.Api.RuleStatements
{
    public class UserIsNotAMemberOfTheFinesboard : IRuleStatement
    {
        private readonly IUserService _userService;
        private readonly Guid _userId;
        private readonly Guid _finesboardId;
        public UserIsNotAMemberOfTheFinesboard(IUserService userService, Guid userId, Guid finesboardId)
        {
            _userService = userService;
            _userId = userId;
            _finesboardId = finesboardId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var finesboards = _userService.GetFinesboards(_userId);
            if(finesboards.Where(x => x.Id == _finesboardId).Count() > 0)
            {
                context.Fail($"User is a member of the finesboard");
            }
        }
    }
}