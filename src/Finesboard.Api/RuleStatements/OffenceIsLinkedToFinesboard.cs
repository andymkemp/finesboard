using System;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Rules;

namespace Finesboard.Api.RuleStatements
{
    public class OffenceIsLinkedToFinesboard : IRuleStatement
    {
        private readonly IFinesboardDataManager _finesboardDataManager;
        private readonly Guid _finesboardId;
        private readonly Guid _offenceId;
        public OffenceIsLinkedToFinesboard(IFinesboardDataManager finesboardDataManager, Guid finesboardId, Guid offenceId)
        {
            _finesboardDataManager = finesboardDataManager;
            _finesboardId = finesboardId;
            _offenceId = offenceId;
        }
        public void Execute(IRulesHandlerContext context)
        {            
            var offence = _finesboardDataManager.GetOffence(_offenceId);
            if(offence == null || offence.FinesboardId != _finesboardId)
            {
                context.Fail($"Offence is not linked to the finesboard");
            }
        }
    }
}