using Microsoft.AspNetCore.Authorization;

namespace Finesboard.Api.Authorization
{
    public class FinesboardUserOrRoleRequirement: IAuthorizationRequirement
    {
        public string[] Roles { get; }
        public FinesboardUserOrRoleRequirement(params string[] roles)
        {
            Roles = roles;
        }
    }
}