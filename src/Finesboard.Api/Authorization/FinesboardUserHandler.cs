using System.Security.Claims;
using System.Threading.Tasks;
using Finesboard.Api.DataManagers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Finesboard.Api.Authorization
{
    public class FinesboardUserHandler: AuthorizationHandler<FinesboardUserRequirement>
    {
        private readonly IFinesboardDataManager _dataManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public FinesboardUserHandler(IFinesboardDataManager dataManager, IHttpContextAccessor httpContextAccessor)
        {
            _dataManager = dataManager;
            _httpContextAccessor = httpContextAccessor;
        }
        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FinesboardUserRequirement requirement)
        {
            return Task.Factory.StartNew(async () => {
                if(await IsFinesboardUser(context))
                {
                    context.Succeed(requirement);
                } 
                else 
                {
                    context.Fail();
                }
            });
        }

        private async Task<bool> IsFinesboardUser(AuthorizationHandlerContext context)
        {
            try
            {
                _httpContextAccessor.HttpContext.Request.EnableBuffering();
                var username = context.User.FindFirstValue(ClaimTypes.Name); 
                if(username == null)
                {
                    return false;
                }
                var requestBody = await new StreamReader(stream: _httpContextAccessor.HttpContext.Request.Body, leaveOpen: true).ReadToEndAsync();
                _httpContextAccessor.HttpContext.Request.Body.Position = 0;
                var jObject = JObject.Parse(requestBody); 
                if(!jObject.TryGetValue("finesboardId", StringComparison.OrdinalIgnoreCase, out JToken finesboardIdToken))
                {
                    return false;
                }
                if(!Guid.TryParse((string)finesboardIdToken, out Guid finesboardId))
                {
                    return false;
                }
                var result = _dataManager.IsFinesboardUser(finesboardId, username);
                return result;                
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}