using System.Security.Claims;
using System.Threading.Tasks;
using Finesboard.Api.DataManagers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;
using System;

namespace Finesboard.Api.Authorization
{
    public class FinesboardUserOrRoleHandler: AuthorizationHandler<FinesboardUserOrRoleRequirement, Guid>
    {
        private readonly IFinesboardDataManager _dataManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public FinesboardUserOrRoleHandler(IFinesboardDataManager dataManager, IHttpContextAccessor httpContextAccessor)
        {
            _dataManager = dataManager;
            _httpContextAccessor = httpContextAccessor;
        }
        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FinesboardUserOrRoleRequirement requirement, Guid finesboardId)
        {
            if(HasRole(context, requirement) || IsFinesboardUser(context, finesboardId))
            {
                context.Succeed(requirement);
            } else {
                context.Fail();
            }
            return Task.CompletedTask;
        }

        private bool HasRole(AuthorizationHandlerContext context, FinesboardUserOrRoleRequirement requirement)
        {
            var roleClaims = context.User.FindFirstValue(ClaimTypes.Role);
            if(requirement.Roles.Length == 0 || roleClaims == null)
            {
                return false;
            }
            var isAuthorized = false;
            foreach (var role in requirement.Roles)
            {
                if(roleClaims.Contains(role))
                {
                    isAuthorized = true;
                }
            }
            return isAuthorized;
        }

        private bool IsFinesboardUser(AuthorizationHandlerContext context)
        {
            var username = context.User.FindFirstValue(ClaimTypes.Name);  
            var hasFinesboardId = Guid.TryParse(_httpContextAccessor.HttpContext.GetRouteValue("finesboardId").ToString(), out Guid finesboardId);          ;
            if(username == null || !hasFinesboardId)
            {
                return false;
            }
            var result = _dataManager.IsFinesboardUser(finesboardId, username);
            return result;
        }

        private bool IsFinesboardUser(AuthorizationHandlerContext context, Guid finesboardId)
        {
            var username = context.User.FindFirstValue(ClaimTypes.Name); 
            if(username == null || finesboardId == null)
            {
                return false;
            }
            var result = _dataManager.IsFinesboardUser(finesboardId, username);
            return result;
        }
    }
}