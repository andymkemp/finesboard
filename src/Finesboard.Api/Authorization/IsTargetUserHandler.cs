using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System;

namespace Finesboard.Api.Authorization
{
    public class IsTargetUserHandler: AuthorizationHandler<IsTargetUserRequirement, Guid>
    {        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsTargetUserRequirement requirement, Guid targetId)
        {
            if(IsTargetUser(context, targetId))
            {
                context.Succeed(requirement);
            } 
            else 
            {
                context.Fail();
            }
            return Task.CompletedTask;
        }

        private bool IsTargetUser(AuthorizationHandlerContext context, Guid targetId)
        {
            var hasUserId = Guid.TryParse(context.User.FindFirstValue(ClaimTypes.NameIdentifier), out Guid userId);  
            if(!hasUserId)
            {
                return false;
            }
            return userId == targetId;
        }
    }
}