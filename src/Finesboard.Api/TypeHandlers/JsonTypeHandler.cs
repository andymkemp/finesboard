using System.Collections;
using Newtonsoft.Json;
using static Dapper.SqlMapper;

namespace Finesboard.Api.TypeHandlers
{
    public class JsonTypeHandler<T> : StringTypeHandler<T> where T: IEnumerable
    {
        protected override string Format(T value)
        {
            throw new System.NotImplementedException();
        }

        protected override T Parse(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}