using System;
using System.Collections.Generic;
using System.Data;

namespace Finesboard.Api
{
    public class DapperConnectionManager: IDatabaseConnectionManager
    {
        private readonly IStatementBuilder _statementBuilder;
        private IDapperWrapper _dapper;
        
        public DapperConnectionManager(IDapperWrapper dapper, IStatementBuilder statementBuilder)
        {
            _dapper = dapper;
            _statementBuilder = statementBuilder;
        }

        public T SelectById<T>(Guid id) where T: IDbModel
        {
            var sql = _statementBuilder.SelectById<T>();
            return _dapper.QuerySingleOrDefault<T>(sql, new { Id = id });
        }

        public IEnumerable<T> SelectList<T>() where T: IDbModel
        {
            var sql = _statementBuilder.SelectAll<T>();
            return _dapper.Query<T>(sql);
        }

        public IEnumerable<T> SelectList<T>(object constraints) where T: IDbModel
        {
            var sql = _statementBuilder.SelectWhere<T>(constraints);
            return _dapper.Query<T>(sql, constraints);
        }

        public int Insert<T>(T data, IDbTransaction tran = null) where T: IDbModel
        {
            var sql = _statementBuilder.Insert<T>();
            return _dapper.Execute(sql, data, tran);
        }

        public int Insert<T>(T data, string[] ignore, IDbTransaction tran = null) where T: IDbModel
        {
            var sql = _statementBuilder.Insert<T>(ignore);
            return _dapper.Execute(sql, data);
        }

        public int Insert<T>(IEnumerable<T> data) where T: IDbModel
        {
            var sql = _statementBuilder.Insert<T>();
            return _dapper.Execute(sql, data);
        }

        public int Update<T>(T data, params string[] ignore) where T: IDbModel
        {
            var sql = _statementBuilder.UpdateById<T>(ignore);
            return _dapper.Execute(sql, data);
        }

        public int Update<T>(T data, object where, params string[] ignore) where T: IDbModel
        {
            var sql = _statementBuilder.UpdateWhere<T>(where, ignore);
            return _dapper.Execute(sql, data);
        }

        public int Update<T>(IEnumerable<T> data) where T: IDbModel
        {
            var sql = _statementBuilder.UpdateById<T>();
            return _dapper.Execute(sql, data);
        }

        public int Delete<T>(Guid id) where T: IDbModel
        {
            var sql = _statementBuilder.DeleteById<T>();
            return _dapper.Execute(sql, new { Id = id });
        }

        public int Delete<T>(object where) where T: IDbModel
        {
            var sql = _statementBuilder.DeleteWhere<T>(where);
            return _dapper.Execute(sql, where);
        }

        public IEnumerable<T1> Query<T1>(IStatement<T1> statement)
        {
            var sql = statement.Statement();
            var result = _dapper.Query<T1>(sql, statement);
            return result;
        }

        public IEnumerable<U> Query<T1,T2,U>(IStatement<T1,T2,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,U>(sql, map, statement, null, splitOn);
        }

        public IEnumerable<U> Query<T1,T2,T3,U>(IStatement<T1,T2,T3,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,T3,U>(sql, map, statement, null, splitOn);
        }

        public IEnumerable<U> Query<T1,T2,T3,T4,U>(IStatement<T1,T2,T3,T4,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,T3,T4,U>(sql, map, statement, null, splitOn);
        }

        public IEnumerable<U> Query<T1,T2,T3,T4,T5,U>(IStatement<T1,T2,T3,T4,T5,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,T3,T4,T5,U>(sql, map, statement, null, splitOn);
        }

        public IEnumerable<U> Query<T1,T2,T3,T4,T5,T6,U>(IStatement<T1,T2,T3,T4,T5,T6,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,T3,T4,T5,T6,U>(sql, map, statement, null, splitOn);
        }

        public IEnumerable<U> Query<T1,T2,T3,T4,T5,T6,T7,U>(IStatement<T1,T2,T3,T4,T5,T6,T7,U> statement)
        {
            var sql = statement.Statement();
            var map = statement.Map();
            var splitOn = statement.SplitOn() == "" ? "Id" : statement.SplitOn();
            return _dapper.Query<T1,T2,T3,T4,T5,T6,T7,U>(sql, map, statement, null, splitOn);
        }

        
        public IDbTransaction BeginTransaction()
        {
            return _dapper.BeginTransaction();
        }
        
        public void Open()
        {
            _dapper.Open();
        }
        
        public void Dispose()
        {
            _dapper.Dispose();
        }

        public void Connect(IDbConnection dbConnection)
        {
            _dapper.Connect(dbConnection);
        }
    }    
}