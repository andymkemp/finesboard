using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Finesboard.Api.Models;
using Finesboard.Api.TypeHandlers;

namespace Finesboard.Api
{
    public class DapperWrapper: IDapperWrapper
    {
        private IDbConnection _dbConnection;

        public DapperWrapper()
        {
            SqlMapper.AddTypeHandler(new JsonTypeHandler<IList<FinesboardDetailDataModel>>());
            SqlMapper.AddTypeHandler(new JsonTypeHandler<IList<OffenceDetailDataModel>>());
            SqlMapper.AddTypeHandler(new JsonTypeHandler<IList<ForfeitDataModel>>());
            SqlMapper.AddTypeHandler(new JsonTypeHandler<IList<LinkedForfeitDataModel>>());
        }
        
        public void Dispose()
        {
            if(_dbConnection != null)
            {
                _dbConnection.Dispose();
            }
        }

        public int Execute(string sql, object param = null, IDbTransaction transaction = null)
        {
            return _dbConnection.Execute(sql, param, transaction);
        }

        public IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null)
        {
            return _dbConnection.Query<T>(sql, param, transaction);
        }

        public IEnumerable<U> Query<T1, T2, U>(string sql, Func<T1, T2, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public IEnumerable<U> Query<T1, T2, T3, U>(string sql, Func<T1, T2, T3, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public IEnumerable<U> Query<T1, T2, T3, T4, U>(string sql, Func<T1, T2, T3, T4, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public IEnumerable<U> Query<T1, T2, T3, T4, T5, U>(string sql, Func<T1, T2, T3, T4, T5, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public IEnumerable<U> Query<T1, T2, T3, T4, T5, T6, U>(string sql, Func<T1, T2, T3, T4, T5, T6, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public IEnumerable<U> Query<T1, T2, T3, T4, T5, T6, T7, U>(string sql, Func<T1, T2, T3, T4, T5, T6, T7, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id")
        {
            return _dbConnection.Query(sql, map, param, transaction, true, splitOn);
        }

        public T QuerySingleOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null)
        {
            return _dbConnection.QuerySingleOrDefault<T>(sql, param, transaction);
        }
        
        public IDbTransaction BeginTransaction()
        {
            return _dbConnection.BeginTransaction();
        }
        
        public void Open()
        {
            _dbConnection.Open();
        }

        public void Connect(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }
    }
}