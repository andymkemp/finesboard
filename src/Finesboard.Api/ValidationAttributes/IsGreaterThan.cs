using System;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ValidationAttributes
{
    public class IsGreaterThan : ValidationAttribute
    {
        private decimal _comparisonValue;
        public IsGreaterThan(int value)
        {
            _comparisonValue = Convert.ToDecimal(value);
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if((decimal)value > _comparisonValue)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult($"{validationContext.MemberName} must be greater than {_comparisonValue}");
            }
        }
    }
}