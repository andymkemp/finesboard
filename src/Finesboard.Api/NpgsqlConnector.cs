using System.Data;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Finesboard.Api
{
    public class NpgsqlConnector : IDatabaseConnector
    {
        public readonly IConfiguration _config;
        public readonly IDatabaseConnectionManager _databaseConnectionManager;
        public NpgsqlConnector(IConfiguration config, IDatabaseConnectionManager databaseConnectionManager)
        {
            _config = config;
            _databaseConnectionManager = databaseConnectionManager;
        }
        public IDatabaseConnectionManager Connect()
        {
            var connectionString = _config.GetConnectionString("finesboard");
            var dbConnection = new NpgsqlConnection(connectionString);
            _databaseConnectionManager.Connect(dbConnection);
            return _databaseConnectionManager;
        }
    }
}