using System;

namespace Finesboard.Api
{
    public interface IStatement
    {
        string Statement();
    }

    public interface IStatement<T1> : IStatement
    {
        
    }

    public interface IStatement<T1, T2, U> : IStatement
    {
        Func<T1, T2, U> Map();
        string SplitOn();
    }

    public interface IStatement<T1, T2, T3, U> : IStatement
    {
        Func<T1, T2, T3, U> Map();
        string SplitOn();
    }

    public interface IStatement<T1, T2, T3, T4, U> : IStatement
    {
        Func<T1, T2, T3, T4, U> Map();
        string SplitOn();
    }

    public interface IStatement<T1, T2, T3, T4, T5, U> : IStatement
    {
        Func<T1, T2, T3, T4, T5, U> Map();
        string SplitOn();
    }

    public interface IStatement<T1, T2, T3, T4, T5, T6, U> : IStatement
    {
        Func<T1, T2, T3, T4, T5, T6, U> Map();
        string SplitOn();
    }

    public interface IStatement<T1, T2, T3, T4, T5, T6, T7, U> : IStatement
    {
        Func<T1, T2, T3, T4, T5, T6, T7, U> Map();
        string SplitOn();
    }
}