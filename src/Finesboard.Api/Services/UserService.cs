
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Finesboard.Api.ApiModels;
using Finesboard.Api.DataManagers;
using Finesboard.Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace Finesboard.Api.Services
{    
    public class UserService: IUserService
    {
        private readonly IUserDataManager _userDataManager;
        private readonly IMapper _mapper;
        private IPasswordHasher<object> _passwordHasher;
        public UserService(IUserDataManager userDataManager, IMapper mapper, IPasswordHasher<object> passwordMapper)
        {
            _userDataManager = userDataManager;
            _mapper = mapper;
            _passwordHasher = passwordMapper;
        }

        public AuthenticatedModel Authenticate(string username, string password)
        {
            var user = _userDataManager.GetUserByUsername(username);
            if (user == null || 
                _passwordHasher.VerifyHashedPassword(user, user.Password, password) == PasswordVerificationResult.Failed)
            {
                return null;
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes("SuperSecretPassword");
            var claims = new ClaimsIdentity(new Claim[] 
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            });
            var userRoles = _userDataManager.GetUserRoles((Guid)user.Id);
            foreach (var userRole in userRoles)
            {
                claims.AddClaim(new Claim(ClaimTypes.Role, userRole.Code));
            }
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new AuthenticatedModel() { Token = tokenHandler.WriteToken(token) };
        }

        public UserRegisteredModel Register(RegisterUserModel user) 
        {
            user.Password = _passwordHasher.HashPassword(user, user.Password);
            var userRegistration = _mapper.Map<UserLoginDataModel>(user);
            var registeredUser = _userDataManager.AddUser(userRegistration);
            return _mapper.Map<UserRegisteredModel>(registeredUser);
        }

        public void ChangePassword(Guid userId, string newPassword) 
        {            
            var hashedPassword = _passwordHasher.HashPassword(userId, newPassword);
            _userDataManager.ChangePassword(userId, hashedPassword);
        }

        public IEnumerable<UserDetailModel> GetUsers() 
        {
            var users = _userDataManager.GetUsersList();
            return _mapper.Map<IEnumerable<UserDetailModel>>(users);
        }
        public IEnumerable<FinesboardExtendedDetailModel> GetFinesboards(Guid userId) 
        {
            var finesboards = _userDataManager.GetUserFinesboardsList(userId);
            return _mapper.Map<IEnumerable<FinesboardExtendedDetailModel>>(finesboards);
        }

        public UserDetailModel GetUserDetails(Guid userId) 
        {
            var users = _userDataManager.GetUserDetails(userId);
            return _mapper.Map<UserDetailModel>(users);
        }

        public IEnumerable<FinesboardExtendedDetailModel> GetFinesboardsByStatus(Guid userId, string status) 
        {
            var finesboards = _userDataManager.GetUserFinesboardsListByStatus(userId, status);
            return _mapper.Map<IEnumerable<FinesboardExtendedDetailModel>>(finesboards);
        }

        public IEnumerable<FineDetailModel> GetFinesboardFinesByTarget(Guid finesboardId, Guid targetId) 
        {
            var fines = _userDataManager.GetFinesboardFinesByTarget(finesboardId, targetId);
            return _mapper.Map<IEnumerable<FineDetailModel>>(fines);
        }

        public IEnumerable<FineDetailModel> GetFinesboardFinesByCreator(Guid finesboardId, Guid creatorId) 
        {
            var fines = _userDataManager.GetFinesboardFinesByCreator(finesboardId, creatorId);
            return _mapper.Map<IEnumerable<FineDetailModel>>(fines);
        }
    }
}