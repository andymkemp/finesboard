using System;
using System.Collections.Generic;
using Finesboard.Api.ApiModels;

namespace Finesboard.Api.Services
{
    public interface IUserService
    {
        AuthenticatedModel Authenticate(string username, string password);
        UserRegisteredModel Register(RegisterUserModel user);
        void ChangePassword(Guid userId, string newPassword);
        IEnumerable<UserDetailModel> GetUsers();
        IEnumerable<FinesboardExtendedDetailModel> GetFinesboards(Guid userId);
        UserDetailModel GetUserDetails(Guid userId);
        IEnumerable<FinesboardExtendedDetailModel> GetFinesboardsByStatus(Guid userId, string status);
        IEnumerable<FineDetailModel> GetFinesboardFinesByTarget(Guid finesboardId, Guid targetId);
        IEnumerable<FineDetailModel> GetFinesboardFinesByCreator(Guid finesboardId, Guid creatorId);
    }
}