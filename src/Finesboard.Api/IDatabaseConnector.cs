using System.Data;

namespace Finesboard.Api
{
    public interface IDatabaseConnector
    {
        IDatabaseConnectionManager Connect();
    }
}