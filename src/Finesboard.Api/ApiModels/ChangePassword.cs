using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class ChangePasswordModel: IApiModel
    {
        [Required]
        public string NewPassword { get; set; }
    }    
}