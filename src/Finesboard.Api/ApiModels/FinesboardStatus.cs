using System;

namespace Finesboard.Api.ApiModels
{
    public class FinesboardStatusModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
    
}