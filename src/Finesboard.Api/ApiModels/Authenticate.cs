using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class AuthenticateModel: IApiModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
    
    public class AuthenticatedModel: IApiModel
    {
        public string Token { get; set; }
    }
}