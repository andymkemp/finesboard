using System;

namespace Finesboard.Api.ApiModels
{
    public class OffenceForfeitModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public Guid FinesboardId { get; set; }
    }    
    public class UpdateOffenceForfeitModel: IApiModel
    {
        public int Status { get; set; }
    }    
}