using System;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class FineDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public UserModel Target { get; set; }
        public UserModel Creator { get; set; }
        public string Reason { get; set; }
        public decimal Amount { get; set; }
        public FinesboardDetailModel Finesboard { get; set; }
        public OffenceModel Offence { get; set; }
        public ForfeitModel Forfeit { get; set; }
    }
    
    public class AddFineModel: IApiModel
    {
        [Required]
        public Guid TargetId { get; set; }
        [Required]
        public string Reason { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Guid FinesboardId { get; set; }
        [Required]
        public Guid OffenceId { get; set; }
        [Required]
        public Guid ForfeitId { get; set; }
    }
    
    public class AddFinesboardFineModel: IApiModel
    {
        [Required]
        public Guid TargetId { get; set; }
        [Required]
        public string Reason { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Guid OffenceId { get; set; }
        [Required]
        public Guid ForfeitId { get; set; }
    }
    
    public class UpdateFineModel: IApiModel
    {
        [Required]
        public Guid TargetId { get; set; }
        [Required]
        public Guid CreatorId { get; set; }
        [Required]
        public string Reason { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Guid FinesboardId { get; set; }
        [Required]
        public Guid OffenceId { get; set; }
        [Required]
        public Guid ForfeitId { get; set; }
    }
}