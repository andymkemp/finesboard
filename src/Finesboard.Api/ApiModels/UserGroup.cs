using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class UserGroupModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}