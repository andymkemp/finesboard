using System;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{public 
    class ForfeitModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }  
    public class ForfeitDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public Guid FinesboardId { get; set; }

    }    
    public class AddForfeitModel: IApiModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Guid FinesboardId { get; set; }
    }    
}