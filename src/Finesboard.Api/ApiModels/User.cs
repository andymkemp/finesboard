using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class UserModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
    }    

    public class UserDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public IList<FinesboardExtendedDetailModel> Finesboards { get; set; }
    }    
    
    public class AddUserModel: IApiModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }    
}