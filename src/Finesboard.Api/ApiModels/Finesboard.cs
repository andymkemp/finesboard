using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class FinesboardModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }    
    public class FinesboardDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public FinesboardStatusModel Status { get; set; }
        public string Currency {get; set; }
    }    
    public class FinesboardExtendedDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public FinesboardStatusModel Status { get; set; }
        public string Currency {get; set; }
        public IList<OffenceDetailModel> Offences { get; set; }
        public IList<ForfeitDetailModel> Forfeits { get; set; }
    }    
    public class AddFinesboardModel: IApiModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Currency {get; set; }
    }    
}