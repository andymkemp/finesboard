using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class OffenceModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid FinesboardId { get; set; }
    }    
    public class OffenceDetailModel: IApiModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid FinesboardId { get; set; }
        public IList<OffenceForfeitModel> Forfeits { get; set; }
    }    
    public class AddOffenceModel: IApiModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid FinesboardId { get; set; }
        [Required]
        public IList<ForfeitDetailModel> Forfeits { get; set; }
    }    
}