using System.ComponentModel.DataAnnotations;

namespace Finesboard.Api.ApiModels
{
    public class RegisterUserModel: IApiModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
    
    public class UserRegisteredModel: IApiModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
    }
}