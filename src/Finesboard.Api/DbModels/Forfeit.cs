using System;

namespace Finesboard.Api.Models
{
    public class ForfeitDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public Guid FinesboardId { get; set; }
    }      
    public class LinkedForfeitDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
        public Guid FinesboardId { get; set; }
    }      
}