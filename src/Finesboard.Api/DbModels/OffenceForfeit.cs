using System;

namespace Finesboard.Api.Models
{
    public class OffenceForfeitDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public Guid OffenceId { get; set; }
        public Guid ForfeitId { get; set; }
        public int Status { get; set; }
    }    
}