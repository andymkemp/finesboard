using System;

namespace Finesboard.Api.Models
{
    public class UserGroupDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
    }    
}