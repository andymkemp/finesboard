using System;
using System.Collections.Generic;

namespace Finesboard.Api.Models
{
    public class FinesboardDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public Guid FinesboardStatusId { get; set; }
        public string Name { get; set; }
        public string Currency {get; set; }
    }    
    
    public class FinesboardDetailDataModel: IDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public FinesboardStatusDataModel Status { get; set; }
        public string Currency {get; set; }
        public IList<OffenceDetailDataModel> Offences { get; set; }
        public IList<ForfeitDataModel> Forfeits { get; set; }
    }    
}