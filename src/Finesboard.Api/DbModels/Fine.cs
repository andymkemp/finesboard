using System;

namespace Finesboard.Api.Models
{    
    public class FineDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public Guid TargetId { get; set; }
        public Guid CreatorId { get; set; }
        public string Reason { get; set; }
        public decimal Amount { get; set; }
        public Guid FinesboardId { get; set; }
        public Guid OffenceId { get; set; }
        public Guid ForfeitId { get; set; }
    }
    public class FineDetailDataModel: IDataModel
    {
        public Guid Id { get; set; }
        public UserLoginDataModel Target { get; set; }
        public UserLoginDataModel Creator { get; set; }
        public string Reason { get; set; }
        public decimal Amount { get; set; }
        public FinesboardDetailDataModel Finesboard { get; set; }
        public OffenceDataModel Offence { get; set; }
        public ForfeitDataModel Forfeit { get; set; }
    }
    
}