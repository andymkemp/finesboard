using System;

namespace Finesboard.Api.Models
{
    public class UserRoleDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }    
}