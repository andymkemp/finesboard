using System;

namespace Finesboard.Api.Models
{
    public class FinesboardStatusDataModel: IDbModel, IDataModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
    
}