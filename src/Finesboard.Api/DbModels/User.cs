using System;
using System.Collections.Generic;

namespace Finesboard.Api.Models
{
    public class UserLoginDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }    

    public class UserDetailDataModel: IDataModel
    {
        public Guid? Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public IEnumerable<FinesboardDetailDataModel> Finesboards { get; set; }
    }
}