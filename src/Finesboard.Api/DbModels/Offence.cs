using System;
using System.Collections.Generic;

namespace Finesboard.Api.Models
{
    public class OffenceDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public Guid FinesboardId { get; set; }
    }    
    
    public class OffenceDetailDataModel: IDataModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid FinesboardId { get; set; }
        public IList<LinkedForfeitDataModel> Forfeits { get; set; }
    }  
}