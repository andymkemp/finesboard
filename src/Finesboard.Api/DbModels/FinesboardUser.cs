using System;

namespace Finesboard.Api.Models
{
    public class FinesboardUserDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public Guid UserLoginId { get; set; }
        public Guid FinesboardId { get; set; }
        public int Status { get; set; }
    }    
}