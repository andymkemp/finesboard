using System;

namespace Finesboard.Api.Models
{
    public class MemberDataModel: IDbModel, IDataModel
    {
        public Guid? Id { get; set; }
        public Guid UserId { get; set; }
        public Guid UserGroupId { get; set; }
    }    
}