using System;
using System.Collections.Generic;
using System.Data;

namespace Finesboard.Api
{
    public interface IDapperWrapper : IDisposable
    {
        IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null);
        IEnumerable<U> Query<T1, T2, U>(string sql, Func<T1, T2, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        IEnumerable<U> Query<T1, T2, T3, U>(string sql, Func<T1, T2, T3, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        IEnumerable<U> Query<T1, T2, T3, T4, U>(string sql, Func<T1, T2, T3, T4, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        IEnumerable<U> Query<T1, T2, T3, T4, T5, U>(string sql, Func<T1, T2, T3, T4, T5, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        IEnumerable<U> Query<T1, T2, T3, T4, T5, T6, U>(string sql, Func<T1, T2, T3, T4, T5, T6, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        IEnumerable<U> Query<T1, T2, T3, T4, T5, T6, T7, U>(string sql, Func<T1, T2, T3, T4, T5, T6, T7, U> map, object param = null, IDbTransaction transaction = null, string splitOn = "Id");
        int Execute(string sql, object param = null, IDbTransaction transaction = null);
        T QuerySingleOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null);
        IDbTransaction BeginTransaction();
        void Open();
        void Connect(IDbConnection dbConnection);
    }
}