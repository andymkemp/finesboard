using System;

namespace Finesboard.Api.Constants
{
    public static class FinesboardStatus
    {
        public static readonly Guid Pending = Guid.Parse("79b65eb7-aebe-4faf-87ba-f7a61ed073da");
        public static readonly Guid Active = Guid.Parse("e55327ed-2e1b-4135-818a-608e09c7630c");
        public static readonly Guid Closed = Guid.Parse("a996a5e0-1f10-471b-96e4-ae50238b7001");
        public static readonly Guid Archived = Guid.Parse("df216125-9ab9-4fa0-9267-c9facc0c7c18");
    }
}