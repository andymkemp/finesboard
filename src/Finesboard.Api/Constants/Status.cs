using System;

namespace Finesboard.Api.Constants
{
    public static class Status
    {
        public static readonly int Inactive = 0;
        public static readonly int Active = 1;
    }
}