using System;
using Machine.Specifications;
using Machine.Fakes;
using Finesboard.Api.Authorization;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Collections.Generic;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(IsTargetUserHandler))]
    public class when_authenticated_user_matches_target_user: WithSubject<IsTargetUserHandler>
    {       
        private static IsTargetUserRequirement _requirement;
        private static Guid _resource;
        private static AuthorizationHandlerContext _context;
        Establish context = () => {
            _requirement = new IsTargetUserRequirement(); 
            _resource = Guid.NewGuid();
            var userId = _resource.ToString();
            var user = new ClaimsPrincipal(
                new ClaimsIdentity(new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, userId)})
            );
            _context = new AuthorizationHandlerContext(
                new[] { _requirement },
                user,
                _resource
            );
        };

        Because of = () => {
            Subject.HandleAsync(_context).Await();
        };
        
        It should_succeed = () => {
            _context.HasSucceeded.ShouldBeTrue();
        };
    }
}