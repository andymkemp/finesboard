using FakeItEasy;
using System;
using Machine.Specifications;
using Machine.Fakes;
using Finesboard.Api.Authorization;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Finesboard.Api.DataManagers;
using System.Collections.Generic;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(FinesboardUserOrRoleHandler))]
    public class when_does_not_have_role_and_is_not_finesboard_user: WithSubject<FinesboardUserOrRoleHandler>
    {       
        private static FinesboardUserOrRoleRequirement _requirement;
        private static Guid _resource;
        private static AuthorizationHandlerContext _context;
        Establish context = () => {
            var role = "admin";
            _requirement = new FinesboardUserOrRoleRequirement(role); 
            _resource = Guid.NewGuid();
            var username = "user1";
            var user = new ClaimsPrincipal(
                new ClaimsIdentity(new List<Claim>() { new Claim(ClaimTypes.Name, username)})
            );
            _context = new AuthorizationHandlerContext(
                new[] { _requirement },
                user,
                _resource
            );
            The<IFinesboardDataManager>().WhenToldTo(x => x.IsFinesboardUser(_resource, username)).Return(false);
        };

        Because of = () => {
            Subject.HandleAsync(_context).Await();
        };
        
        It should_not_succeed = () => {
            _context.HasSucceeded.ShouldBeFalse();
        };
    }
}