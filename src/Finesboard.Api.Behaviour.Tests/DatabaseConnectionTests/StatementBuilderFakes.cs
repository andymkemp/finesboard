using Finesboard.Api.Models;
using Machine.Fakes;
using FakeItEasy;

namespace Finesboard.Api.Behaviour.Tests
{
    public class StatementBuilderFakes
    {
        private static string _sql;
        public StatementBuilderFakes(string sql)
        {
            _sql = sql;
        }
        OnEstablish context = fakeAccessor =>
        {
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.SelectById<FineDataModel>()).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.SelectAll<FineDataModel>()).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.SelectWhere<FineDataModel>(A<object>.Ignored)).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.Insert<FineDataModel>()).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.UpdateById<FineDataModel>()).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.UpdateById<FineDataModel>(A<string[]>.Ignored)).Return(_sql);
            fakeAccessor.The<IStatementBuilder>().WhenToldTo(x => x.DeleteById<FineDataModel>()).Return(_sql);
        };
    }
}