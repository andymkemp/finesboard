using FakeItEasy;
using System;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_deleting_single_record: WithSubject<DapperConnectionManager>
    {
        private static string _sql;
        private static Guid _id;
        private static int _expectedResult;
        private static int _actualResult;
        
        Establish context = () => {
            _expectedResult = 1;
            _id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB");
            _sql = "SELECT/INSERT/UPDATE/DELETE";
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Execute(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(_expectedResult);
        };

        Because of = () => {
            _actualResult = Subject.Delete<FineDataModel>(_id);
        };
        
        It should_build_delete_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.DeleteById<FineDataModel>()).OnlyOnce();
        };

        It should_delete_fine = () => {
            The<IDapperWrapper>().WasToldTo(x => x.Execute(_sql, A<object>.That.Matches(y => Match.Guids(y, _id)), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_result_of_delete_execution = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}