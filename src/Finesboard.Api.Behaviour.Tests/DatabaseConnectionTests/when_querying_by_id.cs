using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_querying_by_id: WithSubject<DapperConnectionManager>
    {
        private static Guid _id;
        private static string _sql;
        private static FineDataModel _expectedResult;
        private static FineDataModel _actualResult;
        
        Establish context = () => {
            _id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB");
            _expectedResult = new FineDataModel();
            _sql = "SELECT/INSERT/UPDATE/DELETE";
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Query<FineDataModel>(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(new List<FineDataModel>{ _expectedResult });
        };

        Because of = () => {
            _actualResult = Subject.SelectById<FineDataModel>(_id);
        };
        
        It should_build_select_by_id_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.SelectById<FineDataModel>()).OnlyOnce();
        };

        It should_run_select_by_id_query = () => {
            The<IDapperWrapper>().WasToldTo(x => x.QuerySingleOrDefault<FineDataModel>(_sql, A<object>.That.Matches(y => Match.Guids(y, _id)), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_list_of_fines = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}