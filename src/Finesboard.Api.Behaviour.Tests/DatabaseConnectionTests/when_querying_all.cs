using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_querying_all: WithSubject<DapperConnectionManager>
    {
        private static Guid _id;
        private static string _sql;
        private static IEnumerable<FineDataModel> _expectedResult;
        private static IEnumerable<FineDataModel> _actualResult;
        
        Establish context = () => {
            _id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB");
            _expectedResult = new List<FineDataModel>() { new FineDataModel() };
            _sql = "SELECT/INSERT/UPDATE/DELETE";      
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Query<FineDataModel>(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(_expectedResult);
        };

        Because of = () => {
            _actualResult = Subject.SelectList<FineDataModel>();
        };
        
        It should_build_select_all_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.SelectAll<FineDataModel>()).OnlyOnce();
        };

        It should_run_select_all_query = () => {
            The<IDapperWrapper>().WasToldTo(x => x.Query<FineDataModel>(_sql, A<object>.That.IsNull(), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_list_of_fines = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}