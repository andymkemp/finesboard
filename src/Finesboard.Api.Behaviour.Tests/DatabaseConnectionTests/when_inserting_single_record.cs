using FakeItEasy;
using System;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_inserting_single_record: WithSubject<DapperConnectionManager>
    {
        private static string _sql;
        private static FineDataModel _data;
        private static int _expectedResult;
        private static int _actualResult;
        
        Establish context = () => {
            _expectedResult = 1;
            _sql = "SELECT/INSERT/UPDATE/DELETE";
            _data = new FineDataModel() {Id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB")};
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Execute(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(_expectedResult);
        };

        Because of = () => {
            _actualResult = Subject.Insert<FineDataModel>(_data);
        };
        
        It should_build_insert_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.Insert<FineDataModel>()).OnlyOnce();
        };

        It should_insert_fine = () => {
            The<IDapperWrapper>().WasToldTo(x => x.Execute(_sql, A<FineDataModel>.That.IsNotNull(), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_result_of_insert_execution = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}