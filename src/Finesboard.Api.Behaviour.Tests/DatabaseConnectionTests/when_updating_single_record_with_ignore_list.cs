using FakeItEasy;
using System;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_updating_single_record_with_ignore_list: WithSubject<DapperConnectionManager>
    {
        private static string _sql;
        private static FineDataModel _data;
        private static int _expectedResult;
        private static int _actualResult;
        private static string[] _ignore;
        
        Establish context = () => {
            _expectedResult = 1;
            _sql = "SELECT/INSERT/UPDATE/DELETE";
            _data = new FineDataModel() {Id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB")};
            _ignore = new string[] { nameof(FineDataModel.TargetId), nameof(FineDataModel.CreatorId) };
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Execute(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(_expectedResult);
        };

        Because of = () => {
            _actualResult = Subject.Update<FineDataModel>(_data, _ignore);
        };
        
        It should_build_update_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.UpdateById<FineDataModel>(_ignore)).OnlyOnce();
        };

        It should_run_update_fines = () => {
            The<IDapperWrapper>().WasToldTo(x => x.Execute(_sql, A<FineDataModel>.That.IsNotNull(), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_result_update_execution = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}