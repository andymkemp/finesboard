using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Models;
using Machine.Specifications;
using Machine.Fakes;
using System.Data;

namespace Finesboard.Api.Behaviour.Tests
{
    [Subject(typeof(DapperConnectionManager))]
    public class when_updating_multiple_records: WithSubject<DapperConnectionManager>
    {
        private static string _sql;
        private static List<FineDataModel> _data;
        private static int _expectedResult;
        private static int _actualResult;
        
        Establish context = () => {
            _expectedResult = 2;
            _sql = "SELECT/INSERT/UPDATE/DELETE";
            _data = new List<FineDataModel>() {
                new FineDataModel() {Id = Guid.Parse("00AF0F21-7224-49AB-B83B-AA2800A12BDB")},
                new FineDataModel() {Id = Guid.Parse("e1be2f75-e1a6-4968-8a70-0a32cc7014d3")} 
            };
            With(new StatementBuilderFakes(_sql));
            The<IDapperWrapper>().WhenToldTo(x => x.Execute(A<string>.Ignored, A<object>.Ignored, A<IDbTransaction>.Ignored)).Return(_expectedResult);
        };

        Because of = () => {
            _actualResult = Subject.Update<FineDataModel>(_data);
        };
        
        It should_build_update_statement = () => {
            The<IStatementBuilder>().WasToldTo(x => x.UpdateById<FineDataModel>()).OnlyOnce();
        };

        It should_run_update_fines = () => {
            The<IDapperWrapper>().WasToldTo(x => x.Execute(_sql, A<List<FineDataModel>>.That.IsNotNull(), A<IDbTransaction>.That.IsNull())).OnlyOnce();
        };

        It should_return_result_update_execution = () => {
            _actualResult.ShouldBeLike(_expectedResult);
        };
    }
}