using System;

namespace Finesboard.Api.Behaviour.Tests
{
    public static class Match
    {
        public static bool Guids(object obj, Guid expectedId) {
            var type = obj.GetType();
            var actualId = type.GetProperty("Id");
            return (Guid)actualId.GetValue(obj) == expectedId;
        }

        public static bool ObjProp(object actual, object expected, string propName) {
            var actualProp = actual.GetType().GetProperty(propName);
            var expectedProp = expected.GetType().GetProperty(propName);
            if(actualProp != null && actualProp.GetValue(actual) == expectedProp.GetValue(expected))
            {
                return true;
            }
            return false;
        }
    }
}