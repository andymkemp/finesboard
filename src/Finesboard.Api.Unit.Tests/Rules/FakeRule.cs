using Finesboard.Api.Rules;

namespace Finesboard.Api.Unit.Tests
{
    public class FakeRule: IRuleStatement
    {
        public string Message { get; set; }
        public FakeRule(string message = null)
        {
            Message = message;
        }
        public void Execute(IRulesHandlerContext context) {
            if(!string.IsNullOrEmpty(Message))
            {
                context.Fail(Message);
            }
        }
    }
}