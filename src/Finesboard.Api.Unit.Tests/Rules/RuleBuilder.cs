using Finesboard.Api.Rules;

namespace Finesboard.Api.Unit.Tests
{
    public static class RuleBuilder
    {
        public static FakeRule AFailingRule() {
            return new FakeRule("Standard test failure message");
        }
        public static FakeRule APassingRule() {
            return new FakeRule();
        }
        public static FakeRule AFailingRuleWithMessage(string message) {
            return new FakeRule(message);
        }
    }
}
