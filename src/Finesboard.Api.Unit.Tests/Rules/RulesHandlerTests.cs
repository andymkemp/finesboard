using Finesboard.Api.Rules;
using NUnit.Framework;

namespace Finesboard.Api.Unit.Tests
{
    [TestFixture]
    public class RulesHandlerTests
    {
        [Test]
        public void When_rule_fails_it_should_return_failure() {
            // arrange
            var rulesService = new RulesHandler();
            var failingRule = RuleBuilder.AFailingRule();
            rulesService.Register(failingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Is.Not.Null);
            Assert.That(result.Messages, Has.One.EqualTo(failingRule.Message));
        }

        [Test]
        public void When_rule_passes_it_should_return_success() {
            // arrange
            var rulesService = new RulesHandler();
            var passingRule = RuleBuilder.APassingRule();
            rulesService.Register(passingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
            Assert.That(result.Messages, Is.Null);
        }

        [Test]
        public void When_no_rules_are_registered_and_rules_are_executed_it_should_return_success() {
            // arrange
            var rulesService = new RulesHandler();
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
            Assert.That(result.Messages, Is.Null);
        }

        [Test]
        public void When_two_rules_are_registered_and_first_rule_fails_it_should_return_failure() {
            // arrange
            var rulesService = new RulesHandler();
            var failingRule = RuleBuilder.AFailingRule();
            rulesService.Register(failingRule);
            var passingRule = RuleBuilder.APassingRule();
            rulesService.Register(passingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Is.Not.Null);
            Assert.That(result.Messages, Has.One.EqualTo(failingRule.Message));
        }

        [Test]
        public void When_two_rules_are_registered_and_second_rule_fails_it_should_return_failure() {
            // arrange
            var rulesService = new RulesHandler();
            var passingRule = RuleBuilder.APassingRule();
            rulesService.Register(passingRule);
            var failingRule = RuleBuilder.AFailingRule();
            rulesService.Register(failingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Is.Not.Null);
            Assert.That(result.Messages, Has.One.EqualTo(failingRule.Message));
        }

        [Test]
        public void When_two_rules_are_registered_and_all_rules_pass_it_should_return_success() {
            // arrange
            var rulesService = new RulesHandler();
            var firstPassingRule = RuleBuilder.APassingRule();
            rulesService.Register(firstPassingRule);
            var secondPassingRule = RuleBuilder.APassingRule();
            rulesService.Register(secondPassingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
            Assert.That(result.Messages, Is.Null);
        }

        [Test]
        public void When_two_rules_are_registered_and_all_rules_fail_it_should_return_failure() {
            // arrange
            var rulesService = new RulesHandler();
            var firstFailingRule = RuleBuilder.AFailingRuleWithMessage("First test failure message");
            rulesService.Register(firstFailingRule);
            var secondFailingRule = RuleBuilder.AFailingRuleWithMessage("Second test failure message");
            rulesService.Register(secondFailingRule);
            // act
            var result = rulesService.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Is.Not.Null);
            Assert.That(result.Messages, Has.One.EqualTo(firstFailingRule.Message));
            Assert.That(result.Messages, Has.One.EqualTo(secondFailingRule.Message));
        }
    }
}