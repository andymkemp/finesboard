using System;
using System.Linq;
using NUnit.Framework;

namespace Finesboard.Api.Unit.Tests
{
    [TestFixture]
    public class ConventionTests
    {
        [Test]
        public void DbModel_Suffix()
        {
            var searchType = typeof(IDbModel);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.Contains("Finesboard.Api"))
                .SelectMany(a => a.GetTypes())
                .Where(t => searchType.IsAssignableFrom(t) && !t.IsInterface);
            Assert.Multiple(() => {
                foreach(var type in types)
                {
                    var name = type.Name;
                    Assert.That(name.EndsWith("DataModel"), Is.True, $"{name} is missing 'DataModel' suffix");
                }
            });
        }
        
        [Test]
        public void DataModel_Suffix()
        {
            var searchType = typeof(IDataModel);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.Contains("Finesboard.Api"))
                .SelectMany(a => a.GetTypes())
                .Where(t => searchType.IsAssignableFrom(t) && !t.IsInterface);
            Assert.Multiple(() => {
                foreach(var type in types)
                {
                    var name = type.Name;
                    Assert.That(name.EndsWith("DataModel"), Is.True, $"{name} is missing 'DataModel' suffix");
                }
            });
        }
    }
}