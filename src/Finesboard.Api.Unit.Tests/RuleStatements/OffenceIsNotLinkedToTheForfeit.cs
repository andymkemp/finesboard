using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceIsNotLinkedToForfietTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;
        private static Guid _forfeitId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
            _forfeitId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_is_linked_to_forfeit_it_should_fail() {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel>() { 
                new OffenceForfeitDataModel() { ForfeitId = _forfeitId },
                new OffenceForfeitDataModel() 
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceIsNotLinkedToTheForfeit(_finesboardDataManager, _offenceId, _forfeitId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is linked to the forfeit"));
        }

        [Test]
        public void When_offence_is_not_linked_to_forfeit_it_should_pass()
        {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel>() { 
                new OffenceForfeitDataModel(),
                new OffenceForfeitDataModel() 
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceIsNotLinkedToTheForfeit(_finesboardDataManager, _offenceId, _forfeitId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}