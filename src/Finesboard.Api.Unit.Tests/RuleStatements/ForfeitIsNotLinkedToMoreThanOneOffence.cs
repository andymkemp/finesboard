using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class ForfeitIsNotLinkedToMoreThanOneOffenceTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_forfiet_is_only_linked_to_one_offence_it_should_pass() {
            // arrange
            var offenceId = Guid.NewGuid();
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { OffenceId = offenceId }
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeits);
            RulesHandler.Register(new ForfeitIsNotLinkedToMoreThanOneOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [Test]
        public void When_forfiet_is_not_linked_to_an_offence_it_should_pass() {
            // arrange
            var offenceId = Guid.NewGuid();
            var offenceForfeits = new List<OffenceForfeitDataModel>();
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeits);
            RulesHandler.Register(new ForfeitIsNotLinkedToMoreThanOneOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [Test]
        public void When_forfeit_is_linked_to_more_than_one_offence_it_should_fail()
        {
            // arrange
            var offenceId = Guid.NewGuid();
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel(),
                new OffenceForfeitDataModel()
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeits);
            RulesHandler.Register(new ForfeitIsNotLinkedToMoreThanOneOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is linked to more than one offence"));
        }
    }
}