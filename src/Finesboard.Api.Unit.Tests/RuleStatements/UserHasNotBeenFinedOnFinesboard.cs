using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Services;
using Finesboard.Api.ApiModels;

namespace Finesboard.Api.Unit.Tests
{
    public class UserHasNotBeenFinedOnFinesboardTests: RuleStatementTest
    {
        private static IUserService _userService;
        private static Guid _userId;
        private static Guid _finesboardId;


        [SetUp]
        public void BeforeEach()
        {
            _userService = A.Fake<IUserService>();
            _userId = Guid.NewGuid();
            _finesboardId = Guid.NewGuid();
        }

        [Test]
        public void When_user_has_been_fined_on_finesboard_should_fail() {
            // arrange
            var fines = new List<FineDetailModel>() { 
                new FineDetailModel()
            };
            A.CallTo(() => _userService.GetFinesboardFinesByTarget(_finesboardId, _userId)).Returns(fines);
            RulesHandler.Register(new UserHasNotBeenFinedOnFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"User has been fined on finesboard"));
        }

        [Test]
        public void When_user_has_not_been_fined_on_finesboard_it_should_pass()
        {
            // arrange
            var fines = new List<FineDetailModel>();
            A.CallTo(() => _userService.GetFinesboardFinesByTarget(_finesboardId, _userId)).Returns(fines);
            RulesHandler.Register(new UserHasNotBeenFinedOnFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}