using Finesboard.Api.Rules;
using NUnit.Framework;

namespace Finesboard.Api.Unit.Tests
{
    [TestFixture]
    public class RuleStatementTest
    {
        public IRulesHandler RulesHandler { get; private set; }

        [SetUp]
        public void BeforeEach()
        {
            RulesHandler = new RulesHandler();
        }        
    }
}