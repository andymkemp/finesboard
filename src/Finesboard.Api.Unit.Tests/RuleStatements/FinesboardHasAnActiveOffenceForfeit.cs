using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class FinesboardHasAnActiveOffenceForfeitTests: RuleStatementTest
    {
        [Test]
        public void When_finesboard_has_an_active_offence_forfeit_pass() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() {
                Offences = new List<OffenceDetailDataModel>() {
                    new OffenceDetailDataModel() {
                        Forfeits = new List<LinkedForfeitDataModel> {
                            new LinkedForfeitDataModel()
                        }
                    },
                    new OffenceDetailDataModel() {
                        Forfeits = new List<LinkedForfeitDataModel> {
                            new LinkedForfeitDataModel(),
                            new LinkedForfeitDataModel() { Status = 1 },
                            new LinkedForfeitDataModel()
                        }
                    }
                }
            };
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasAnActiveOffenceForfeit(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [Test]
        public void When_finesboard_has_no_active_offence_forfeit_it_should_fail() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() {
                Offences = new List<OffenceDetailDataModel>() {
                    new OffenceDetailDataModel() {
                        Forfeits = new List<LinkedForfeitDataModel> {
                            new LinkedForfeitDataModel(),
                            new LinkedForfeitDataModel()
                        }
                    },
                    new OffenceDetailDataModel() {
                        Forfeits = new List<LinkedForfeitDataModel> {
                            new LinkedForfeitDataModel()
                        }
                    },
                    new OffenceDetailDataModel() {
                        Forfeits = null
                    }
                }
            };
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasAnActiveOffenceForfeit(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard does not have an active offence forfeit"));
        }

        [Test]
        public void When_finesboard_has_no_offence_it_should_fail() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel(){
                Offences = null
            };
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasAnActiveOffenceForfeit(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard does not have an active offence forfeit"));
        }

        [Test]
        public void When_finesboard_has_no_offence_forfeit_it_should_fail() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() {
                Offences = new List<OffenceDetailDataModel>() {
                    new OffenceDetailDataModel() {
                        Forfeits = null
                    },
                    new OffenceDetailDataModel() {
                        Forfeits = null
                    }
                }
            };
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasAnActiveOffenceForfeit(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard does not have an active offence forfeit"));
        }
    }
}