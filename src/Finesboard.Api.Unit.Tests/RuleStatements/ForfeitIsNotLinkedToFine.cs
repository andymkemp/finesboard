using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;

namespace Finesboard.Api.Unit.Tests
{
    public class ForfeitIsNotLinkedToFineTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_forfiet_is_linked_to_fine_it_should_fail() {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsForfeitLinkedToFine(_forfietId)).Returns(true);
            RulesHandler.Register(new ForfeitIsNotLinkedToFine(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is linked to a fine"));
        }

        [Test]
        public void When_forfeit_is_not_linked_to_fine_it_should_pass()
        {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsForfeitLinkedToFine(_forfietId)).Returns(false);
            RulesHandler.Register(new ForfeitIsNotLinkedToFine(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}