using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceForfeitIsActiveTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _finesboardId;
        private static Guid _offenceId;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _finesboardId = Guid.NewGuid();
            _offenceId = Guid.NewGuid();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_forfeit_is_inactive_should_fail() {
            // arrange
            var offenceForfeit = new OffenceForfeitDataModel() { Status = 0 };
            A.CallTo(() => _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceForfeitIsActive(_finesboardDataManager, _finesboardId, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence forfeit is not active"));
        }

        [Test]
        public void When_offence_forfeit_is_not_linked_to_fine_it_should_pass()
        {
            // arrange
            var offenceForfeit = new OffenceForfeitDataModel() { Status = 1 };
            A.CallTo(() => _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceForfeitIsActive(_finesboardDataManager, _finesboardId, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}