using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class FinesboardIsActiveTests: RuleStatementTest
    {
        [Test]
        public void When_finesboard_status_is_active_it_should_pass() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = "Active"}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardIsActive(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [TestCase("Pending")]
        [TestCase("Closed")]
        [TestCase("Archived")]
        public void When_finesboard_status_is_not_active_it_should_fail(string statusDescription) {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = statusDescription}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardIsActive(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard status is not active"));
        }
    }
}