using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class ForfeitIsNotTheOnlyForfietLinkedToOffenceTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_forfiet_is_only_forfeit_linked_to_offence_it_should_fail() {
            // arrange
            var offenceId = Guid.NewGuid();
            var forfeitOffences = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { OffenceId = offenceId }
            };
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel()
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(forfeitOffences);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(offenceId)).Returns(offenceForfeits);
            RulesHandler.Register(new ForfeitIsNotTheOnlyForfietLinkedToOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Only forfeit linked to offence: { offenceId }"));
        }

        [Test]
        public void When_forfiet_is_only_forfeit_linked_to_multiple_offences_it_should_return_multiple_messages() {
            // arrange
            var firstOffenceId = Guid.NewGuid();
            var secondOffenceId = Guid.NewGuid();
            var thirdOffenceId = Guid.NewGuid();
            var forfeitOffences = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { OffenceId = firstOffenceId },
                new OffenceForfeitDataModel() { OffenceId = secondOffenceId },
                new OffenceForfeitDataModel() { OffenceId = thirdOffenceId }
            };
            var singleOffenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel()
            };
            var multipleOffenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel(),
                new OffenceForfeitDataModel()
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(forfeitOffences);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(firstOffenceId)).Returns(singleOffenceForfeits);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(secondOffenceId)).Returns(singleOffenceForfeits);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(thirdOffenceId)).Returns(multipleOffenceForfeits);
            RulesHandler.Register(new ForfeitIsNotTheOnlyForfietLinkedToOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Only forfeit linked to offence: { firstOffenceId }"));
            Assert.That(result.Messages, Has.One.EqualTo($"Only forfeit linked to offence: { secondOffenceId }"));
            Assert.That(result.Messages, Has.None.EqualTo($"Only forfeit linked to offence: { thirdOffenceId }"));
        }

        [Test]
        public void When_forfeit_is_not_the_only_forfiet_linked_to_offence_it_should_pass()
        {
            // arrange
            var firstOffenceId = Guid.NewGuid();
            var secondOffenceId = Guid.NewGuid();
            var forfeitOffences = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { OffenceId = firstOffenceId },
                new OffenceForfeitDataModel() { OffenceId = secondOffenceId }
            };
            var multipleOffenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel(),
                new OffenceForfeitDataModel()
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(forfeitOffences);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(firstOffenceId)).Returns(multipleOffenceForfeits);
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(secondOffenceId)).Returns(multipleOffenceForfeits);
            RulesHandler.Register(new ForfeitIsNotTheOnlyForfietLinkedToOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [Test]
        public void When_forfeit_is_not_linked_to_an_offence_it_should_pass()
        {
            // arrange
            var offenceId = Guid.NewGuid();
            var forfeitOffences = new List<OffenceForfeitDataModel>();
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(forfeitOffences);
            RulesHandler.Register(new ForfeitIsNotTheOnlyForfietLinkedToOffence(_finesboardDataManager, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}