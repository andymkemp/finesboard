using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class ForfeitIsLinkedToFinesboardTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _finesboardId;
        private static Guid _forfeitId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _finesboardId = Guid.NewGuid();
            _forfeitId = Guid.NewGuid();
        }

        [Test]
        public void When_forfeit_is_not_linked_to_finesboard_it_should_fail() {
            // arrange
            ForfeitDataModel forfeit = new ForfeitDataModel();
            A.CallTo(() => _finesboardDataManager.GetForfeit(_forfeitId)).Returns(forfeit);
            RulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _forfeitId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is not linked to the finesboard"));
        }

        [Test]
        public void When_forfeit_does_not_exist_it_should_fail() {
            // arrange
            ForfeitDataModel forfeit = null;
            A.CallTo(() => _finesboardDataManager.GetForfeit(_forfeitId)).Returns(forfeit);
            RulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _forfeitId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is not linked to the finesboard"));
        }

        [Test]
        public void When_forfeit_is_linked_to_fine_it_should_pass()
        {
            // arrange
            ForfeitDataModel forfeit = new ForfeitDataModel() { FinesboardId = _finesboardId };
            A.CallTo(() => _finesboardDataManager.GetForfeit(_forfeitId)).Returns(forfeit);
            RulesHandler.Register(new ForfeitIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _forfeitId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}