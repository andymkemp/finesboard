using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceIsLinkedToFinesboardTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _finesboardId;
        private static Guid _offenceId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _finesboardId = Guid.NewGuid();
            _offenceId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_is_not_linked_to_finesboard_it_should_fail() {
            // arrange
            OffenceDataModel offence = new OffenceDataModel();
            A.CallTo(() => _finesboardDataManager.GetOffence(_offenceId)).Returns(offence);
            RulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is not linked to the finesboard"));
        }

        [Test]
        public void When_offence_does_not_exist_it_should_fail() {
            // arrange
            OffenceDataModel offence = null;
            A.CallTo(() => _finesboardDataManager.GetOffence(_offenceId)).Returns(offence);
            RulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is not linked to the finesboard"));
        }

        [Test]
        public void When_offence_is_linked_to_fine_it_should_pass()
        {
            // arrange
            OffenceDataModel offence = new OffenceDataModel() { FinesboardId = _finesboardId };
            A.CallTo(() => _finesboardDataManager.GetOffence(_offenceId)).Returns(offence);
            RulesHandler.Register(new OffenceIsLinkedToFinesboard(_finesboardDataManager, _finesboardId, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}