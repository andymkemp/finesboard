using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class ForfeitIsLinkedToOffenceTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;
        private static Guid _forfietId;

        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_forfiet_is_not_linked_to_any_offence_it_should_fail() {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel> { };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is not linked to the offence"));
        }

        [Test]
        public void When_forfiet_is_not_linked_to_the_offence_it_should_fail() {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel> { new OffenceForfeitDataModel() { OffenceId = Guid.NewGuid() } };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Forfeit is not linked to the offence"));
        }

        [Test]
        public void When_forfeit_is_linked_to_the_offence_it_should_pass()
        {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel> { new OffenceForfeitDataModel() { OffenceId = _offenceId } };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByForfeitId(_forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ForfeitIsLinkedToOffence(_finesboardDataManager, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}