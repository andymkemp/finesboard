using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class ActiveOffenceForfeitExistsOnFinesboardTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _finesboardId;
        private static Guid _offenceId;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _finesboardId = Guid.NewGuid();
            _offenceId = Guid.NewGuid();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_forfeit_does_not_exist_on_finesboard_it_should_fail() {
            // arrange
            OffenceForfeitDataModel offenceForfeit = null;
            A.CallTo(() => _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ActiveOffenceForfeitExistsOnFinesboard(_finesboardDataManager, _finesboardId, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence forfeit does not exist on finesboard"));
        }

        [Test]
        public void When_offence_forfeit_is_not_active_it_should_fail() {
            // arrange
            OffenceForfeitDataModel offenceForfeit = new OffenceForfeitDataModel() { Status = 0 };
            A.CallTo(() => _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ActiveOffenceForfeitExistsOnFinesboard(_finesboardDataManager, _finesboardId, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence forfeit is not active"));
        }

        [Test]
        public void When_active_offence_forfeit_exists_on_finesboard_it_should_pass()
        {
            // arrange
            OffenceForfeitDataModel offenceForfeit = new OffenceForfeitDataModel() { Status = 1 };
            A.CallTo(() => _finesboardDataManager.GetFinesboardOffenceForfeit(_finesboardId, _offenceId, _forfietId)).Returns(offenceForfeit);
            RulesHandler.Register(new ActiveOffenceForfeitExistsOnFinesboard(_finesboardDataManager, _finesboardId, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}