using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceForfeitIsNotLinkedToFineTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;
        private static Guid _forfietId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
            _forfietId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_forfeit_is_linked_to_fine_it_should_fail() {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsOffenceForfeitLinkedToFine(_offenceId, _forfietId)).Returns(true);
            RulesHandler.Register(new OffenceForfeitIsNotLinkedToFine(_finesboardDataManager, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence forfeit is linked to a fine"));
        }

        [Test]
        public void When_offence_forfeit_is_not_linked_to_fine_it_should_pass()
        {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsOffenceForfeitLinkedToFine(_offenceId, _forfietId)).Returns(false);
            RulesHandler.Register(new OffenceForfeitIsNotLinkedToFine(_finesboardDataManager, _offenceId, _forfietId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}