using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class FinesboardHasNotBeenDeactivatedTests: RuleStatementTest
    {
        [TestCase("Active")]
        [TestCase("Pending")]
        public void When_finesboard_has_not_been_deactivated_it_should_pass(string statusDescription) {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = statusDescription}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasNotBeenDeactivated(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [TestCase("Closed")]
        [TestCase("Archived")]
        public void When_finesboard_has_been_deactivated_it_should_fail(string statusDescription) {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = statusDescription}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardHasNotBeenDeactivated(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard is {statusDescription}"));
        }
    }
}