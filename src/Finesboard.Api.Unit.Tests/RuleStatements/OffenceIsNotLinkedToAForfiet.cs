using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceIsNotLinkedToAForfietTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_is_linked_forfeit_it_should_fail() {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel>() { new OffenceForfeitDataModel() };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceIsNotLinkedToAForfiet(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is linked to a forfeit"));
        }

        [Test]
        public void When_offence_is_not_linked_to_forfeit_it_should_pass()
        {
            // arrange
            var offenceForfeit = new List<OffenceForfeitDataModel>();
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeit);
            RulesHandler.Register(new OffenceIsNotLinkedToAForfiet(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}