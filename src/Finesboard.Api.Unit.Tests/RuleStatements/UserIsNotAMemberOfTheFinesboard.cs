using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Services;
using Finesboard.Api.ApiModels;

namespace Finesboard.Api.Unit.Tests
{
    public class UserIsNotAMemberOfTheFinesboardTests: RuleStatementTest
    {
        private static IUserService _userService;
        private static Guid _userId;
        private static Guid _finesboardId;


        [SetUp]
        public void BeforeEach()
        {
            _userService = A.Fake<IUserService>();
            _userId = Guid.NewGuid();
            _finesboardId = Guid.NewGuid();
        }

        [Test]
        public void When_user_is_a_member_of_the_finesboard_should_fail() {
            // arrange
            var finesboards = new List<FinesboardExtendedDetailModel>() { 
                new FinesboardExtendedDetailModel() { Id = Guid.NewGuid() },
                new FinesboardExtendedDetailModel() { Id = _finesboardId }
            };
            A.CallTo(() => _userService.GetFinesboards(_userId)).Returns(finesboards);
            RulesHandler.Register(new UserIsNotAMemberOfTheFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"User is a member of the finesboard"));
        }

        [Test]
        public void When_user_is_not_a_member_of_the_finesboard_it_should_pass()
        {
            // arrange
            var finesboards = new List<FinesboardExtendedDetailModel>() { 
                new FinesboardExtendedDetailModel() { Id = Guid.NewGuid() },
                new FinesboardExtendedDetailModel() { Id = Guid.NewGuid() }
            };
            A.CallTo(() => _userService.GetFinesboards(_userId)).Returns(finesboards);
            RulesHandler.Register(new UserIsNotAMemberOfTheFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [Test]
        public void When_user_is_not_a_member_of_any_finesboard_it_should_pass()
        {
            // arrange
            var finesboards = new List<FinesboardExtendedDetailModel>();
            A.CallTo(() => _userService.GetFinesboards(_userId)).Returns(finesboards);
            RulesHandler.Register(new UserIsNotAMemberOfTheFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}