using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;

namespace Finesboard.Api.Unit.Tests
{
    public class FinesboardIsPendingTests: RuleStatementTest
    {
        [Test]
        public void When_finesboard_status_is_pending_it_should_pass() {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = "Pending"}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardIsPending(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }

        [TestCase("Active")]
        [TestCase("Closed")]
        [TestCase("Archived")]
        public void When_finesboard_status_is_not_pending_it_should_fail(string statusDescription) {
            // arrange
            var finesboardDataManager = A.Fake<IFinesboardDataManager>();
            var finesboardId = Guid.NewGuid();
            var finesboard = new FinesboardDetailDataModel() { Status = new FinesboardStatusDataModel() { Description = statusDescription}};
            A.CallTo(() => finesboardDataManager.GetFinesboard(finesboardId)).Returns(finesboard);
            RulesHandler.Register(new FinesboardIsPending(finesboardDataManager, finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Finesboard status is not pending"));
        }
    }
}