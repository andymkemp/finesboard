using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceIsNotLinkedToFineTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_is_linked_to_fine_it_should_fail() {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsOffenceLinkedToFine(_offenceId)).Returns(true);
            RulesHandler.Register(new OffenceIsNotLinkedToFine(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is linked to a fine"));
        }

        [Test]
        public void When_offence_is_not_linked_to_fine_it_should_pass()
        {
            // arrange
            A.CallTo(() => _finesboardDataManager.IsOffenceLinkedToFine(_offenceId)).Returns(false);
            RulesHandler.Register(new OffenceIsNotLinkedToFine(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}