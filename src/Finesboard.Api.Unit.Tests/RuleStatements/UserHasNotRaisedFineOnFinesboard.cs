using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using System;
using System.Collections.Generic;
using Finesboard.Api.Services;
using Finesboard.Api.ApiModels;

namespace Finesboard.Api.Unit.Tests
{
    public class UserHasNotRaisedFineOnFinesboardTests: RuleStatementTest
    {
        private static IUserService _userService;
        private static Guid _userId;
        private static Guid _finesboardId;


        [SetUp]
        public void BeforeEach()
        {
            _userService = A.Fake<IUserService>();
            _userId = Guid.NewGuid();
            _finesboardId = Guid.NewGuid();
        }

        [Test]
        public void When_user_has_raised_fine_on_finesboard_should_fail() {
            // arrange
            var fines = new List<FineDetailModel>() { 
                new FineDetailModel()
            };
            A.CallTo(() => _userService.GetFinesboardFinesByCreator(_finesboardId, _userId)).Returns(fines);
            RulesHandler.Register(new UserHasNotRaisedFineOnFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"User has raised fine on finesboard"));
        }

        [Test]
        public void When_user_has_not_raised_fine_on_finesboard_it_should_pass()
        {
            // arrange
            var fines = new List<FineDetailModel>();
            A.CallTo(() => _userService.GetFinesboardFinesByCreator(_finesboardId, _userId)).Returns(fines);
            RulesHandler.Register(new UserHasNotRaisedFineOnFinesboard(_userService, _userId, _finesboardId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}