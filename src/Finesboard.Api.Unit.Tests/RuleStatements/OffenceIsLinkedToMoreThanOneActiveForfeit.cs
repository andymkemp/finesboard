using Finesboard.Api.RuleStatements;
using NUnit.Framework;
using FakeItEasy;
using Finesboard.Api.DataManagers;
using System;
using Finesboard.Api.Models;
using System.Collections.Generic;

namespace Finesboard.Api.Unit.Tests
{
    public class OffenceIsLinkedToMoreThanOneActiveForfeitTests: RuleStatementTest
    {
        private static IFinesboardDataManager _finesboardDataManager;
        private static Guid _offenceId;


        [SetUp]
        public void BeforeEach()
        {
            _finesboardDataManager = A.Fake<IFinesboardDataManager>();
            _offenceId = Guid.NewGuid();
        }

        [Test]
        public void When_offence_is_linked_to_only_one_active_forfeit_it_should_fail() {
            // arrange
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { Status = 1 }
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeits);
            RulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is only linked to one active forfeit"));
        }

        [Test]
        public void When_offence_is_linked_to_only_one_active_and_one_inavtice_forfeit_it_should_fail() {
            // arrange
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { Status = 1 },
                new OffenceForfeitDataModel() { Status = 0 }
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeits);
            RulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is only linked to one active forfeit"));
        }

        [Test]
        public void When_offence_not_linked_to_a_forfeit_it_should_fail() {
            // arrange
            var offenceForfeits = new List<OffenceForfeitDataModel>();
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeits);
            RulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.True);
            Assert.That(result.Messages, Has.One.EqualTo($"Offence is only linked to one active forfeit"));
        }

        [Test]
        public void When_offence_is_linked_to_more_than_one_forfeit_it_should_pass()
        {
            // arrange
            var offenceForfeits = new List<OffenceForfeitDataModel>() {
                new OffenceForfeitDataModel() { Status = 1 },
                new OffenceForfeitDataModel() { Status = 0 },
                new OffenceForfeitDataModel() { Status = 1 }
            };
            A.CallTo(() => _finesboardDataManager.GetOffenceForfeitsByOffenceId(_offenceId)).Returns(offenceForfeits);
            RulesHandler.Register(new OffenceIsLinkedToMoreThanOneActiveForfeit(_finesboardDataManager, _offenceId));
            // act
            var result = RulesHandler.HandleRules();
            // assert
            Assert.That(result.Failed, Is.False);
        }
    }
}