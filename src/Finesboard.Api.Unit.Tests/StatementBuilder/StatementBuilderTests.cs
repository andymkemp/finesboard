using System;
using NUnit.Framework;

namespace Finesboard.Api.Unit.Tests
{
    [TestFixture]
    public class StatementBuilderTests
    {
        private IStatementBuilder _statementBuilder;

        [SetUp]
        public void Setup()
        {
            _statementBuilder = new StatementBuilder();
        }

        [Test]
        public void GivenFinesModel_WhenBuildingSelectByIdStatement_ShouldReturnStatement()
        {
            var statement = _statementBuilder.SelectById<TestDataModel>();
            Assert.That(statement, Is.EqualTo("SELECT * FROM Test WHERE Id = @Id"));
        }

        [Test]        
        public void GivenFinesModel_WhenBuildingSelectAllStatement_ShouldReturnStatement()
        {
            var statement = _statementBuilder.SelectAll<TestDataModel>();
            Assert.That(statement, Is.EqualTo("SELECT * FROM Test"));
        }

        [Test]        
        public void GivenFinesModel_WhenBuildingSelectWhereStatementWithSingleConstraint_ShouldReturnStatement()
        {
            var statement = _statementBuilder.SelectWhere<TestDataModel>(new { FinesboardId = "something"});
            Assert.That(statement, Is.EqualTo("SELECT * FROM Test WHERE FinesboardId = @FinesboardId"));
        }

        [Test]        
        public void GivenFinesModel_WhenBuildingSelectWhereStatementWithMultipleConstraint_ShouldReturnStatement()
        {
            var statement = _statementBuilder.SelectWhere<TestDataModel>(new { FinesboardId = "something", StatusId = "somethingelse"});
            Assert.That(statement, Is.EqualTo("SELECT * FROM Test WHERE FinesboardId = @FinesboardId and StatusId = @StatusId"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingUpdateStatement_ShouldReturnStatement()
        {
            var statement = _statementBuilder.UpdateById<TestDataModel>();
            Assert.That(statement, Is.EqualTo("UPDATE Test SET Firstname = @Firstname,Surname = @Surname,Description = @Description,Amount = @Amount WHERE Id = @Id"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingInsertStatement_ShouldReturnStatement()
        {
            var statement = _statementBuilder.Insert<TestDataModel>();
            Assert.That(statement, Is.EqualTo("INSERT INTO Test (Id,Firstname,Surname,Description,Amount) VALUES (@Id,@Firstname,@Surname,@Description,@Amount)"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingUpdateStatementWithIgnoreList_ShouldIgnoreGivenColumns()
        {
            var statement = _statementBuilder.UpdateById<TestDataModel>(nameof(TestDataModel.Firstname), nameof(TestDataModel.Surname));
            Assert.That(statement, Is.EqualTo("UPDATE Test SET Description = @Description,Amount = @Amount WHERE Id = @Id"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingUpdateStatementWithWhereClause_ShouldReturnStatement()
        {
            var statement =_statementBuilder.UpdateWhere<TestDataModel>(new { Firstname = "Madonna", Surname = "Madonna"});
            Assert.That(statement, Is.EqualTo("UPDATE Test SET Firstname = @Firstname,Surname = @Surname,Description = @Description,Amount = @Amount WHERE Firstname = @Firstname and Surname = @Surname"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingUpdateStatementWithNoWhereClause_ShouldThrowException()
        {
            Assert.That(() => _statementBuilder.UpdateWhere<TestDataModel>(new {}), Throws.Exception.Message.Contains("Invalid constraints argument: array cannot be empty"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingUpdateStatementWithConstraintFieldsThatDontExistOnTheModel_ShouldThrowException()
        {
            Assert.That(() => _statementBuilder.UpdateWhere<TestDataModel>(new { InvalidConstraint = "Madonna" }), Throws.Exception.Message.Contains($"Invalid constraints argument: InvalidConstraint did not match any properties of the {nameof(TestDataModel)} model"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingDeleteStatementWithWhereClause_ShouldReturnStatement()
        {            
            var statement =_statementBuilder.DeleteWhere<TestDataModel>(new { Firstname = "Madonna", Surname = "Madonna"});
            Assert.That(statement, Is.EqualTo("DELETE FROM Test WHERE Firstname = @Firstname and Surname = @Surname"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingDeleteStatementWithNoWhereClause_ShouldThrowException()
        {
            Assert.That(() => _statementBuilder.DeleteWhere<TestDataModel>(new {}), Throws.Exception.Message.Contains("Invalid constraints argument: array cannot be empty"));
        }

        [Test]
        public void GivenFinesModel_WhenBuildingDeleteStatementWithConstraintFieldsThatDontExistOnTheModel_ShouldThrowException()
        {
            Assert.That(() => _statementBuilder.DeleteWhere<TestDataModel>(new { InvalidConstraint = "Madonna" }), Throws.Exception.Message.Contains($"Invalid constraints argument: InvalidConstraint did not match any properties of the {nameof(TestDataModel)} model"));
        }
    }

    public class TestDataModel: IDbModel
    {
        public Guid? Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}