const gulp = require('gulp');
const Q = require('q');
const tap = require('gulp-tap');
const sort = require('gulp-sort');
const concat = require('gulp-concat');
const exec = require('gulp-exec');
const { build, test, clean, publish } = require('gulp-dotnet-cli');
const gulpClean = require('gulp-clean');

const dbFolder = './db';
const migrationFolder = './migration';
let migrationVersions = [];

gulp.task('db:build', async function () {
  await getVersions();
  for (let index = 0; index < migrationVersions.length; index++) {
    const version = migrationVersions[index];
    await getFiles(version, index, 'ddl');
    await getFiles(version, index, 'dml');
    await getFiles(version, index, 'test_data');
  }
  for (let index = 0; index < migrationVersions.length; index++) {
    const version = migrationVersions[index];
    await concatFiles(version);
  }
});

gulp.task('db:stop', function () {
  return gulp.src('./docker-compose.yml', { read: false })
    .pipe(exec('docker-compose -f <%= file.path %> down'))
    .pipe(exec.reporter());
});

gulp.task('db:start', gulp.series('db:build', 'db:stop', function () {
  return gulp.src('./docker-compose.yml', { read: false })
    .pipe(exec('docker-compose -f <%= file.path %> up -d  && RESULT=$(docker wait finesboard_flyway_1) && docker logs finesboard_flyway_1 && exit $RESULT'))
    .pipe(exec.reporter());
}));

gulp.task('dotnet:build', function () {
  return gulp.src('./src/**/*.csproj', { read: false })
    .pipe(build());
});

gulp.task('dotnet:clean', function () {
  return gulp.src('./src/**/*.csproj', { read: false })
    .pipe(clean());
});

gulp.task('dotnet:publish', gulp.series(
  function () {
    return gulp.src('./src/Finesboard.Api/publish/*', { read: false })
      .pipe(gulpClean());
  },
  function () {
    return gulp.src('./src/Finesboard.Api/Finesboard.Api.csproj', { read: false })
      .pipe(publish({
        output: 'published',
        runtime: 'linux - x64',
        selfcontained: false
      }));
  })
);

gulp.task('dotnet:unit-test', function () {
  return gulp.src([
    './src/Finesboard.Api.Unit.Tests/Finesboard.Api.Unit.Tests.csproj',
    './src/Finesboard.Api.Behaviour.Tests/Finesboard.Api.Behaviour.Tests.csproj']
  , { read: false })
    .pipe(test());
});

gulp.task('docker:build', function () {
  return gulp.src('./dockerfile', { read: false })
    .pipe(exec('docker build -t finesboard:1.0.0.1 .'))
    .pipe(exec.reporter());
});

gulp.task('docker:run', function () {
  return gulp.src('./dockerfile', { read: false })
    .pipe(exec('docker run -p 5000:80 --name finesboard -d --rm --network finesboard_default --link finesboard_db_1:database finesboard:1.0.0.1'))
    .pipe(exec.reporter());
});

gulp.task('docker:stop', function () {
  return gulp.src('./dockerfile', { read: false })
    .pipe(exec('docker stop finesboard'))
    .pipe(exec.reporter());
});

gulp.task('dotnet:e2e-test', gulp.series('docker:build','db:start','docker:run', function () {
  return gulp.src(['./src/Finesboard.Api.E2E.Tests/Finesboard.Api.E2E.Tests.csproj']
    , { read: false })
    .pipe(test());
}));

gulp.task('dotnet:db-test', gulp.series('db:start', function () {
  return gulp.src('./src/Finesboard.Api.DB.Integration.Tests/Finesboard.Api.DB.Integration.Tests.csproj', { read: false })
    .pipe(test());
}));

const getVersions = async function () {
  return promisifyStream(
    gulp.src(`${dbFolder}/*`)
      .pipe(tap((folder) => {
        if (folder.isDirectory()) {
          migrationVersions.push({ name: folder.basename, path: folder.path, files: [] });
        }
      }))
  );
};

const getFiles = async function (version, index, subDirectory) {
  return promisifyStream(
    gulp.src(`${version.path}/${subDirectory}/*.sql`)
      .pipe(sort(sortByNumberedPrefix))
      .pipe(tap((file) => {
        migrationVersions[index].files.push({ name: file.basename, path: file.path });
      }))
  );
};

const concatFiles = async function (version) {
  if (version.files.length === 0) {
    return;
  }
  const files = version.files.map((file) => {
    return file.path;
  });
  await promisifyStream(
    gulp.src(files)
      .pipe(tap((file) => {
        file.contents = Buffer.concat([
          Buffer.from(`----${file.basename}----\n`),
          file.contents
        ]);
      }))
      .pipe(concat(`${version.name}__migration.sql`))
      .pipe(gulp.dest(migrationFolder, {}))
  );
};

const promisifyStream = function (stream) {
  const deferred = Q.defer();
  const onFinish = function (msg) {
    stream.removeListener('error', onError);
    deferred.resolve(msg);
  };
  const onError = function (err) {
    stream.removeListener('finish', onFinish);
    deferred.reject(err);
  };
  stream.once('finish', onFinish);
  stream.once('error', onError);
  return deferred.promise;
};

const sortByNumberedPrefix = function (a, b) {
  const _a = a.basename.split('_');
  const _b = b.basename.split('_');
  return Number.parseInt(_a[0]) > Number.parseInt(_b[0]);
};
