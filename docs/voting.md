# User Story
```
As a 3rd party to the fine
When a new fine is capture
I must be able to second a fine

As a 3rd party to the fine
when a new fine is seconded
I must be able to vote on the fine

```
# New Endpoints
```
[get, post] api/fines/{id}/second
[get, post] api/fines/{id}/votes
[get, post] api/users
[get, put, delete] api/users/{id}
```
# Extend Fine Model
```
{
    Id,
    Target: {
        Id,
        Firstname,
        Surname,
    }
    Description,
    Amount,
    State, [Created, Seconded, Vote Passed, Vote Failed, Struck Off the Roll]
    CreatedDate,
    Creator: {
        Id,
        Firstname,
        Surname,
    },
    SecondedDate,
    Seconder: {
        Id,
        Firstname,
        Surname,
    }
    VoteEndedDate,
}
```