# User Story
```
As an Admin
When configuring the application
I must be able to add a Fine Category
```
```
As an Admin
When configuring the application
I must be able to update a Fine Category
```
```
As an Admin
When configuring the application
I must be able to delete a Fine Category
```
```
As a User
When capturing a Fine 
I must be able to select a Fine Category
```
# New Endpoints
```
[get] api/finesboardtypes
[get] api/finesdenomiations
[get, post] api/finesboards
[get, put, delete] api/finesboards/{id}
[get, post] api/users
[get, put, delete] api/users/{id}
[get, post] api/finesboards/{id}/members
[get, put, delete] api/finesboards/{finesboardid}/members/{memberid}
[get, post] api/categories
[get, put, delete] api/categories/{id}
```
# Update Endpoints
```
[get, post] api/fines
[get, put, delete] api/fines/{id}
```
# api/finesboardtypes
## GET result
```
{
    Id: number,
    Name: string: {
        Id: number,
        Description: string,
    }
}
```
# api/finesboards
## POST body
```
{
    Name: string,
    FinesboardStatus: {
        Id: number,
    },  
}
```
## POST response
```
{
    Id: number,
    Name: string: 
    FinesboardStatus: {
        Id: number,
        Description: string,
    }
}